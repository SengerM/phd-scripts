import xml.etree.ElementTree as ET
from pathlib import Path
import pandas

class ClariusRunBureaucrat:
	def __init__(self, path):
		self.path = path
		xml_run = ET.parse(Path(path)/Path('run.xml')).getroot()
		rating = None
		relative_path_to_data = None
		for child in xml_run:
			if child.tag == 'Rating':
				rating = child.text
			if child.tag == 'DataFileRelativePath':
				relative_path_to_data = child.text
		self.data = {
			'id': xml_run.get('runId'),
			'name': xml_run.get('username') if xml_run.get('username') != '' else None,
			'rating': rating,
			'path to data': Path(path).parent/Path(relative_path_to_data.replace('\\','/')),
		}
		
	def __getitem__(self, key):
		return self.data.get(key)
	
	@property
	def measured_data(self):
		if not hasattr(self, '_measured_data'):
			self._measured_data = pandas.read_excel(self['path to data'])
		return self._measured_data
	
	@property
	def name(self):
		return self.data['name']

if __name__ == '__main__':
	run = ClariusRunBureaucrat('/home/alf/cernbox/measurements_data/LGAD/EPR2021_LGAD_long_term_test/FBK_UFSD3.2_IV_3Terminals_ProbeStation/FBK_UFSD3.2_PINs/measured_data/Run129')
	print(run.measured_data)
	print(run.name)

