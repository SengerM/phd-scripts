from data_processing_bureaucrat.Bureaucrat import Bureaucrat
from clarius_run_bureaucrat import ClariusRunBureaucrat
from pathlib import Path
from io import StringIO
import pandas

# The big string below containing a CSV file was produced from the Excel sheet I made with the sensors the day they arrived. I just hardcoded it here to make it easy.
sensors_from_torino_df = pandas.read_csv(StringIO(""""Manufacturer","Production?","Wafer","Type","Position?","Fluence","Letter?","Fluence/1e14"
"FBK","UFSD3.2",4,4,"10,2",8.0E+14,"E",8
"FBK","UFSD3.2",4,4,"5,2",2.5E+15,"I",25
"FBK","UFSD3.2",4,10,"3,2",2.5E+15,"J",25
"FBK","UFSD3.2",10,4,"7,3",8.0E+14,"C’",8
"FBK","UFSD3.2",10,10,"5,3",8.0E+14,"G’",8
"FBK","UFSD3.2",10,4,"3,3",1.5E+15,"C’",15
"FBK","UFSD3.2",10,10,"2,3",1.5E+15,"F’",15
"FBK","UFSD3.2",10,4,"3,2",2.5E+15,"D’",25
"FBK","UFSD3.2",10,10,"5,2",2.5E+15,"I’",25
"FBK","UFSD3.2",18,4,"9,1",4.0E+14,"R",4
"FBK","UFSD3.2",18,10,"8,3",4.0E+14,"P",4
"FBK","UFSD3.2",18,4,"10,2",8.0E+14,"S",8
"FBK","UFSD3.2",18,10,"6,3",8.0E+14,"X",8
"FBK","UFSD3.2",18,4,"6,5",1.5E+15,"U",15
"FBK","UFSD3.2",18,10,"6,5",1.5E+15,"X",15
"FBK","UFSD3.2",18,4,"2,3",2.5E+15,"R",25
"FBK","UFSD3.2",18,10,"10,5",2.5E+15,"O",25
"FBK","UFSD3.2","4A",10,"4,3",8.0E+14,"G",8
"FBK","UFSD3.2","4A",4,"3,3",1.5E+15,"E",15
"FBK","UFSD3.2","4A",10,"2,3",1.5E+15,"H",15"""))

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	with open(bureaucrat.processed_data_dir_path/Path('list_of_measurements.csv'), 'w') as ofile:
		print('Run,Wafer,Type,Position,Pad,Rating,n_measurement,Fluence/1e14', file = ofile)
		for rundir in sorted((bureaucrat.measurement_base_path/Path('measured_data')).iterdir()):
			run = ClariusRunBureaucrat(rundir)
			print(run['id'], end=',', file=ofile)
			if run['name'] is None:
				print('',file=ofile)
				continue
			thisrun_wafer = run['name'].split(' ')[0].replace('W','')
			thisrun_type = run['name'].split(' ')[1].replace('T','')
			thisrun_position = run["name"].split(" ")[2]
			thisrun_pad = run['name'].split(' ')[3]
			thisrun_number_of_measurement = run['name'].split(' ')[4]
			print(thisrun_wafer, end=',', file=ofile)
			print(thisrun_type, end=',', file=ofile)
			print(f'"{thisrun_position}"', end=',', file=ofile)
			print(thisrun_pad, end=',', file=ofile)
			print(run['rating'], end=',', file=ofile)
			print(thisrun_number_of_measurement, end=',', file=ofile)
			print(sensors_from_torino_df.loc[(sensors_from_torino_df['Wafer']==str(thisrun_wafer))&(sensors_from_torino_df['Type']==int(thisrun_type))&(sensors_from_torino_df['Position?']==str(thisrun_position)),'Fluence/1e14'].iloc[0], file=ofile)
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)

