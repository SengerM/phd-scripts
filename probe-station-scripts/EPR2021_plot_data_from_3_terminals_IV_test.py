from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import myplotlib as mpl
from clarius_run_bureaucrat import ClariusRunBureaucrat
from pathlib import Path
import pandas
import numpy as np
import plotly.graph_objects as go

COLORS_VS_FLUENCE = {
	4: (77, 5, 0),
	8: (0, 98, 255),
	15: (0, 255, 13), 
	25: (255, 72, 0),
}

for fluence in COLORS_VS_FLUENCE:
	COLORS_VS_FLUENCE[fluence] = tuple([x/255 for x in COLORS_VS_FLUENCE[fluence]])

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	measurements_table_df = pandas.read_csv(bureaucrat.processed_by_script_dir_path('EPR2021_create_excel_of_measurements.py')/Path('list_of_measurements.csv'))
	measurements_table_df = measurements_table_df.sort_values(
		by = ['Fluence/1e14','Wafer','Type','Position','Pad','Rating'],
		ascending = [True, True, True, True, True, False],
	)
	
	fig_Ichuck = mpl.manager.new(
		title = 'I_chuck vs V_bias',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = 'Bias voltage (V)',
		ylabel = 'Current (A)',
		yscale = 'log',
	)
	fig_Ipad = mpl.manager.new(
		title = 'I_pad vs V_bias',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = 'Bias voltage (V)',
		ylabel = 'Current (A)',
		yscale = 'log',
	)
	fig_Iguard = mpl.manager.new(
		title = 'I_guard_ring vs V_bias',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = 'Bias voltage (V)',
		ylabel = 'Current (A)',
		yscale = 'log',
	)
	fig_all_currents_together = go.Figure()
	fig_all_currents_together.update_layout(
		title = f"Current vs bias voltage {bureaucrat.measurement_name}",
		xaxis_title = "Bias voltage (V)",
		yaxis_title = "Current (A)",
		yaxis = dict(
			type = 'log',
		)
    )
	already_plotted_devices = []
	for index, row in measurements_table_df.iterrows():
		if row['Wafer'] != row['Wafer']:
			continue
		current_device_name = f'W{row["Wafer"]} T{int(row["Type"])} {row["Position"]}'
		if current_device_name in already_plotted_devices:
			continue
		this_run_bureaucrat = ClariusRunBureaucrat(bureaucrat.measurement_base_path/Path(f'measured_data/Run{row["Run"]}'))
		data = this_run_bureaucrat.measured_data[:int(this_run_bureaucrat.measured_data.shape[0]/2)]
		fig_Ichuck.plot(
			data['AV'],
			data['AI'].abs(),
			label = f'{int(row["Fluence/1e14"])}×10<sup>14</sup> n<sub>eq</sub>/cm<sup>2</sup>, {current_device_name} (Run{this_run_bureaucrat["id"]})',
			color = COLORS_VS_FLUENCE[int(row['Fluence/1e14'])]
		)
		fig_Ipad.plot(
			data['AV'],
			data['BI'].abs(),
			label = f'{int(row["Fluence/1e14"])}×10<sup>14</sup> n<sub>eq</sub>/cm<sup>2</sup>, {current_device_name} (Run{this_run_bureaucrat["id"]})',
			color = COLORS_VS_FLUENCE[int(row['Fluence/1e14'])]
		)
		fig_Iguard.plot(
			data['AV'],
			data['AI'].abs()-data['BI'].abs(),
			label = f'{int(row["Fluence/1e14"])}×10<sup>14</sup> n<sub>eq</sub>/cm<sup>2</sup>, {current_device_name} (Run{this_run_bureaucrat["id"]})',
			color = COLORS_VS_FLUENCE[int(row['Fluence/1e14'])]
		)
		fig_all_currents_together.add_trace(
			go.Scatter(
				x = data['AV'],
				y = data['AI'].abs(),
				name = f'I<sub>chuck</sub> {int(row["Fluence/1e14"])}×10<sup>14</sup> n<sub>eq</sub>/cm<sup>2</sup>, {current_device_name} (Run{this_run_bureaucrat["id"]})',
				legendgroup = current_device_name,
				line = dict(
					color = f"rgb{tuple([i*255 for i in COLORS_VS_FLUENCE[int(row['Fluence/1e14'])]])}",
				),
			)
		)
		fig_all_currents_together.add_trace(
			go.Scatter(
				x = data['AV'],
				y = data['BI'].abs(),
				name = f'I<sub>pad</sub> {int(row["Fluence/1e14"])}×10<sup>14</sup> n<sub>eq</sub>/cm<sup>2</sup>, {current_device_name} (Run{this_run_bureaucrat["id"]})',
				legendgroup = current_device_name,
				line = dict(
					color = f"rgb{tuple([i*255 for i in COLORS_VS_FLUENCE[int(row['Fluence/1e14'])]])}",
					dash = 'dash',
				),
			)
		)
		fig_all_currents_together.add_trace(
			go.Scatter(
				x = data['AV'],
				y = data['AI'].abs()-data['BI'].abs(),
				name = f'I<sub>gr</sub> {int(row["Fluence/1e14"])}×10<sup>14</sup> n<sub>eq</sub>/cm<sup>2</sup>, {current_device_name} (Run{this_run_bureaucrat["id"]})',
				legendgroup = current_device_name,
				line = dict(
					color = f"rgb{tuple([i*255 for i in COLORS_VS_FLUENCE[int(row['Fluence/1e14'])]])}",
					dash = 'dot',
				),
			)
		)
		already_plotted_devices.append(current_device_name)
	fig_all_currents_together.write_html(
		str(bureaucrat.processed_data_dir_path/Path('current vs bias voltage all together.html')),
		include_plotlyjs = 'cdn',
	)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Plots all the IV curves measured in the probe station.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)

