from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import myplotlib as mpl
from clarius_run_bureaucrat import ClariusRunBureaucrat
from pathlib import Path
import plotly.graph_objects as go
import itertools
import plotly.express as px

color_pallete = px.colors.qualitative.Plotly
color_pallete_iterator = itertools.cycle(color_pallete) 


def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	fig_Ichuck = mpl.manager.new(
		title = 'I_chuck vs V_bias',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = 'Bias voltage (V)',
		ylabel = 'Current (A)',
		yscale = 'log',
	)
	fig_Ipad = mpl.manager.new(
		title = 'I_pad vs V_bias',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = 'Bias voltage (V)',
		ylabel = 'Current (A)',
		yscale = 'log',
	)
	fig_Iguard = mpl.manager.new(
		title = 'I_guard_ring vs V_bias',
		
	)
	fig_all_currents_together = mpl.manager.new(
		title = f"Current vs bias voltage {bureaucrat.measurement_name}",
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = 'Bias voltage (V)',
		ylabel = 'Current (A)',
		yscale = 'log',
	)
	for rundir in sorted((bureaucrat.measurement_base_path/Path('measured_data')).iterdir()):
		this_run_bureaucrat = ClariusRunBureaucrat(rundir)
		data = this_run_bureaucrat.measured_data[:int(this_run_bureaucrat.measured_data.shape[0]/2)]
		color_for_this_data = next(color_pallete_iterator)
		fig_Ichuck.plot(
			data['AV'],
			data['AI'].abs(),
			label = this_run_bureaucrat.name,
		)
		fig_Ipad.plot(
			data['AV'],
			data['BI'].abs(),
			label = this_run_bureaucrat.name,
		)
		fig_Iguard.plot(
			data['AV'],
			data['AI'].abs()-data['BI'].abs(),
			label = this_run_bureaucrat.name,
		)
		fig_all_currents_together.plotly_fig.add_trace(
			go.Scatter(
				x = data['AV'],
				y = data['AI'].abs(),
				name = f'I<sub>chuck</sub> ' + this_run_bureaucrat.name,
				legendgroup = this_run_bureaucrat.name,
				line = dict(
					color = color_for_this_data,
				),
			)
		)
		fig_all_currents_together.plotly_fig.add_trace(
			go.Scatter(
				x = data['AV'],
				y = data['BI'].abs(),
				name = f'I<sub>pad</sub> ' + this_run_bureaucrat.name,
				legendgroup = this_run_bureaucrat.name,
				line = dict(
					color = color_for_this_data,
					dash = 'dash',
				),
			)
		)
		fig_all_currents_together.plotly_fig.add_trace(
			go.Scatter(
				x = data['AV'],
				y = data['AI'].abs()-data['BI'].abs(),
				name = f'I<sub>gr</sub> ' + this_run_bureaucrat.name,
				legendgroup = this_run_bureaucrat.name,
				line = dict(
					color = color_for_this_data,
					dash = 'dot',
				),
			)
		)
	# ~ fig_all_currents_together.write_html(
		# ~ str(bureaucrat.processed_data_dir_path/Path('current vs bias voltage all together.html')),
		# ~ include_plotlyjs = 'cdn',
	# ~ )
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Plots all the IV curves measured in the probe station.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)

