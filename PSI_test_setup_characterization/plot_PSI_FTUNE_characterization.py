from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
from scipy.stats import norm

def script_core(dir_with_reconstructor_measurement):
	bureaucrat = Bureaucrat(
		dir_with_reconstructor_measurement,
		variables = locals(),
	)
	
	data_df = pandas.read_csv(bureaucrat.processed_by_script_dir_path('characterize_FTUNE.py')/Path('measured_data.csv'))
	
	delay_chips = sorted(set(data_df['delay_chip']))
	FTUNEs = np.array(sorted(set(data_df['FTUNE voltage (V)'])))
	
	fig_mean = mpl.manager.new(
			title = f'Measured delay average vs FTUNE voltage',
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = f'FTUNE voltage (V)',
			ylabel = f'Average delay (s)',
		)
	fig_std = mpl.manager.new(
			title = f'Delay fluctuations vs FTUNE voltage',
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = f'FTUNE voltage (V)',
			ylabel = f'Delay fluctuations (s)',
		)
	fig_mean_superimposed = mpl.manager.new(
			title = f'Measured delay average vs FTUNE voltage superimposing both chips and removing offset',
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = f'FTUNE voltage (V)',
			ylabel = f'Average delay (s)',
		)
	for delay_chip in delay_chips:
		mean_delay = data_df.loc[data_df['delay_chip']==delay_chip].groupby(['FTUNE voltage (V)']).mean()['Delta t (s)'].tolist()
		mean_delay = np.array(mean_delay)
		std_delay = data_df.loc[data_df['delay_chip']==delay_chip].groupby(['FTUNE voltage (V)']).std()
		
		fig_mean.plot(
			FTUNEs,
			mean_delay,
			label = f'Delay to chip {delay_chip}',
			marker = '.',
		)
		
		fig_mean_superimposed.plot(
			FTUNEs,
			np.array(np.abs(mean_delay-mean_delay[-1])),
			label = f'Delay to chip {delay_chip}',
			marker = '.',
		)
		
		
		fig_std.plot(
			FTUNEs,
			std_delay['Delta t (s)'],
			label = f'Delay to chip {delay_chip}',
			marker = '.',
		)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
	
	for delay_chips in delay_chips:
		for FTUNE in FTUNEs[sorted(np.random.randint(low=0, high=len(FTUNEs), size=10))]:
			fig = mpl.manager.new(
				title = f'Distribution of Delta_t for delay to chip {delay_chip} FTUNE {FTUNE} ',
				subtitle = f'Dataset: {bureaucrat.measurement_name}',
				xlabel = f'Δt (s)',
				ylabel = f'Density of events',
			)
			fig.hist(
				data_df.loc[(data_df['FTUNE voltage (V)']==FTUNE) & (data_df['delay_chip']==delay_chip), 'Delta t (s)'],
				density = True,
				label = f'Measured Δt',
			)
			data_for_fit = data_df.loc[(data_df['FTUNE voltage (V)']==FTUNE) & (data_df['delay_chip']==delay_chip), 'Delta t (s)'].to_numpy()
			data_for_fit = data_for_fit[~np.isnan(data_for_fit)]
			µ, σ = norm.fit(data_for_fit)
			x_axis = np.linspace(min(data_df.loc[(data_df['FTUNE voltage (V)']==FTUNE) & (data_df['delay_chip']==delay_chip), 'Delta t (s)']), max(data_df.loc[(data_df['FTUNE voltage (V)']==FTUNE) & (data_df['delay_chip']==delay_chip), 'Delta t (s)']), 99)
			fig.plot(
				x_axis,
				norm.pdf(x_axis, µ, σ),
				label = f'Fit (µ={µ*1e9:.3f} ns, σ={σ*1e12:.2f} ps)',
			)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('Delta t distributions'))
			
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='')
	parser.add_argument(
		'--dir-with-reconstructor-measurement',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement which has already been used to train a reconstructor.',
		required = True,
		dest = 'directory_reconstructor',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory_reconstructor)

