from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
from scipy.stats import norm

def script_core(dir_with_reconstructor_measurement):
	bureaucrat = Bureaucrat(
		dir_with_reconstructor_measurement,
		variables = locals(),
	)
	
	data_df = pandas.read_csv(bureaucrat.processed_by_script_dir_path('characterize_D.py')/Path('measured_data.csv'))
	
	delay_chips = sorted(set(data_df['delay_chip']))
	Ds = np.array(sorted(set(data_df['D'])))
	
	with open(bureaucrat.processed_data_dir_path/Path('calibration_file.csv'), 'w') as ofile:
		print(f'# Calibration done with dataset {bureaucrat.measurement_name}', file = ofile)
		print(f'delay_chip,D,average delay (s)', file = ofile)
		for delay_chip in delay_chips:
			mean_delay = data_df.loc[data_df['delay_chip']==delay_chip].groupby(['D']).mean()
			std_delay = data_df.loc[data_df['delay_chip']==delay_chip].groupby(['D']).std()
			
			for D, delay in zip(list(Ds), list(mean_delay['Delta t (s)'])):
				print(f'{delay_chip},{D},{delay}', file = ofile)
			
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='')
	parser.add_argument(
		'--dir-with-reconstructor-measurement',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement which has already been used to train a reconstructor.',
		required = True,
		dest = 'directory_reconstructor',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory_reconstructor)

