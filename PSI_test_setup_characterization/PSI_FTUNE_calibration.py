from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
from scipy.stats import norm

COLORS = [
	(.2,.2,.8),
	(.9,.2,.2)
]

def script_core(dir_with_reconstructor_measurement):
	bureaucrat = Bureaucrat(
		dir_with_reconstructor_measurement,
		variables = locals(),
	)
	
	data_df = pandas.read_csv(bureaucrat.processed_by_script_dir_path('characterize_FTUNE.py')/Path('measured_data.csv'))
	
	delay_chips = sorted(set(data_df['delay_chip']))
	FTUNEs = np.array(sorted(set(data_df['FTUNE voltage (V)'])))
	
	fig_calibration = mpl.manager.new(
		title = f'FTUNE calibration',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = f'FTUNE voltage (V)',
		ylabel = f'Average delay (s)',
	)
	with open(bureaucrat.processed_data_dir_path/Path('calibration_file.csv'), 'w') as ofile:
		print(f'# Calibration done with dataset {bureaucrat.measurement_name}', file = ofile)
		print(f'delay_chip,FTUNE (V),average delay (s)', file = ofile)
		for idx,delay_chip in enumerate(delay_chips):
			mean_delay = data_df.loc[data_df['delay_chip']==delay_chip].groupby(['FTUNE voltage (V)']).mean()['Delta t (s)'].tolist()
			mean_delay = np.array(mean_delay)
			
			delays_with_no_offset = np.array(np.abs(mean_delay-mean_delay[-1]))
			
			fitted_parameters = np.polyfit(
				x = FTUNEs,
				y = delays_with_no_offset,
				deg = 2,
			)
			
			fig_calibration.plot(
				FTUNEs,
				delays_with_no_offset,
				label = f'Delay to chip {delay_chip}',
				marker = '.',
				color = COLORS[idx],
			)
			fig_calibration.plot(
				FTUNEs,
				np.polyval(p = fitted_parameters, x = FTUNEs),
				label = f'Fit {fitted_parameters[0]:.1e} x² + {fitted_parameters[1]:.1e} x + {fitted_parameters[2]:.1e}',
				color = COLORS[idx],
			)
			
			for delay, ftune in zip(delays_with_no_offset, FTUNEs):
				print(f'{delay_chip},{ftune},{delay}', file = ofile)
		
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='')
	parser.add_argument(
		'--dir-with-reconstructor-measurement',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement which has already been used to train a reconstructor.',
		required = True,
		dest = 'directory_reconstructor',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory_reconstructor)

