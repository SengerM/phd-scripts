from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
from scipy.stats import norm

def script_core(dir_with_reconstructor_measurement):
	bureaucrat = Bureaucrat(
		dir_with_reconstructor_measurement,
		variables = locals(),
	)
	
	data_df = pandas.read_csv(bureaucrat.processed_by_script_dir_path('test_D_FTUNE_calibration.py')/Path('measured_data.csv'))
	
	fig_mean = mpl.manager.new(
		title = f'Measured delay average vs set delay',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = f'Set delay (s)',
		ylabel = f'Average measured delay (s)',
	)
	fig_std = mpl.manager.new(
		title = f'Delay fluctuations vs set delay',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = f'Set delay (s)',
		ylabel = f'Measured delay fluctuations (s)',
	)
	mean_delay = data_df.groupby(['set_delay (s)']).mean()
	std_delay = data_df.groupby(['set_delay (s)']).std()
	
	fig_mean.plot(
		mean_delay.index,
		mean_delay['Delta t (s)'],
		marker = '.',
	)
	
	fig_std.plot(
		std_delay.index,
		std_delay['Delta t (s)'],
		marker = '.',
	)
	fig_mean.plot(
		mean_delay.index,
		mean_delay.index,
		color = (0,)*3,
		alpha = .5,
		label = 'y = x',
	)
	
	fig = mpl.manager.new(
		title = f'Calibration mean error',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = f'Set delay (s)',
		ylabel = f'Average measured delay - Set delay (s)',
	)
	fig.plot(
		mean_delay.index,
		mean_delay['Delta t (s)'] - mean_delay.index,
		marker = '.',
	)
	
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='')
	parser.add_argument(
		'--dir-with-reconstructor-measurement',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement which has already been used to train a reconstructor.',
		required = True,
		dest = 'directory_reconstructor',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory_reconstructor)

