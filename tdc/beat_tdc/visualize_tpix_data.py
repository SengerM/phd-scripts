import numpy as np
import argparse
import myplotlib as mpl
from tpix_utils import plot_colormap, normalize_tdc_inverter_signal
from bureaucrat import Bureaucrat

########################################################################

parser = argparse.ArgumentParser(description='Produces visualization plots for a single run of data from tpix.')
parser.add_argument(
	'-dir',
	metavar = 'path', 
	help = 'Path to a measurement dir.',
	required = True,
	dest = 'path',
)
parser.add_argument(
	'-n',
	metavar = 'n',
	help = 'Number of file to visualize (e.g.33). Default is 1.',
	dest = 'n_file',
	type = int,
	required = False,
	default = 1,
)
########################################################################

args = parser.parse_args()
bureaucrat = Bureaucrat(args.path)

data = np.genfromtxt(f'{bureaucrat.raw_data_dir_path}/{args.n_file:0>5}.txt').transpose()
time = data[0]*1e-12
counter = data[1]
inverters = {}
for inverter_number in range(9):
	inverter_name = f'INVERTER{inverter_number+1}'
	inverters[inverter_name] = data[inverter_number+2]

normalized_inverters = {}
for inverter in inverters.keys():
	normalized_inverters[inverter] = normalize_tdc_inverter_signal(inverters[inverter])

# Do plots:

mpl.manager.set_plotting_package('plotly')

colormap_fig = plot_colormap(time, normalized_inverters)
colormap_fig.set(
	title = f'Colormap plot of normalized signals',
	subtitle = f'Measurement {bureaucrat.measurement_name}, file {args.n_file:0>5}.txt',
	xlabel = 'Inverter number',
	ylabel = 'Time (s)',
)

counters_fig = mpl.manager.new(
	title = f'Counter',
	subtitle = f'Measurement {bureaucrat.measurement_name}, raw file {args.n_file:0>5}.txt',
	xlabel = 'Time (s)',
	ylabel = 'Counter value',
)
counters_fig.plot(
	time,
	counter,
	marker = '.',
)

inverters_fig = mpl.manager.new(
	title = f'Raw inverters signals',
	subtitle = f'Measurement {bureaucrat.measurement_name}, raw file {args.n_file:0>5}.txt',
	xlabel = 'Time (s)',
	ylabel = 'Amplitude',
)
for inverter in inverters.keys():
	inverters_fig.plot(
		time,
		inverters[inverter],
		label = inverter,
	)

inverters_fig = mpl.manager.new(
	title = f'Normalized inverters signals',
	subtitle = f'Measurement {bureaucrat.measurement_name}, raw file {args.n_file:0>5}.txt',
	xlabel = 'Time (s)',
	ylabel = 'Amplitude',
)
for inverter in inverters.keys():
	inverters_fig.plot(
		time,
		normalized_inverters[inverter],
		label = inverter,
	)

mpl.manager.save_all(mkdir = f'{bureaucrat.processed_data_dir_path}')
print(f'Plots were saved in {bureaucrat.processed_data_dir_path}')
mpl.manager.show()
