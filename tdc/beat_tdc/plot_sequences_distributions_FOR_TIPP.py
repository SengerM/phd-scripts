import numpy as np
import myplotlib as mpl
import os
from bureaucrat import Bureaucrat
from sequence import TDCInvertersSequenceAndCounter

COLORS = [
	(0,0,0),
	(1,0,0),
	(0,0,1),
	(0,.7,0),
	(.8,0,.8),
	(1,.5,0),
]

def script_core(path, n_levels):
	bureaucrat = Bureaucrat(
		path,
		processing_script_path = script_core.__globals__['__file__'],
	)

	if not os.path.isdir(f'{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution'):
		raise ValueError(f'There is no data processed by the script "sequences_distribution.py". I know this becauseh the directory "{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution" does not exist.')

	mpl.manager.set_plotting_package('plotly')

	# Read data ---
	sequences_indices = {}
	sequences_times = {}
	sequences = []
	mean_times = []
	for fname in sorted(os.listdir(f'{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution/{n_levels}_discretization_levels')):
		data = np.genfromtxt(f'{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution/{n_levels}_discretization_levels/{fname}').transpose()
		if data[0].shape == (): # This means that the sequence appeared only once. It is irrelevant, and introduces a complication for plotting.
			continue
		sequence = TDCInvertersSequenceAndCounter(counter = int(fname.replace('.txt','').split('.')[0]), sequence = fname.replace('.txt','').split('.')[1])
		sequences.append(sequence)
		sequences_indices[sequence] = data[0].astype(int)
		sequences_times[sequence] = data[1]
		mean_times.append(sequences_times[sequence].mean())
	n_levels = sequence.n_levels
	sequences = [x for _,_,x in sorted(zip(mean_times,range(len(mean_times)),sequences))] # Sort sequences according to mean time.
	
	histograms = {}
	times_for_the_plot = {}
	bins = list(np.arange(-5e-12,max([max(times) for times in sequences_times.values()]),10e-12))
	for sequence in sequences:
		count, index = np.histogram(
			sequences_times[sequence], 
			bins = bins,
		)
		
		count = list(count)
		count.insert(0,0)
		count.append(0)
		index = list(index)
		index.insert(0,index[0] - np.diff(index)[0])
		index.append(index[-1] + np.diff(index)[-1])
		index += np.diff(index)[0]/2 # This is because np.histogram returns the bins edges and I want to plot in the middle.
		histograms[sequence] = count
		times_for_the_plot[sequence] = index

	# Plot time distribution for sequences ---
	sequences_histogram = mpl.manager.new(
		title = f'Time distribution of sequences with {n_levels} levels of discretization',
		show_title = False,
		# ~ subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'Δt = t<sub>START</sub> - t<sub>STOP</sub> (s)',
		ylabel = 'Number of events',
	)

	bins = list(np.arange(-5e-12,2222e-12,10e-12))
	for idx,sequence in enumerate(sequences):
		if sequences_times[sequence].mean() > max(bins):
			continue
		sequences_histogram.hist(
			sequences_times[sequence],
			label = str(sequence),
			bins = bins,
			color = tuple(np.random.rand(3)),
		)
	
	cmap = mpl.manager.new(
		title = f'Colormap distribution with {n_levels} levels of discretization',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'Time of occurence (s)',
		ylabel = 'Sequence (see "sequences look up table" file)',
		package = 'plotly',
	)
	xx,yy = np.meshgrid(np.array(times_for_the_plot[sequences[0]]), np.array([i for i in range(len(sequences))]))
	cmap.colormap(
		x = xx,
		y = yy,
		z = np.array(list(histograms.values())),
	)
	
	# Save data ---
	mpl.manager.save_all(mkdir = f'{bureaucrat.processed_data_dir_path}')
	with open(f'{bureaucrat.processed_data_dir_path}/sequences look up table for {n_levels} of discretization.txt', 'w') as ofile:
		print('# Int\tSequence', file = ofile)
		for idx, sequence in enumerate(sequences):
			print(f'{idx}\t{sequence}', file = ofile)
	print(f'Plot has been saved in {bureaucrat.processed_data_dir_path}')

if __name__ == '__main__':
	import argparse
	
	########################################################################

	parser = argparse.ArgumentParser(description='Plot results from analysis of script "sequences_distribution.py".')
	parser.add_argument(
		'-dir',
		metavar = 'path', 
		help = 'Path to measurement directory',
		required = True,
		dest = 'path',
	)
	parser.add_argument(
		'-nlevels',
		metavar = 'n', 
		help = 'Number of discretization levels to analyze',
		required = True,
		dest = 'n_levels',
	)

	########################################################################

	args = parser.parse_args()
	script_core(
		path = args.path,
		n_levels = args.n_levels,
	)
