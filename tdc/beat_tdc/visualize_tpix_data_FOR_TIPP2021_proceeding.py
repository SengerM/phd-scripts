import numpy as np
import argparse
import myplotlib as mpl
from tpix_utils import plot_colormap, normalize_tdc_inverter_signal
from bureaucrat import Bureaucrat

########################################################################

parser = argparse.ArgumentParser(description='Produces visualization plots for a single run of data from tpix.')
parser.add_argument(
	'-dir',
	metavar = 'path', 
	help = 'Path to a measurement dir.',
	required = True,
	dest = 'path',
)
parser.add_argument(
	'-n',
	metavar = 'n',
	help = 'Number of file to visualize (e.g.33). Default is 1.',
	dest = 'n_file',
	type = int,
	required = False,
	default = 1,
)
########################################################################

args = parser.parse_args()
bureaucrat = Bureaucrat(args.path)

data = np.genfromtxt(f'{bureaucrat.raw_data_dir_path}/{args.n_file:0>5}.txt').transpose()
time = data[0]*1e-12
counter = data[1]
inverters = {}
for inverter_number in range(9):
	inverter_name = f'INVERTER{inverter_number+1}'
	inverters[inverter_name] = data[inverter_number+2]

normalized_inverters = {}
for inverter in inverters.keys():
	normalized_inverters[inverter] = normalize_tdc_inverter_signal(inverters[inverter])

# Do plots:
import matplotlib.pyplot as plt
plt.style.use('alfrc_style')

mpl.manager.set_plotting_package('matplotlib')

inverters_fig = mpl.manager.new(
	title = f'Normalized inverters signals',
	show_title = False,
	xlabel = r'$\Delta t = t_{START}-t_{STOP}$ (ps)',
	ylabel = 'Normalized amplitude',
)
for idx,inverter in enumerate(inverters.keys()):
	inverters_fig.plot(
		1e12*time,
		(-1)**idx*normalized_inverters[inverter] - (-1+(-1)**idx)/2,
		label = (r'-1$\times$' if (-1)**idx==-1 else '') + f'INV[{idx}]',
		marker = '.',
	)
inverters_fig.matplotlib_fig.set_size_inches(7*2/3, 2)
inverters_fig.matplotlib_ax.legend(loc='center left', bbox_to_anchor=(1.04,.5), prop={'size': 6})
inverters_fig.matplotlib_ax.set_xlim(0, 2222)
mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path, format='pdf')
