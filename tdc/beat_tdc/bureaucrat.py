import os
from myplotlib.utils import get_timestamp
import __main__
from shutil import copyfile

class Bureaucrat:
	def __init__(self, measurement_base_path: str, processing_script_path=None):
		if not isinstance(measurement_base_path, str):
			raise TypeError(f'<measurement_base_path> must be a string.')
		
		if processing_script_path == None:
			processing_script_path = os.path.join(os.getcwd(), __main__.__file__)
		
		self._processing_script_path = processing_script_path
		self._timestamp = get_timestamp()
		self._measurement_base_path =  measurement_base_path
		self._raw_data_subdir = 'raw'
		self._processed_data_subdir = f'processed_by_script_{self._processing_script_path.split("/")[-1]}'.replace(".py","")
		
		if not os.path.isdir(measurement_base_path):
			raise ValueError(f'Directory {measurement_base_path} does not exist.')
		
		self.create_dir_structure_for_new_measurement()
		
		copyfile(
			self._processing_script_path,
			os.path.join(self.scripts_dir_path, self._processing_script_path.split('/')[-1])
		)
	
	def create_dir_structure_for_new_measurement(self):
		dirs = [
			self.raw_data_dir_path,
			self.processed_data_dir_path,
			self.scripts_dir_path,
		]
		for d in dirs:
			if not os.path.isdir(d):
				os.makedirs(d)
	
	@property
	def raw_data_dir_path(self):
		return os.path.join(self.measurement_base_path,self._raw_data_subdir)
	
	@property
	def processed_data_dir_path(self):
		return os.path.join(self.measurement_base_path,self._processed_data_subdir)
	
	@property
	def scripts_dir_path(self):
		return os.path.join(self.measurement_base_path, 'scripts')
	
	@property
	def measurement_base_path(self):
		return self._measurement_base_path
	
	@property
	def measurement_name(self):
		return self.measurement_base_path.split('/')[-1]
	
	@property
	def this_run_timestamp(self):
		return self._timestamp
