import numpy as np
import os
from bureaucrat import Bureaucrat

def script_core(ifile):
	raw_data = np.genfromtxt(ifile).transpose()
	time_data = raw_data[0]

	ring_of_inverters_signals = {
		'TDC1': {},
		'TDC2': {},
		'TDC3': {},
	}
	counters = {
		'TDC1': {},
		'TDC2': {},
		'TDC3': {},
	}
	bureaucrats = {}
	for tdc_number in [1,2,3]:
		tdc_name = f'TDC{tdc_number}'
		counters[tdc_name] = raw_data[(tdc_number-1)*10+1]
		for inverter_number in [1,2,3,4,5,6,7,8,9]:
			ring_of_inverters_signals[tdc_name][f'INVERTER{inverter_number}'] = raw_data[(tdc_number-1)*10+1+inverter_number].astype(int)
		
		os.mkdir(f'{ifile[:-4]}_{tdc_name}')
		bureaucrats[tdc_name] = Bureaucrat(
			f'{ifile[:-4]}_{tdc_name}',
			processing_script_path = script_core.__globals__['__file__'],
			)

	n_run = 0
	end_of_file = False
	while not end_of_file:
		n_run += 1
		for tdc in counters.keys():
			k = 0
			with open(f'{bureaucrats[tdc].raw_data_dir_path}/{n_run:0>5}.txt', 'w') as ofile:
				print(f'# Time\tCounter' + f'\tInverter '.join([f'{i+1}' for i in range(9)]), file = ofile)
			with open(f'{bureaucrats[tdc].raw_data_dir_path}/{n_run:0>5}.txt', 'a') as ofile:
				while True:
					ostring = f'{int(time_data[k])}\t'
					k += 1
					ostring += f'{int(counters[tdc][0])}\t'
					for inverter in ring_of_inverters_signals[tdc].keys():
						ostring += f'{ring_of_inverters_signals[tdc][inverter][0]}\t'
						ring_of_inverters_signals[tdc][inverter] = ring_of_inverters_signals[tdc][inverter][1:]
					ostring = ostring[:-1]
					print(
						ostring,
						file = ofile,
					)
					counters[tdc] = counters[tdc][1:]
					if k >= len(time_data) or int(time_data[k]) == 0:
						break
			if tdc == 'TDC3':
				time_data = time_data[k:]
			if len(time_data) == 0:
				end_of_file = True
				break

	for tdc in counters.keys():
		os.rmdir(f'{bureaucrats[tdc].processed_data_dir_path}')

if __name__ == '__main__':
	import argparse
	
	########################################################################
	
	parser = argparse.ArgumentParser(description='Separate a file containing all the measurements from a TDC in Beat\'s format into a set of files each with a unique measurement each. The file is assumed to contain many runs for 3 TDC in the same test structure.')
	parser.add_argument('-i',
		metavar = 'file', 
		help = 'Input raw file.',
		required = True,
		dest = 'ifile'
	)

	########################################################################

	args = parser.parse_args()
	script_core(args.ifile)
