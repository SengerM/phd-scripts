import numpy as np
import myplotlib as mpl
import os
from bureaucrat import Bureaucrat
from sequence import TDCInvertersSequenceAndCounter
from tpix_utils import average_filter

def calculate_color(n, n_min, n_max):
	def gaussian(x, µ, σ):
		return np.exp(-(x-µ)**2/2/σ**2)
	# ~ r = gaussian(n, n_min, (n_max-n_min)/8) + gaussian(n, 6, .5)
	# ~ g = gaussian(n, 5, (n_max-n_min)/4)*.9
	# ~ b = gaussian(n, 7, (n_max-n_min)/11)
	r = gaussian(n, 2, 2)
	g = gaussian(n, 8, 1)*.6
	b = gaussian(n, 5, 1)
	return tuple(np.array((r,g,b))/(max(r,g,b) if any([c>1 for c in (r,g,b)]) else 1))

def calculate_time_resolution(times):
	times = np.array(times)
	if len(times) < 2:
		return float('NaN')
	resolution = np.quantile(times, q = .95) - np.quantile(times, q = .5)
	return resolution# if 0 < resolution < 99e-12 else float('NaN')

def script_core(path):
	bureaucrat = Bureaucrat(
		path,
		processing_script_path = script_core.__globals__['__file__'],
	)
	
	# Do plots:
	import matplotlib.pyplot as plt
	plt.style.use('alfrc_style')

	mpl.manager.set_plotting_package('matplotlib')
	
	if not os.path.isdir(f'{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution'):
		raise ValueError(f'There is no data processed by the script "sequences_distribution.py". I know this becauseh the directory "{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution" does not exist.')
	
	n_levels = []
	for fname in sorted(os.listdir(f'{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution')):
		n_levels.append(int(fname.replace('_discretization_levels','')))
	
	# Read data ---
	sequences_indices = {}
	sequences_times = {}
	sequences = {}
	mean_times = {}
	time_resolution = {}
	for n in n_levels:
		sequences_indices[n] = {}
		sequences_times[n] = {}
		sequences[n] = []
		mean_times[n] = []
		time_resolution[n] = {}
		for fname in sorted(os.listdir(f'{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution/{n}_discretization_levels')):
			data = np.genfromtxt(f'{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution/{n}_discretization_levels/{fname}').transpose()
			if data[0].shape == (): # This means that the sequence appeared only once. It is irrelevant, and introduces a complication for plotting.
				continue
			sequence = TDCInvertersSequenceAndCounter(counter = int(fname.replace('.txt','').split('.')[0]), sequence = fname.replace('.txt','').split('.')[1])
			sequences[n].append(sequence)
			sequences_indices[n][sequence] = data[0].astype(int)
			sequences_times[n][sequence] = data[1]
			mean_times[n].append(sequences_times[n][sequence].mean())
			time_resolution[n][sequence] = calculate_time_resolution(sequences_times[n][sequence])
		sequences[n] = [x for _,_,x in sorted(zip(mean_times[n],range(len(mean_times[n])),sequences[n]))] # Sort sequences according to mean time.
	
	fig_quant_distribution = mpl.manager.new(
		title = f'Time resolution histograms',
		show_title = False,
		xlabel = f'Output distribution width (ps)',
		ylabel = f'Number of outputs',
		package = 'matplotlib',
	)
	bins = 1e12*np.array([10e-12*i for i in range(11)])
	for n in [8]:
		stuff_for_the_histogram = np.array([time_resolution[n][s] for s in sequences[n]])
		stuff_for_the_histogram = stuff_for_the_histogram[~np.isnan(stuff_for_the_histogram)]
		fig_quant_distribution.hist(
			1e12*stuff_for_the_histogram,
			label = r'$W = q_{95\%}-q_{5\%}$',
			bins = bins,
			color = tuple(np.array((255, 98, 87))/255),
		)
		
		stuff_for_the_histogram = np.array([np.array(sequences_times[n][s]).std() for s in sequences[n]])
		stuff_for_the_histogram = stuff_for_the_histogram[~np.isnan(stuff_for_the_histogram)]
		fig_quant_distribution.hist(
			1e12*stuff_for_the_histogram,
			label = r'$\sigma$',
			bins = bins,
			color = tuple(np.array((115, 222, 73))/255),
		)
	fig_quant_distribution.matplotlib_fig.set_size_inches(7*1/3, 2)
	fig_quant_distribution.matplotlib_ax.legend(prop={'size': 6})
	fig_quant_distribution.matplotlib_ax.set_xlim(0, 70)
	
	# Save data ---
	mpl.manager.save_all(mkdir = f'{bureaucrat.processed_data_dir_path}', format='pdf')
	print(f'Plot has been saved in {bureaucrat.processed_data_dir_path}')

if __name__ == '__main__':
	import argparse
	mpl.manager.set_plotting_package('plotly')
	
	########################################################################

	parser = argparse.ArgumentParser(description='Calculates the time resolution usin the results from script "sequences_distribution.py".')
	parser.add_argument(
		'-dir',
		metavar = 'path', 
		help = 'Path to measurement directory',
		required = True,
		dest = 'path',
	)

	########################################################################

	args = parser.parse_args()
	script_core(
		path = args.path,
	)
