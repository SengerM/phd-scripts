import os
import argparse
from split_single_raw_file_into_many_files import script_core as split_single_raw_file_into_many_files
from sequences_distribution import script_core as sequences_distribution
from plot_sequences_distributions import script_core as plot_sequences_distributions
from fix_counter import script_core as fix_counter
from time_resolution import script_core as time_resolution

########################################################################

parser = argparse.ArgumentParser(description='Main script that calls sub-scripts to do the complete analysis of the data produced by Beat.')
parser.add_argument(
	'-raw-file',
	metavar = 'path', 
	help = 'Path to the raw file from Beat',
	required = True,
	dest = 'path',
)
parser.add_argument(
	'-up-to-nlevels',
	metavar = 'n', 
	help = 'Number of discretization levels to analyze',
	required = True,
	dest = 'n_levels',
	type = int,
)

########################################################################

args = parser.parse_args()
base_path = '/'.join(args.path.split('/')[:-1])
measurement_base_name = args.path.split('/')[-1].replace('.txt','')

split_single_raw_file_into_many_files(
	ifile = f'{base_path}/{measurement_base_name}.txt',
)

for tdc_number in [1,2,3]:
	print(f'Starting processing TDC {tdc_number}...')
	for fname in sorted(os.listdir(f'{base_path}/{measurement_base_name}_TDC{tdc_number}/raw')):
		print(f'Fixing {fname}')
		fix_counter(
			path = f'{base_path}/{measurement_base_name}_TDC{tdc_number}',
			fname = fname,
		)
	print('All counters were fixed!')
	print(f'Starting analysis for TDC {tdc_number}, this may take some time...')
	for n_levels in range(args.n_levels):
		n_levels += 1
		if n_levels == 1:
			continue
		sequences_distribution(
			path = f'{base_path}/{measurement_base_name}_TDC{tdc_number}',
			n_levels = n_levels,
		)
		plot_sequences_distributions(
			path = f'{base_path}/{measurement_base_name}_TDC{tdc_number}',
			n_levels = n_levels,
		)
	time_resolution(f'{base_path}/{measurement_base_name}_TDC{tdc_number}')
	print(f'Finished with TDC {tdc_number}!')
