import numpy as np
import myplotlib as mpl
from copy import copy

def read_data(fname):
	raw_data = np.genfromtxt(fname).transpose()
	time_data = raw_data[0]*1e-12
	ring_of_inverters_signals = {
		'TDC1': {},
		'TDC2': {},
		'TDC3': {},
	}
	counters = {
		'TDC1': {},
		'TDC2': {},
		'TDC3': {},
	}
	for tdc_number in [1,2,3]:
		counters[f'TDC{tdc_number}'] = raw_data[(tdc_number-1)*10+1]
		for inverter_number in [1,2,3,4,5,6,7,8,9]:
			ring_of_inverters_signals[f'TDC{tdc_number}'][f'INVERTER{inverter_number}'] = raw_data[(tdc_number-1)*10+1+inverter_number]
	return time_data, ring_of_inverters_signals, counters

def plot_colormap(time, signals: dict):
	tt,nn = np.meshgrid(time, [i for i in range(len(signals))])
	z = [signals[inv]*(-1)**idx-(-1+(-1)**idx)/2 for idx,inv in enumerate(signals)]
	z = np.array(z)
	fig = mpl.manager.new()
	fig.colormap(
		y = tt.transpose(),
		x = nn.transpose(),
		z = z.transpose(),
	)
	fig.set(
		ylabel = 'Time (s)',
		xlabel = 'Inverter number',
	)
	return fig

def discretize_signal_with_levels(normalized_signal, n_levels: int):
	discretized_signal = copy(normalized_signal)
	s = discretized_signal
	for level in range(n_levels):
		discretized_signal[(s>=((level)/(n_levels-1)-1/2/(n_levels-1))) & (s<((level+1)/(n_levels-1)-1/2/(n_levels-1)))] = level/(n_levels-1)
	discretized_signal *= (n_levels-1)
	discretized_signal[s<=0] = 0
	discretized_signal[s>=n_levels-1] = n_levels-1
	return discretized_signal

def discretize_signal_with_hysteresis(normalized_signal, n_levels: int):
	s = discretize_signal_with_levels(normalized_signal, n_levels)
	falling_windows, rising_windows = find_falling_and_rising_windows(normalized_signal, average_filter_length=20)
	revised_indices = []
	for rwind in rising_windows:
		for n in range(rwind[0], rwind[1]):
			if s[n] > s[n+1]:
				s[n+1] = s[n]
			revised_indices.append(n)
	for fwind in falling_windows:
		for n in range(fwind[0], fwind[1]):
			if s[n] < s[n+1]:
				s[n+1] = s[n]
			revised_indices.append(n)
	non_revised_indices = set([i for i in range(len(normalized_signal))]) - set(revised_indices)
	for k in non_revised_indices:
		if normalized_signal[k] > 1/2:
			s[k] = n_levels-1
		else:
			s[k] = 0
	return s

def average_filter(signal, filter_length: int):
	signal = np.array(signal)
	filtered_signal = signal*0
	if filter_length%2 != 0:
		filter_length += 1
	for n in range(len(signal)):
		if n+1 < (filter_length-1)/2+1:
			filtered_signal[n] = np.nanmean(signal[:n+int((filter_length-1)/2+1)])
		elif len(signal)-n < ((filter_length-1)/2+1):
			filtered_signal[n] = np.nanmean(signal[n-int((filter_length-1)/2+1):])
		else:
			filtered_signal[n] = np.nanmean(signal[int(n-(filter_length-1)/2):int(n+(filter_length-1)/2)])
	return filtered_signal
			

def find_falling_and_rising_windows(normalized_signal, umbral=.9, average_filter_length=5):
	s = average_filter(normalized_signal, average_filter_length)
	is_commuting = (s<umbral)&(s>1-umbral)
	rising = s*0
	rising_windows = []
	falling_windows = []
	temp = []
	for nsamp in range(len(s)):
		if is_commuting[nsamp]:
			temp.append(nsamp)
		if nsamp>0 and is_commuting[nsamp-1] and not is_commuting[nsamp]: # It had just finished a commutation.
			slope = np.diff(s[temp]).mean()
			rising[temp] = 1 if slope > 0 else -1
			if slope > 0:
				rising_windows.append((min(temp),max(temp)))
			elif slope < 0:
				falling_windows.append((min(temp),max(temp)))
			temp = []
		if nsamp==len(s)-1 and is_commuting[nsamp]: # We have reached the end of the data array and it was commuting.
			slope = np.diff(s[temp]).mean()
			rising[temp] = 1 if slope > 0 else -1
			if slope > 0:
				rising_windows.append((min(temp),max(temp)))
			elif slope < 0:
				falling_windows.append((min(temp),max(temp)))
	return falling_windows, rising_windows

def discretized_signals_2_list_of_sequences(discretized_signals: dict):
	temp = []
	for inverter in sorted(discretized_signals.keys()):
		temp.append(discretized_signals[inverter])
	sequences = list(map(list, zip(*temp))) # https://stackoverflow.com/a/6473724/8849755
	return [''.join([str(int(i)) for i in sequence]) for sequence in sequences]

def indices_from_sequence(sequences: list, sequence: str):
	return [idx for idx,seq in enumerate(sequences) if seq==sequence]

def normalize_tdc_inverter_signal(samples, filter_length=50):
	filtered_signal = average_filter(samples, filter_length)
	normalized_signal = copy(samples)
	normalized_signal = (normalized_signal-min(filtered_signal))/max(filtered_signal-min(filtered_signal))
	return normalized_signal
