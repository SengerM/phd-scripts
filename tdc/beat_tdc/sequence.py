
class InvertersSequence:
	def __init__(self, sequence):
		if isinstance(sequence, list):
			sequence = tuple(sequence)
		if isinstance(sequence, tuple):
			if any([not isinstance(i,int) for i in sequence]):
				raise ValueError(f'The sequence must be composed only by integer numbers, received {sequence}.')
		elif isinstance(sequence, str):
			for c in sequence:
				try:
					int(c)
				except:
					raise ValueError(f'The string can only contain integer values, received {sequence}.')
			sequence = tuple([int(i) for i in sequence])
		else:
			raise TypeError(f'The sequence must be a list, a tuple or a string, received {sequence} of type {type(sequence)}.')
		
		self._sequence = sequence
		self._n_levels = max(sequence)+1
		
		if not is_a_good_sequence(self):
			raise ValueError(f'The sequence you have provided is not a "good sequence" in the sense that it is not something that can be expected from a ring of oscillators. The sequence is {self}.')
	
	@property
	def n_levels(self):
		return self._n_levels
	
	def _are_comparable(self, other):
		if self.n_levels != other.n_levels:
			raise ValueError(f'Cannot compare two sequences that are from different discretization levels, received {self} and {other}')
		if len(self._sequence) != len(other._sequence):
			raise ValueError(f'Cannot compare two sequences of different length, received {self} and {other}')
	
	def __eq__(self, other):
		self._are_comparable(other)
		return True if self._sequence == other._sequence else False
	
	def __str__(self):
		string = ''
		for idx,i in enumerate(self._sequence[::-1]):
			string += str(i) if (-1)**idx==1 else str(self._n_levels-1 - i)
		return string
	
	def __getitem__(self, key):
		return self._sequence[key]
	
	def __len__(self):
		return len(self._sequence)
	
	def __hash__(self):
		return int(''.join([str(i) for i in self._sequence]))
	
class TDCInvertersSequenceAndCounter:
	def __init__(self, counter: int, sequence):
		if not isinstance(counter, int):
			raise TypeError(f'<counter> must be an integer number. Received {counter} of type {type(counter)}')
		if counter < 0:
			raise ValueError(f'<counter> must be positive. Received {counter}.')
		self._counter = counter
		self._inverters = InvertersSequence(sequence)
	
	@property
	def counter(self):
		return self._counter
	
	@property
	def inverters(self):
		return self._inverters
	
	@property
	def n_levels(self):
		return self._inverters.n_levels
	
	def __eq__(self, other):
		return True if self.counter == other.counter and self.inverters == other.inverters else False
	
	def __str__(self):
		return f'{self.counter}|{self.inverters}'
	
	def __repr__(self):
		return self.__str__()
	
	def __hash__(self):
		return int(f'{hash(self.counter)}{hash(self.inverters)}')

def find_waves(sequence):
	wave_idxs = []
	for idx,char in enumerate(sequence):
		if idx == 0:
			prev_char = sequence[-1]
		else:
			prev_char = sequence[idx-1]
		if char + prev_char - sequence.n_levels+1 == 0: # There is no wave here.
			continue
		else:
			wave_idxs.append([idx-1,idx])
	return wave_idxs

def is_a_good_sequence(sequence):
	# Returns True if the sequence is something that can be expected from a ring of oscillators, otherwise returns False.
	waves = find_waves(sequence)
	if len(waves) > sequence.n_levels-1:
		return False
	else:
		return True

if __name__ == '__main__':
	sequence1 = TDCInvertersSequenceAndCounter(0,'0010101')
