from tpix_utils import discretize_signal_with_hysteresis, average_filter, discretized_signals_2_list_of_sequences, indices_from_sequence, normalize_tdc_inverter_signal
import numpy as np
import os
from bureaucrat import Bureaucrat
from sequence import TDCInvertersSequenceAndCounter

def script_core(path, n_levels):
	bureaucrat = Bureaucrat(
		path,
		processing_script_path = script_core.__globals__['__file__'],
	)

	dir_for_saving = f'{bureaucrat.processed_data_dir_path}/{n_levels}_discretization_levels'
	if os.path.isdir(dir_for_saving):
		print(f'There is already data processed for this number of discretization levels. If you want to do a new processing please delete the directory {dir_for_saving}.')
		print('Nothing was done, exiting...')
		exit()

	# Read and prepare data ----
	times = {}
	normalized_inverters = {}
	counters = {}
	for fname in sorted(os.listdir(bureaucrat.raw_data_dir_path)):
		data = np.genfromtxt(f'{bureaucrat.raw_data_dir_path}/{fname}').transpose()
		times[fname] = data[0]
		counters[fname] = list(data[1])
		for idx,c in enumerate(counters[fname]):
			if c == c: # if c is not NaN:
				counters[fname][idx] = int(c)
		normalized_inverters[fname] = {}
		for inverter_number in range(len(data)-2):
			inverter_name = f'inverter{inverter_number+1}'
			normalized_inverters[fname][inverter_name] = normalize_tdc_inverter_signal(data[inverter_number+2])
			
	# Find sequences in each file ----
	sequences = {}
	for fname in normalized_inverters.keys():
		discretized_signals = {}
		for inverter in normalized_inverters[fname].keys():
			discretized_signals[inverter] = discretize_signal_with_hysteresis(
				normalized_inverters[fname][inverter], 
				n_levels
			)
		sequences_in_this_file = discretized_signals_2_list_of_sequences(discretized_signals)
		sequences[fname] = []
		for k in range(len(sequences_in_this_file)):
			if not isinstance(counters[fname][k], int): # if it is NaN:
				continue
			sequences[fname].append(TDCInvertersSequenceAndCounter(counter = int(counters[fname][k]), sequence = sequences_in_this_file[k]))
	
	# Combine sequences ----
	set_of_sequences = set()
	for fname in sequences.keys():
		for sequence in sequences[fname]:
			set_of_sequences.add(sequence)

	# Find indices of appearence for each sequence combining all the fnames ----
	sequence_indices = {}
	for fname in normalized_inverters.keys():
		for sequence in set_of_sequences:
			if sequence_indices.get(sequence) == None: # Initialize for this sequence.
				sequence_indices[sequence] = []
			sequence_indices[sequence].append(indices_from_sequence(sequences[fname], sequence))
	for sequence in set_of_sequences: # Make a list from a list of lists.
		sequence_indices[sequence] = [
			item for sublist in sequence_indices[sequence] for item in sublist
		]

	# Save indices of sequences to file ----
	os.makedirs(dir_for_saving)
	print(f'Created directory "{dir_for_saving}" where the results will be saved')
	for sequence in set_of_sequences:
		with open(f'{dir_for_saving}/{sequence}.txt', 'w') as ofile:
			print('#Index\tTime (s)', file = ofile)
			for index in sequence_indices[sequence]:
				print(f'{int(index)}\t{int(index)*10e-12}', file = ofile) # The time is hardcoded, in the future a better implementation can be done.

if __name__ == '__main__':
	import argparse
	
	########################################################################

	parser = argparse.ArgumentParser(description='Analyzes one raw data set from a tpix structure (from a single TDC) to find the distribution of sequences. As a result, this script produces a new directory containing one file for each sequence found, and each file contains the indices at which such sequence appeared.')
	parser.add_argument('-dir',
		metavar = 'path', 
		help = 'Path to directory containing the measurement data (not the raw data but the measurement base directory)',
		required = True,
		dest = 'path',
	)
	parser.add_argument('-nlevels',
		metavar = 'n', 
		help = 'Number of discrete levels to use (e.g. 2, 3, 4, 5...)',
		required = True,
		dest = 'n_levels',
		type = int,
	)

	########################################################################

	args = parser.parse_args()
	script_core(
		path = args.path,
		n_levels = args.n_levels,
	)
