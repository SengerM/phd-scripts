import numpy as np
import myplotlib as mpl
import os
from bureaucrat import Bureaucrat
from sequence import TDCInvertersSequenceAndCounter
from tpix_utils import average_filter

def calculate_time_resolution(times):
	times = np.array(times)
	if len(times) < 2:
		return float('NaN')
	resolution = np.quantile(times, q = .9) - np.quantile(times, q = .1)
	return resolution if 0 < resolution < 99e-12 else float('NaN')

def script_core(path):
	bureaucrat = Bureaucrat(
		path,
		processing_script_path = script_core.__globals__['__file__'],
	)

	if not os.path.isdir(f'{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution'):
		raise ValueError(f'There is no data processed by the script "sequences_distribution.py". I know this becauseh the directory "{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution" does not exist.')
	
	n_levels = []
	for fname in sorted(os.listdir(f'{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution')):
		n_levels.append(int(fname.replace('_discretization_levels','')))
	
	# Read data ---
	sequences_indices = {}
	sequences_times = {}
	sequences = {}
	mean_times = {}
	time_resolution = {}
	for n in n_levels:
		sequences_indices[n] = {}
		sequences_times[n] = {}
		sequences[n] = []
		mean_times[n] = []
		time_resolution[n] = {}
		for fname in sorted(os.listdir(f'{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution/{n}_discretization_levels')):
			data = np.genfromtxt(f'{bureaucrat.measurement_base_path}/processed_by_script_sequences_distribution/{n}_discretization_levels/{fname}').transpose()
			if data[0].shape == (): # This means that the sequence appeared only once. It is irrelevant, and introduces a complication for plotting.
				continue
			sequence = TDCInvertersSequenceAndCounter(counter = int(fname.replace('.txt','').split('.')[0]), sequence = fname.replace('.txt','').split('.')[1])
			sequences[n].append(sequence)
			sequences_indices[n][sequence] = data[0].astype(int)
			sequences_times[n][sequence] = data[1]
			mean_times[n].append(sequences_times[n][sequence].mean())
			time_resolution[n][sequence] = calculate_time_resolution(sequences_times[n][sequence])
		sequences[n] = [x for _,_,x in sorted(zip(mean_times[n],range(len(mean_times[n])),sequences[n]))] # Sort sequences according to mean time.
	
	std_vs_mean_fig = mpl.manager.new(
		title = 'Time resolution vs time',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'Sequence mean time (s)',
		ylabel = 'Sequence temporal resolution (s)',
	)
	for n in n_levels:
		std_vs_mean_fig.plot(
			[sequences_times[n][s].mean() for s in sequences[n]],
			[time_resolution[n][s] for s in sequences[n]],
			marker = '.',
			label = f'{n} levels',
			color = (((n-min(n_levels))/(max(n_levels)-min(n_levels)))**2,0,((max(n_levels)-n)/(min(n_levels)-max(n_levels)))**2),
			alpha = .3,
		)
		std_vs_mean_fig.plot(
			[sequences_times[n][s].mean() for s in sequences[n]],
			average_filter([time_resolution[n][s] for s in sequences[n]],22),
			marker = '.',
			label = f'{n} levels averaged',
			color = (((n-min(n_levels))/(max(n_levels)-min(n_levels)))**2,0,((max(n_levels)-n)/(min(n_levels)-max(n_levels)))**2),
		)
	
	time_resolution_histogram = mpl.manager.new(
		title = f'Distribution of time resolution',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'Temporal resolution each sequence (s)',
		ylabel = 'Density of occurrences',
		yscale = 'lin',
	)
	bins = [10e-12*i-5e-12 for i in range(11)]
	for n in n_levels:
		stuff_for_the_histogram = np.array([time_resolution[n][s] for s in sequences[n]])
		stuff_for_the_histogram = stuff_for_the_histogram[~np.isnan(stuff_for_the_histogram)]
		time_resolution_histogram.hist(
			stuff_for_the_histogram,
			label = f'{n} levels',
			bins = bins,
			density = True,
			# ~ color = (np.sin(np.pi/2*n/max(n_levels))**2,np.sin(np.pi*n/max(n_levels))**3,np.cos(np.pi/2*n/max(n_levels))**2),
		)
	
	# Save data ---
	mpl.manager.save_all(mkdir = f'{bureaucrat.processed_data_dir_path}')
	print(f'Plot has been saved in {bureaucrat.processed_data_dir_path}')

if __name__ == '__main__':
	import argparse
	mpl.manager.set_plotting_package('plotly')
	
	########################################################################

	parser = argparse.ArgumentParser(description='Calculates the time resolution usin the results from script "sequences_distribution.py".')
	parser.add_argument(
		'-dir',
		metavar = 'path', 
		help = 'Path to measurement directory',
		required = True,
		dest = 'path',
	)

	########################################################################

	args = parser.parse_args()
	script_core(
		path = args.path,
	)
