import numpy as np
from tpix_utils import read_data, discretize_signal_with_hysteresis, normalize_tdc_inverter_signal, discretize_signal_with_levels
import myplotlib as mpl
import argparse
from bureaucrat import Bureaucrat

########################################################################

parser = argparse.ArgumentParser(description='Produce plots with discretized signal from raw data')
parser.add_argument(
	'-dir',
	metavar = 'path', 
	help = 'Path to a measurement dir.',
	required = True,
	dest = 'path',
)
parser.add_argument(
	'-nfile',
	metavar = 'n',
	help = 'Number of file to visualize (e.g.33). Default is 1.',
	dest = 'n_file',
	type = int,
	required = False,
	default = 1,
)
parser.add_argument(
	'-ninv',
	metavar = 'n',
	help = 'Each TDC has 9 inverters, and this script will take one of these to make the plots. Which inverter? (Default is inverter 1)',
	default = 1,
	dest = 'n_inv',
	required = False,
	type = int,
)
parser.add_argument(
	'-nlevels',
	metavar = 'n',
	help = 'Number of levels for the discretization. Default is 2.',
	default = 2,
	dest = 'n_levels',
	required = False,
	type = int,
)

########################################################################

args = parser.parse_args()
bureaucrat = Bureaucrat(args.path)

mpl.manager.set_plotting_package('plotly')

data = np.genfromtxt(f'{bureaucrat.raw_data_dir_path}/{args.n_file:0>5}.txt').transpose()
time = data[0]
counter = data[1].astype(int)
inverters = {}
for inverter_number in range(9):
	inverter_name = f'INVERTER{inverter_number+1}'
	inverters[inverter_name] = data[inverter_number+2]

normalized_inverters = {}
for inverter in inverters.keys():
	normalized_inverters[inverter] = normalize_tdc_inverter_signal(inverters[inverter])

demonstration_fig = mpl.manager.new(
	title = f'Discretization demonstration',
	# ~ subtitle = f'Measurement {bureaucrat.measurement_name}, file {args.n_file:0>5}.txt, inverter {args.n_inv}',
	show_title = False,
	xlabel = 'Δt = t<sub>START</sub> - t<sub>STOP</sub> (s)',
	ylabel = 'Normalized amplitude',
)
demonstration_fig.plot(
	time,
	normalized_inverters[f'INVERTER{args.n_inv}'],
	label = 'Normalized raw data',
	marker = '.',
)
for n in [2,3,4,5]:
	demonstration_fig.plot(
		time,
		discretize_signal_with_hysteresis(normalized_inverters[f'INVERTER{args.n_inv}'], n)/(n-1),
		label = f'{n} levels digitized signal',
		marker = '.',
	)
# ~ demonstration_fig.plot(
	# ~ time,
	# ~ discretize_signal_with_hysteresis(normalized_inverters[f'INVERTER{args.n_inv}'], args.n_levels)/(args.n_levels-1),
	# ~ label = f'{args.n_levels} levels digitized signal',
	# ~ marker = '.',
# ~ )
# ~ for level in range(args.n_levels-1):
	# ~ demonstration_fig.plot(
		# ~ [min(time), max(time)],
		# ~ [(level+1)/(args.n_levels-1)-1/2/(args.n_levels-1)]*2,
		# ~ linestyle = '--',
		# ~ color = (0,)*3,
		# ~ linewidth = .5,
		# ~ alpha = .5,
	# ~ )

mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
print(f'Plot was saved in {bureaucrat.processed_data_dir_path}')
mpl.manager.show()
