import numpy as np
import os
from bureaucrat import Bureaucrat

def fix_counter(raw_counter: list):
	FORWARD_BACKWARD_WINDOW = 5
	raw_counter = np.array(raw_counter)
	fixed_counter = list(raw_counter)
	fixed_counter = np.array(fixed_counter)
	conflictive_indices = list(np.nonzero(np.diff(raw_counter))[0])
	for k2check in list(np.array(conflictive_indices)):
		if k2check not in conflictive_indices:
			continue
		k_forward = np.array([k2check+i+1 for i in range(FORWARD_BACKWARD_WINDOW)])
		k_backward = np.array(sorted([k2check-i-1 for i in range(FORWARD_BACKWARD_WINDOW)]))
		k_forward = np.delete(k_forward, np.where(k_forward >= len(raw_counter)))
		k_backward = np.delete(k_backward, np.where(k_backward < 0))
		if (all([raw_counter[k2check]==raw_counter[i] for i in k_backward]) and all([raw_counter[k2check]==raw_counter[i]-1 for i in k_forward])) or (all([raw_counter[k2check]==raw_counter[i]+1 for i in k_backward]) and all([raw_counter[k2check]==raw_counter[i] for i in k_forward])):
			# This means that there are no problems for this k2check. It is just the normal change from one value to another of the counter. 
			conflictive_indices.remove(k2check)
			continue
		# If we are here, there are problems around...
		if list(raw_counter[:k2check]).count(raw_counter[k2check-1]) > FORWARD_BACKWARD_WINDOW and list(raw_counter[k2check:]).count(raw_counter[k2check-1]) > FORWARD_BACKWARD_WINDOW:
			# This is fixable, it is just some values in the middle that have gone crazy.
			k2fix = k2check
			conflictive_indices.remove(k2check)
			while raw_counter[k2fix] != raw_counter[k2check-1] or raw_counter[k2check] != raw_counter[k2fix+1]:
				fixed_counter[k2fix] = raw_counter[k2check-1]
				k2fix += 1
				try:
					conflictive_indices.remove(k2fix)
				except ValueError:
					pass
		elif list(raw_counter[:k2check]).count(raw_counter[k2check]) > FORWARD_BACKWARD_WINDOW:
			fixed_counter[k2check] = raw_counter[k2check]
			conflictive_indices.remove(k2check)
		else:
			fixed_counter[k2check] = -1
	return [int(c) if c >= 0 else float('NaN') for c in fixed_counter]

def script_core(path, fname):
	bureaucrat = Bureaucrat(
		path,
		processing_script_path = script_core.__globals__['__file__'],
	)
	raw_data = np.genfromtxt(f'{bureaucrat.raw_data_dir_path}/{fname}').transpose()
	raw_counter = raw_data[1]
	fixed_counter = fix_counter(raw_counter)
	with open(f'{bureaucrat.raw_data_dir_path}/{fname}', 'w') as ofile:
		print('# Time	Counter1	Inverter 2	Inverter 3	Inverter 4	Inverter 5	Inverter 6	Inverter 7	Inverter 8	Inverter 9', file = ofile)
		for k in range(len(raw_data[0])):
			print(f'{int(raw_data[0][k])}\t{fixed_counter[k]}\t' + '\t'.join([str(int(raw_data[i+2][k])) for i in range(9)]), file = ofile)

if __name__ == '__main__':
	import argparse
	
	########################################################################

	parser = argparse.ArgumentParser(description='Reads the raw data and fixes the problem with the values of the counter that sometimes randomly go crazy. The raw files are overwritten with the fixed values.')
	parser.add_argument(
		'-dir',
		metavar = 'path', 
		help = 'Path to a measurement dir.',
		required = True,
		dest = 'path',
	)
	parser.add_argument(
		'-n',
		metavar = 'n',
		help = 'Number of file to fix (e.g.33). Default is 1. If n<0 the script is applied to all the raw files.',
		dest = 'n_file',
		type = int,
		required = False,
		default = 1,
	)

	########################################################################

	args = parser.parse_args()
	if args.n_file < 0:
		for fname in sorted(os.listdir(f'{args.path}/raw')):
			print(f'Fixing {fname}...')
			script_core(
			path = args.path,
			fname = fname,
		)
	else:
		print(f'Fixing {args.n_file:0>5}.txt...')
		script_core(
			path = args.path,
			fname = f'{args.n_file:0>5}.txt',
		)
