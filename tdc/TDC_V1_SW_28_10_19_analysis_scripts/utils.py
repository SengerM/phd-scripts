def validate_SAFF_string(SAFF: str):
	# This assumes that SAFF already contains only '0' and '1', and it
	# checks that they are properly grouped. E.g.:
	#     00001111 is a good string
	#     00000001 is a good string
	#     00000000 is good
	#     11111111 is good
	#     10111111 is NOT good
	if len(set(SAFF)) == 1:
		return True
	if len(set(SAFF[:SAFF.rfind('0')+1]))==1 and len(set(SAFF[SAFF.find('1'):]))==1:
		return True
	if len(set(SAFF[:SAFF.rfind('1')+1]))==1 and len(set(SAFF[SAFF.find('0'):]))==1:
		return True
	return False

class SAFFData:
	def __init__(self, SAFF_sequence: str):
		if not isinstance(SAFF_sequence, str):
			raise TypeError(f'<SAFF_sequence> must be a string.')
		if set(SAFF_sequence) not in [set(['0','1']), set('0'), set('1')]:
			raise ValueError(f'<SAFF_sequence> must be a string containing a binary sequence with only "0"s and "1"s, received "{SAFF_sequence}".')
		SAFF_sequence = SAFF_sequence
		
		self._sequence = SAFF_sequence
		
	def __str__(self):
		return self._sequence if not self.isnan else f'({self._sequence})'
	
	def __repr__(self):
		return self.__str__()
	
	def __len__(self):
		return len(self._sequence)
	
	def _check_comparable(self, other):
		if len(self) != len(other):
			raise RuntimeError(f'Cannot compare SAFF sequences "{self}" and "{other}" because they have different lengths (i.e. they belong to different spaces).')
	
	def __eq__(self, other):
		self._check_comparable(other)
		return self._sequence == other._sequence
	
	def __lt__(self, other):
		self._check_comparable(other)
		if self.isnan or other.isnan:
			raise RuntimeError(f'Cannot compare SAFF sequences "{self}" and "{other}" because at least one of them is NaN (i.e. an invalid sequence).')
		if self == other:
			return False
		self_zeros_indices = [index for index, character in enumerate(self.bits_sequence) if character=='0']
		other_zeros_indices = [index for index, character in enumerate(other.bits_sequence) if character=='0']
		if len(self_zeros_indices) == 0:
			return False
		if len(other_zeros_indices) == 0:
			return True
		return sum(self_zeros_indices)/len(self_zeros_indices) > sum(other_zeros_indices)/len(other_zeros_indices)
	
	@property
	def bits_sequence(self):
		return self._sequence
	
	@property
	def isnan(self):
		return not validate_SAFF_string(self._sequence)

class TDCOutputDataType:
	def __init__(self, COUNT: int, SAFF: str):
		try:
			COUNT = int(COUNT)
		except:
			raise TypeError(f'<COUNT> must be an integer number. Received {COUNT} of type {type(COUNT)}.')
		self.SAFF = SAFFData(SAFF)
		self.COUNT = COUNT
	
	def _check_comparable(self, other):
		try:
			self.SAFF._check_comparable(other.SAFF)
		except:
			raise RuntimeError(f'Cannot compare {self} with {other} because it makes no sense this comparison.')
	
	def __str__(self):
		return f'{self.COUNT}|{self.SAFF}'
	
	def __repr__(self):
		return self.__str__()
	
	def __eq__(self, other):
		return self.COUNT == other.COUNT and self.SAFF == other.SAFF
	
	def __lt__(self, other):
		if self.COUNT == other.COUNT:
			return self.SAFF < other.SAFF
		else:
			return self.COUNT < other.COUNT
	
	def __hash__(self):
		return int(f'{self.COUNT:b}{self.SAFF.bits_sequence}')
	
	@property
	def isnan(self):
		return self.SAFF.isnan

if __name__ == '__main__':
	test_strings = [
		'0|000000000000000000001',
		'0|000000000000000000011',
		'0|000000000000000000111',
		'0|000000000000000011111',
		'0|000000000011111111111',
		'0|000000000111111111111',
		'0|000000001111111111111',
		'0|000000111111111111111',
		'0|000001111111111111111',
		'0|000011111111111111111',
		'0|001111111111111111111',
		'0|011111111111111111111',
		'0|111111111111111111111',
		'0|000000000000000111111',
		'0|000000000000001111111',
		'0|000000000000011111111',
		'0|000000000000111111111',
		'0|000000000001111111111',
		'0|000111111111111111111',
		# ~ ##############################
		# ~ '0|000000000000000000001',
		# ~ '0|000000000000000000011',
		# ~ '0|000000000000000000111',
		# ~ '0|000000000000000011111',
		# ~ '0|000000000000000111111',
		# ~ '0|000000000000001111111',
		# ~ '0|000000000000011111111',
		# ~ '0|000000000000111111111',
		# ~ '0|000000000001111111111',
		# ~ '0|000000000011111111111',
		# ~ '0|000000000111111111111',
		# ~ '0|000111111111111111111',
		# ~ '0|001111111111111111111',
		# ~ '0|011111111111111111111',
		# ~ '0|111111111111111111111',
		# ~ '0|000000001111111111111',
		# ~ '0|000000111111111111111',
		# ~ '0|000001111111111111111',
		# ~ '0|000011111111111111111',
	]
	outputs = [TDCOutputDataType(s.split('|')[0], s.split('|')[-1]) for s in test_strings]
	
	for o in sorted(outputs):
		print(o)
