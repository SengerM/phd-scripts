from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
from utils import TDCOutputDataType

TDC_NUMBERS = [2,3,4]

COLORS = [
	np.array((232, 237, 85))/255,
	np.array((255, 98, 87))/255,
	np.array((115, 222, 73))/255,
	np.array((91, 153, 252))/255,
]
COLORS_DARK = [
	np.array((158, 163, 0))/255,
	np.array((161, 18, 8))/255,
	np.array((35, 122, 0))/255,
	np.array((0, 47, 122))/255,
]

COLORS = [tuple(c) for c in COLORS]
COLORS_DARK = [tuple(c) for c in COLORS_DARK]

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	fig_mean = mpl.manager.new(
		title = f'Mean time vs sequence number',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = f'Output',
		ylabel = f'Mean time (s)',
	)
	fig_std = mpl.manager.new(
		title = f'Std vs sequence number',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = f'Output',
		ylabel = f'Std (s)',
	)
	fig_quant = mpl.manager.new(
		title = f'Time resolution vs sequence number',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = f'Output',
		ylabel = f'Time resolution = q<sub>95 %</sub> - q<sub>5 %</sub> (s)',
	)
	fig_quant_distribution = mpl.manager.new(
		title = f'Time resolution histograms',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = f'Time resolution = q<sub>95 %</sub> - q<sub>5 %</sub> (s)',
		ylabel = f'Number of output sequences',
	)
	# Read data ---
	data_per_TDC = {}
	all_observed_outputs = set()
	for n_TDC in TDC_NUMBERS:
		try:
			data_per_TDC[n_TDC] = pandas.read_csv(
				bureaucrat.processed_by_script_dir_path('calculate_time_resolution.py')/Path(f'time_resolution_data_TDC_{n_TDC}.csv'),
				dtype = str, # Read everything as string to avoid errors in converting binary to decimals.
			)
			data_per_TDC[n_TDC]['q95-q05 (s)'] = data_per_TDC[n_TDC]['q95-q05 (s)'].apply(lambda x: float(x))
		except FileNotFoundError as e:
			print(f'Cannot process data for TDC number {n_TDC}, reason: {e}')
			continue
		data_per_TDC[n_TDC]['Output'] = data_per_TDC[n_TDC]['Output'].apply(lambda x: TDCOutputDataType(COUNT=x.split('|')[0], SAFF=x.split('|')[-1].replace('(','').replace(')','')))
		data_per_TDC[n_TDC] = data_per_TDC[n_TDC][data_per_TDC[n_TDC]['Output'].apply(lambda x: x.COUNT<20)]
		data_per_TDC[n_TDC]['Output'] = data_per_TDC[n_TDC]['Output'].apply(lambda x: TDCOutputDataType(x.COUNT-1, str(x.SAFF)) if (n_TDC in [2,3,4] and x.COUNT%2 and not x.isnan) else x) # This is because of the bug in the bit 0 of the counter.
		all_observed_outputs |= set(data_per_TDC[n_TDC]['Output'])
	all_observed_outputs = sorted([o for o in all_observed_outputs if not o.isnan]) + [o for o in all_observed_outputs if o.isnan]
	
	unified_data = pandas.DataFrame({'Output': all_observed_outputs})
	unified_data.set_index('Output', inplace=True)
	for n_TDC in TDC_NUMBERS:
		data_per_TDC[n_TDC].set_index('Output', inplace=True)
		data_per_TDC[n_TDC] = data_per_TDC[n_TDC][~data_per_TDC[n_TDC].index.duplicated(keep='first')]
		unified_data = pandas.concat([unified_data, data_per_TDC[n_TDC]], axis=1)
		unified_data.rename(columns={'Mean time (s)': f'Mean time TDC {n_TDC} (s)', 'std (s)': f'std TDC {n_TDC} (s)', 'q95-q05 (s)': f'q95-q05 TDC {n_TDC} (s)'}, inplace=True)
	
	for n_TDC in TDC_NUMBERS:
		fig_mean.plot(
			[f'{o}' for o in unified_data.index],
			unified_data[f'Mean time TDC {n_TDC} (s)'],
			marker = '.',
			label = f'TDC {n_TDC}',
			color = COLORS[n_TDC-1],
		)
		fig_std.plot(
			[f'{o}' for o in unified_data.index],
			unified_data[f'std TDC {n_TDC} (s)'],
			marker = '.',
			label = f'TDC {n_TDC}',
			color = COLORS[n_TDC-1],
		)
		fig_quant.plot(
			[f'{o}' for o in unified_data.index],
			unified_data[f'q95-q05 TDC {n_TDC} (s)'],
			marker = '.',
			label = f'TDC {n_TDC}',
			color = COLORS[n_TDC-1],
		)
		fig_quant.plot(
			[f'{o}' for o in unified_data.index],
			pandas.Series([q if q<100e-12 else float('NaN') for q in unified_data[f'q95-q05 TDC {n_TDC} (s)']]).rolling(window=55, center=True, min_periods=1).mean(),
			marker = '.',
			label = f'TDC {n_TDC} average',
			color = COLORS_DARK[n_TDC-1],
		)
		fig_quant_distribution.hist(
			unified_data[f'q95-q05 TDC {n_TDC} (s)'],
			label = f'TDC {n_TDC}',
			color = COLORS[n_TDC-1],
		)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Parses the raw data to get parameters such as amplitude, collected charge, etc.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)
