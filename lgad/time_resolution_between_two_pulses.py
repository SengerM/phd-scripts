import numpy as np
import myplotlib as mpl
import os
from bureaucrat import Bureaucrat
from lgadtools.LGADSignal import LGADSignal
import argparse
from lmfit import Model

########################################################################

parser = argparse.ArgumentParser(description='Process data consisting of time correlated pulses to get the time resolution of two LGAD detectors. Raw data must be separated in two directories called raw/A and raw/B each containing the raw txt files from the oscilloscope.')
parser.add_argument('-dir',
	metavar = 'path', 
	help = 'Path to the base measurement directory. The raw data must be in a sub directory called "raw" inside subdirectories called "A" and "B" containing each all the raw pulses. The name of the files must be an integer number, e.g. if the oscilloscope produced files names "C1_00000.txt" and "C2_00000.txt" you must rename them to "00000.txt" in each directory A and B. There is a small function within this script that can help you to do this.',
	required = True,
	dest = 'path',
)
parser.add_argument('-preprocess-raw-files',
	help = 'If this argument is provided, the raw data is separated in two directories A and B and renamed with just numbers for the files. This has to be done the first time you analyze the data coming from the oscilloscope.',
	required = False,
	dest = 'preprocess_raw_files',
	action = 'store_true',
)
parser.add_argument('-invert',
	help = 'If this argument is provided, the raw data is multiplied by -1 when reading. This is for negative pulses. Default is not to invert.',
	required = False,
	dest = 'invert',
	action = 'store_true',
)
parser.add_argument('-delimiter',
	metavar = 'delimiter', 
	help = 'Delimiter in the raw txt files (e.g. , \\t |). Default is \\t',
	dest = 'delimiter',
	type = str,
	default = '\t',
)
parser.add_argument('-nbins',
	metavar = 'N', 
	help = 'Number of bins to use in the time resolution histograms. Default is 99.',
	dest = 'nbins',
	type = int,
	default = '99',
)
parser.add_argument('-identical',
	help = 'Provide this flag if the two devices in the measurement were identical. In this way, the time resolution will be divided by sqrt(2). Otherwise it will not.',
	required = False,
	dest = 'identical',
	action = 'store_true',
)

########################################################################

args = parser.parse_args()
bureaucrat = Bureaucrat(
	measurement_base_path = args.path,
)
if not os.path.exists(f'{bureaucrat.measurement_base_path}/devices.txt'):
	with open(f'{bureaucrat.measurement_base_path}/devices.txt', 'w') as ofile:
		print('Device A: ', file = ofile)
		print('Device B: ', file = ofile)
	raise ValueError(f'Could not find file {bureaucrat.measurement_base_path}/devices.txt. This is a simple file that specifies the name of the device connected to each of the channels of the oscilloscope, named "Device A" and "Device B". An example for this file is:\n\tDevice A: LGAD CNM RUN11478 W5-DA11\n\tDevice B: LGAD CNM RUN11478 W5-DA12\nI have created the file, please complete the data.')
devices_names = {}
with open(f'{bureaucrat.measurement_base_path}/devices.txt', 'r') as ifile:
	for line in ifile:
		if 'Device A:' in line:
			devices_names['A'] = line.replace('Device A: ', '').replace('\n', '')
		if 'Device B:' in line:
			devices_names['B'] = line.replace('Device B: ', '').replace('\n', '')
	if devices_names['A'].replace(' ','') == '' or devices_names['B'].replace(' ','') == '':
		raise ValueError(f'Please complete the file {bureaucrat.measurement_base_path}/devices.txt with the names of the devices.')

def rename_raw_files():
	fnames = {}
	for dvc in ['A','B']:
		fnames = sorted(os.listdir(f'{bureaucrat.raw_data_dir_path}/{dvc}'))
		for fname in fnames:
			new_name = f'{fname[-9:]}'
			os.system(f'mv {bureaucrat.raw_data_dir_path}/{dvc}/{fname} {bureaucrat.raw_data_dir_path}/{dvc}/{new_name}')
			# ~ print(f'File {bureaucrat.raw_data_dir_path}/{dvc}/{fname} was renamed to {bureaucrat.raw_data_dir_path}/{dvc}/{new_name}')

if args.preprocess_raw_files == True:
	if not (os.path.isdir(f'{bureaucrat.raw_data_dir_path}/A') and os.path.isdir(f'{bureaucrat.raw_data_dir_path}/B')):
		os.makedirs(f'{bureaucrat.raw_data_dir_path}/A')
		os.makedirs(f'{bureaucrat.raw_data_dir_path}/B')
		os.system(f'mv {bureaucrat.measurement_base_path}/C1* {bureaucrat.raw_data_dir_path}/A/')
		os.system(f'mv {bureaucrat.measurement_base_path}/C2* {bureaucrat.raw_data_dir_path}/B/')
		print('Renaming raw files...')
		rename_raw_files()
		print('Successfully renamed the raw files')

def parse_individual_signals():
	try:
		os.makedirs(f'{bureaucrat.processed_data_dir_path}/worthless_triggers')
	except:
		pass
	fnames = sorted(os.listdir(f'{bureaucrat.raw_data_dir_path}/A')) # It is assumed that the file names in the "B" directory have the same name.
	for dvc in ['A','B']:
		with open(f'{bureaucrat.processed_data_dir_path}/parsed_{dvc}.txt', 'w') as ofile:
			print(f'# Trigger number\tAmplitude (V)\tNoise (V)\tRise time (s)\tCollected charge (AU)\tTime over 50 % threshold (s)', file = ofile)
	for fname in fnames:
		print(f'Parsing raw data from {fname}...')
		# Verify that the two file names are good:
		try:
			int(fname[:-4])
		except ValueError as ex:
			raise ValueError(f'Wrong file name {fname} for raw data, files must be <integer.txt>, e.g. "00012.txt". You can run this script with the option "-rename_raw_files".')
		if not os.path.exists(f'{bureaucrat.raw_data_dir_path}/B/{fname}'):
			print(f'File {fname} is present in {bureaucrat.raw_data_dir_path}/A but not in {bureaucrat.raw_data_dir_path}/B. Cannot process {fname}, will continue with the next file')
			continue
		# If we are here, all the problems related to the raw data files are assumed to be solved, i.e. we have to open "{bureaucrat.raw_data_dir_path}/A/{fname}" and "{bureaucrat.raw_data_dir_path}/B/{fname}" and process the data.
		signals = {}
		for dvc in ['A','B']:
			data = np.genfromtxt(
				f'{bureaucrat.raw_data_dir_path}/{dvc}/{fname}',
				skip_header = 5,
				delimiter = args.delimiter,
			).transpose()
			signals[dvc] = LGADSignal(
				time = data[0],
				samples = data[1]*(-1 if args.invert == True else 1),
			)
		if not all([signals['A'].worth, signals['B'].worth]): # This means that at least one of the two signals is pure noise.
			fig = mpl.manager.new(
				title = f'Data in {fname}',
				xlabel = 'Time (s)',
				ylabel = 'Amplitude (V)',
				package = 'matplotlib',
			)
			for dvc in ['A','B']:
				fig.plot(
					signals[dvc].t,
					signals[dvc].s,
					label = dvc,
				)
			fig.save(fname = f'{bureaucrat.processed_data_dir_path}/worthless_triggers/{fname[:-4]}.png')
			mpl.manager.delete_all_figs()
			continue # with the next fname
		# If we are here it means that the two signals are good enough to be processed.
		for dvc in ['A','B']:
			with open(f'{bureaucrat.processed_data_dir_path}/parsed_{dvc}.txt', 'a') as ofile:
				print(f'{fname[:-4]}\t{signals[dvc].amplitude}\t{signals[dvc].noise}\t{signals[dvc].risetime}\t{signals[dvc].collected_charge()}\t{signals[dvc].time_over_threshold()}', file = ofile)
		if np.random.rand() < 9/len(fnames):
			for dvc in ['A','B']:
				fig = mpl.manager.new(
					title = f'Signal {fname[:-4]}-{dvc}',
					subtitle = f'Measurement {bureaucrat.measurement_name}, device {devices_names[dvc]}',
					package = 'plotly',
				)
				signals[dvc].plot_myplotlib(fig)
			mpl.manager.save_all(mkdir = f'{bureaucrat.processed_data_dir_path}/processed_signals_plots')
			mpl.manager.delete_all_figs()

def filter_and_plot_individual_distributions():
	trigger_numbers = []
	amplitudes = {}
	noises = {}
	rise_times = {}
	collected_charges = {}
	times_over_threshold = {}
	for dvc in ['A', 'B']:
		trigger_numbers = np.genfromtxt(f'{bureaucrat.processed_data_dir_path}/parsed_{dvc}.txt').transpose()[0].astype(int)
		amplitudes[dvc] = np.genfromtxt(f'{bureaucrat.processed_data_dir_path}/parsed_{dvc}.txt').transpose()[1]
		noises[dvc] = np.genfromtxt(f'{bureaucrat.processed_data_dir_path}/parsed_{dvc}.txt').transpose()[2]
		rise_times[dvc] = np.genfromtxt(f'{bureaucrat.processed_data_dir_path}/parsed_{dvc}.txt').transpose()[3]
		collected_charges[dvc] = np.genfromtxt(f'{bureaucrat.processed_data_dir_path}/parsed_{dvc}.txt').transpose()[4]
		times_over_threshold[dvc] = np.genfromtxt(f'{bureaucrat.processed_data_dir_path}/parsed_{dvc}.txt').transpose()[5]
	
	volt_hist = mpl.manager.new(
		title = f'Measured amplitudes and noises distributions',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'Amplitude (V)',
		xscale = 'log',
		ylabel = 'Number of events',
		package = 'plotly',
	)
	rise_time_hist = mpl.manager.new(
		title = f'Measured rise times distribution',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'Rise time (s)',
		ylabel = 'Number of events',
		package = 'plotly',
	)
	SNR_hist = mpl.manager.new(
		title = 'Measured signal to noise ratio (SNR) distribution',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'SNR',
		ylabel = 'Number of events',
		package = 'plotly',
	)
	collected_charge_hist = mpl.manager.new(
		title = 'Measured collected charge distribution',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'Collected charge (Arbitrary Units)',
		ylabel = 'Number of events',
		package = 'plotly',
	)
	time_over_threshold_hist = mpl.manager.new(
		title = 'Measured time over 20 % threshold',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'Time over 20 % threshold (s)',
		ylabel = 'Number of events',
		package = 'plotly',
	)
	for dvc in ['A', 'B']:
		volt_hist.hist(
			amplitudes[dvc],
			label = f'Amplitude for {devices_names[dvc]}',
		)
		volt_hist.hist(
			noises[dvc],
			label = f'Noise for {devices_names[dvc]}',
		)
		rise_time_hist.hist(
			rise_times[dvc],
			label = f'Rise time for {devices_names[dvc]}',
		)
		SNR_hist.hist(
			amplitudes[dvc]/noises[dvc],
			label = f'SNR for {devices_names[dvc]}',
		)
		collected_charge_hist.hist(
			collected_charges[dvc],
			label = f'Collected charge for {devices_names[dvc]}'
		)
		time_over_threshold_hist.hist(
			times_over_threshold[dvc],
			label = f'Time over 20 % threshold for {devices_names[dvc]}'
		)
	mpl.manager.save_all(mkdir = f'{bureaucrat.processed_data_dir_path}/distributions plots')
	mpl.manager.delete_all_figs()
	
	# Filtering the signals:
	with open(f'{bureaucrat.processed_data_dir_path}/filtered_triggers.txt', 'w') as ofile:
		print(f'# List of triggers after filtering was applied.', file=ofile)
	nice_triggers = []
	for trigger_number in trigger_numbers:
		trigger_number = int(trigger_number)
		is_nice = True
		# PLACE THE FILTERS HERE ↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
		# ~ if amplitudes['B'][np.where(trigger_numbers==trigger_number)] < .5:
			# ~ is_nice = False
		# PLACE THE FILTERS HERE ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
		if is_nice:
			nice_triggers.append(trigger_number)
			with open(f'{bureaucrat.processed_data_dir_path}/filtered_triggers.txt', 'a') as ofile:
				print(f'{trigger_number}', file=ofile)
	
	volt_hist = mpl.manager.new(
		title = f'Filtered amplitudes and noises distributions',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'Amplitude (V)',
		xscale = 'log',
		ylabel = 'Number of events',
		package = 'plotly',
	)
	rise_time_hist = mpl.manager.new(
		title = f'Filtered rise times distribution',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'Rise time (s)',
		ylabel = 'Number of events',
		package = 'plotly',
	)
	SNR_hist = mpl.manager.new(
		title = 'Filtered signal to noise ratio (SNR) distribution',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'SNR',
		ylabel = 'Number of events',
		package = 'plotly',
	)
	collected_charge_hist = mpl.manager.new(
		title = 'Filtered collected charge distribution',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'Collected charge (Arbitrary Units)',
		ylabel = 'Number of events',
		package = 'plotly',
	)
	time_over_threshold_hist = mpl.manager.new(
		title = 'Filtered time over 20 % threshold',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = 'Time over 20 % threshold (s)',
		ylabel = 'Number of events',
		package = 'plotly',
	)
	for dvc in ['A', 'B']:
		volt_hist.hist(
			amplitudes[dvc][[np.argwhere(trigger_numbers==n)[0] for n in nice_triggers]],
			label = f'Amplitude for {devices_names[dvc]}',
		)
		volt_hist.hist(
			noises[dvc][[np.argwhere(trigger_numbers==n)[0] for n in nice_triggers]],
			label = f'Noise for {devices_names[dvc]}',
		)
		rise_time_hist.hist(
			rise_times[dvc][[np.argwhere(trigger_numbers==n)[0] for n in nice_triggers]],
			label = f'Rise time for {devices_names[dvc]}',
		)
		SNR_hist.hist(
			amplitudes[dvc][[np.argwhere(trigger_numbers==n)[0] for n in nice_triggers]]/noises[dvc][[np.argwhere(trigger_numbers==n)[0] for n in nice_triggers]],
			label = f'SNR for {devices_names[dvc]}',
		)
		collected_charge_hist.hist(
			collected_charges[dvc][[np.argwhere(trigger_numbers==n)[0] for n in nice_triggers]],
			label = f'Collected charge for {devices_names[dvc]}'
		)
		time_over_threshold_hist.hist(
			times_over_threshold[dvc][[np.argwhere(trigger_numbers==n)[0] for n in nice_triggers]],
			label = f'Time over 20 % threshold for {devices_names[dvc]}'
		)
	mpl.manager.save_all(mkdir = f'{bureaucrat.processed_data_dir_path}/distributions plots')
	mpl.manager.delete_all_figs()

def extract_cfd_times():
	CFD_values_percentage = [10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90]
	trigger_numbers = np.genfromtxt(f'{bureaucrat.processed_data_dir_path}/parsed_A.txt').transpose()[0] # These are the triggers that look nice.
	try:
		os.makedirs(f'{bureaucrat.processed_data_dir_path}/time_at_kCFD')
	except:
		pass
	for cfd in CFD_values_percentage:
		with open(f'{bureaucrat.processed_data_dir_path}/time_at_kCFD/kCFD={cfd}%.txt', 'w') as ofile:
			print(f'# Trigger number\tt_kCFD A (s)\tt_kCFD B (s)', file = ofile)
	for trig_number in trigger_numbers:
		trig_number = int(trig_number)
		fname = f'{trig_number:0>5}.txt'
		print(f'Extracting time deltas from files A,B/{fname}')
		signals = {}
		for dvc in ['A', 'B']:
			data = np.genfromtxt(
				f'{bureaucrat.raw_data_dir_path}/{dvc}/{fname}',
				skip_header = 5,
				delimiter = args.delimiter,
			).transpose()
			signals[dvc] = LGADSignal(
				time = data[0],
				samples = data[1]*(-1 if args.invert == True else 1),
			)
		for cfd in CFD_values_percentage:
			with open(f'{bureaucrat.processed_data_dir_path}/time_at_kCFD/kCFD={cfd}%.txt', 'a') as ofile:
				print(f'{trig_number}\t{signals["A"].time_at(cfd)}\t{signals["B"].time_at(cfd)}', file = ofile)

def calculate_time_resolution():
	n_bins = args.nbins
	
	def gaussian(x, amp, cen, sigma):
		return amp*np.exp(-(x-cen)**2/2/sigma**2)
	
	filtered_triggers = np.genfromtxt(f'{bureaucrat.processed_data_dir_path}/filtered_triggers.txt').astype(int)
	
	fnames = sorted(os.listdir(f'{bureaucrat.processed_data_dir_path}/time_at_kCFD'))
	k_CFD = []
	jitter = []
	for fname in fnames:
		data = np.genfromtxt(f'{bureaucrat.processed_data_dir_path}/time_at_kCFD/{fname}').transpose()
		time_deltas = data[1][[np.argwhere(data[0]==n)[0] for n in filtered_triggers]]-data[2][[np.argwhere(data[0]==n)[0] for n in filtered_triggers]]
		
		hist, bins = np.histogram(time_deltas, bins = n_bins)
		bins = bins[:-1] + np.diff(bins)[0]/2
		gmodel = Model(gaussian)
		params = gmodel.make_params()
		params['amp'].set(value = max(hist), min = 0)
		params['cen'].set(value = 0, min = -100e-12, max = 100e-12)
		params['sigma'].set(value = 40e-12, min = 0, max = 1000e-12)
		fit_result = gmodel.fit(hist, params, x=bins)
		
		k_CFD.append(int(fname.split('=')[-1].split('%')[0])/100)
		jitter.append(fit_result.params["sigma"].value)
		
		fig = mpl.manager.new(
			title = f'Time difference distribution at kCFD = {int(k_CFD[-1]*100)} %',
			subtitle = f'Measurement {bureaucrat.measurement_name}',
			xlabel = r'Δt (s)',
			ylabel = '# of events',
			package = 'plotly',
		)
		fig.hist(
			time_deltas,
			label = f'Δt @ CFD = {int(k_CFD[-1]*100)} %',
			bins = n_bins,
		)
		fig.plot(
			bins,
			fit_result.best_fit,
			label = f'Fit (σ = {fit_result.params["sigma"].value*1e12:.2f} ps)'
		)
		mpl.manager.save_all(mkdir = f'{bureaucrat.processed_data_dir_path}/jitter_plots')
		mpl.manager.delete_all_figs()
	
	with open(f'{bureaucrat.processed_data_dir_path}/jitter_vs_kCFD.txt', 'w') as ofile:
		print(f'# kCFD\tJitter (s)', file = ofile)
		for k,j in zip(k_CFD,jitter):
			print(f'{k}\t{j}', file = ofile)
	
	time_resolution_plot = mpl.manager.new(
		title = 'Time resolution as function of kCFD' if args.identical==True else 'Time difference stdev as function of kCFD',
		subtitle = f'Measurement {bureaucrat.measurement_name}',
		xlabel = r'k_CFD',
		ylabel = f'σ_Δt' + (' /sqrt(2)' if args.identical==True else '') + ' (ps)',
		package = 'plotly',
	)
	time_resolution_plot.plot(
		k_CFD,
		np.array(jitter)*1e12/(2**.5 if args.identical==True else 1),
		marker = '.',
	)
	mpl.manager.save_all(mkdir = f'{bureaucrat.processed_data_dir_path}/jitter_plots')
	mpl.manager.delete_all_figs()

shal_I_parse_raw_data = True
if os.path.exists(f'{bureaucrat.processed_data_dir_path}/parsed_A.txt') or os.path.exists(f'{bureaucrat.processed_data_dir_path}/parsed_B.txt'):
	if str(input(f'There is already parsed data for this measurement located in\n{bureaucrat.processed_data_dir_path}/parsed_A.txt\n{bureaucrat.processed_data_dir_path}/parsed_B.txt\nDo you want to proceed with the parsing and override these files? (yes) ')).lower() == 'yes':
		shal_I_parse_raw_data = True
	else:
		shal_I_parse_raw_data = False

shal_I_extract_cfd_times = True
if os.path.exists(f'{bureaucrat.processed_data_dir_path}/time_at_kCFD'):
	if str(input(f'There is already timing data processed for this measurement in\n{bureaucrat.processed_data_dir_path}/time_at_kCFD\nDo you want to proceed with the parsing and override these files? (yes) ')).lower() == 'yes':
		shal_I_extract_cfd_times = True
	else:
		shal_I_extract_cfd_times = False

if shal_I_parse_raw_data == True:
	print('Parsing signals...')
	parse_individual_signals()
print('Plotting distributions...')
filter_and_plot_individual_distributions()
if shal_I_extract_cfd_times == True:
	print('Calculating Delta_t for each CFD...')
	extract_cfd_times()
print('Calculating time resolution...')
calculate_time_resolution()
