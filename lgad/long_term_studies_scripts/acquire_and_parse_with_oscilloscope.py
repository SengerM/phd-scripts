from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
from progressreporting.TelegramProgressReporter import TelegramProgressReporter # https://github.com/SengerM/progressreporting
from pathlib import Path
import grafica # https://github.com/SengerM/grafica
import pandas
from lgadtools.LGADSignal import LGADSignal # https://github.com/SengerM/lgadtools
import numpy as np
from TeledyneLeCroyPy import LeCroyWaveRunner640Zi # https://github.com/SengerM/TeledyneLeCroyPy
import datetime

def script_core(directory, N_triggers: int, oscilloscope_visa_resource, oscilloscope_channels: list, invert=False, new_measurement=False):
	"""Acquires and parses `N_triggers` single triggers with the oscilloscope
	and stores the data in a CSV file. 
	- directory: Where to store the data.
	- N_triggers: Number of triggers to acquire.
	- oscilloscope_visa_resource: An object created with the resource
	manager of pyvisa with the Teledyne LeCroy 640Zi oscilloscope to use.
	- oscilloscope_channels: list of integers with the channel numbers
	to acquire and parse.
	- invert: boolean specifying if the voltage signal has to be inverted
	before parsing.
	- new_measurement: boolean for the Bureaucrat."""
	bureaucrat = Bureaucrat(
		directory,
		new_measurement = new_measurement,
		variables = locals(),
	)
	
	try:
		N_triggers = int(N_triggers)
	except:
		raise TypeError(f'<N_triggers> must be an integer number, received object of type {type(N_triggers)}.')
	
	oscilloscope = LeCroyWaveRunner640Zi(oscilloscope_visa_resource)
	
	with open(bureaucrat.processed_data_dir_path/Path('parsed_data.csv'), 'w') as ofile:
		print(f'Event number,Channel number,When,Amplitude (V),Noise (V),Rise time (s),Time over 20% (s),Time over noise (s),Collected charge (V*s)', file=ofile)
		with TelegramProgressReporter(
			N_triggers, 
			TelegramReportingInformation().token, 
			TelegramReportingInformation().chat_id, 
			f'"acquire_and_parse_with_oscilloscope.py", {bureaucrat.measurement_name}',
		) as reporter:
			current_event_number = 0
			while current_event_number < N_triggers:
				current_event_number += 1
				reporter.update(1)
				oscilloscope.wait_for_single_trigger(timeout=60*5) # If after 5 minutes there was no trigger, rise an error here.
				lgad_signals = {}
				for ch in oscilloscope_channels:
					raw_df = pandas.DataFrame(oscilloscope.get_waveform(channel=ch))
					if invert:
						raw_df['Amplitude (V)'] *= -1
					s = LGADSignal(
						time = raw_df['Time (s)'],
						samples = raw_df['Amplitude (V)'],
					)
					try:
						time_over_20_percent = s.find_time_over_threshold(20)
					except:
						time_over_20_percent = float('NaN')
					print(f'{current_event_number},{ch},{datetime.datetime.now()},{s.amplitude},{s.noise},{s.rise_time},{time_over_20_percent},{s.time_over_noise},{s.collected_charge}' , file=ofile)
					lgad_signals[ch] = s
				if np.random.rand() < 33/N_triggers:
					for package in {'plotly','matplotlib'}:
						for ch,s in lgad_signals.items():
							fig = grafica.manager.new(
								title = f'Event number {current_event_number} channel {ch}',
								subtitle = f'Dataset: {bureaucrat.measurement_name}',
								xlabel = 'Time (s)',
								ylabel = 'Amplitude (V)',
								plotter_name = package,
							)
							s.plot_grafica(fig)
					grafica.save_unsaved(mkdir = bureaucrat.processed_data_dir_path/Path('plots of some processed signals'.replace(' ','_')))
	
if __name__ == '__main__':
	import argparse
	import pyvisa
	
	parser = argparse.ArgumentParser(
		description = 'Parses the raw data of a single beta scan of a single detector, i.e. a single channel in the oscilloscope.'
	)
	parser.add_argument(
		'--N_triggers',
		metavar = 'int', 
		help = 'Number of triggers to record and parse.',
		required = True,
		dest = 'N_triggers',
		type = int,
	)
	parser.add_argument(
		'--oscilloscope_visa_name',
		metavar = 'str', 
		help = 'The name you have to plug into the visa resource manager to open the connection to the oscilloscope. Default is "USB0::0x05ff::0x1023::2810N60091::INSTR" which is my oscilloscope in the lab (640Zi).',
		required = False,
		dest = 'oscilloscope_visa_name',
		type = str,
		default = 'USB0::0x05ff::0x1023::2810N60091::INSTR',
	)
	parser.add_argument(
		'--channels',
		metavar = 'int', 
		help = 'Channel numbers to acquire from the oscilloscope, e.g. "--channels 1 2".',
		required = True,
		nargs = '+',
		dest = 'channels',
		type = int,
	)
	parser.add_argument(
		'--invert',
		action = 'store_true',
		dest = 'invert',
	)
	args = parser.parse_args()
	
	script_core(
		directory = Path.home()/Path('cernbox/measurements_data/LGAD/EPR2021_LGAD_long_term_test/beta_scans')/Path(input('Measurement name? ').replace(' ','_')),
		N_triggers = args.N_triggers,
		oscilloscope_visa_resource = pyvisa.ResourceManager().open_resource(args.oscilloscope_visa_name),
		oscilloscope_channels = args.channels,
		invert = args.invert,
		new_measurement = True,
	)
