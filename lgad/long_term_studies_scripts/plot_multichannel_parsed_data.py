from data_processing_bureaucrat.Bureaucrat import Bureaucrat
from pathlib import Path
import pandas
import grafica
import plotly.graph_objects as go

def script_core(directory, csv_sub_path: None):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	if csv_sub_path is None:
		csv_sub_path = Path(bureaucrat.processed_by_script_dir_path('acquire_and_parse_with_oscilloscope.py').parts[-1])/Path('parsed_data.csv')
	else:
		csv_sub_path = Path(csv_sub_path)
	data_df = pandas.read_csv(bureaucrat.measurement_base_path/csv_sub_path)
	
	for column in data_df:
		if column in {'Event number','When','Channel number'}: 
			continue
		fig = grafica.new(
			title = column,
			subtitle = f'Data set: {bureaucrat.measurement_name}',
			xlabel = column,
			ylabel = 'Density of events',
		)
		for ch in sorted(set(data_df['Channel number'])):
			fig.histogram(
				data_df.loc[data_df['Channel number']==ch,column],
				density = True,
				label = f'Histogram CH{ch}',
			)
			fig.KDE(
				data_df.loc[data_df['Channel number']==ch,column], 
				color = fig.traces[-1].color,
				label = f'KDE CH{ch}',
				linestyle = 'dashed',
			)
	
	fig = grafica.new(
		title = 'Signal to noise ratio',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'SNR',
		ylabel = 'Density of events',
	)
	for ch in sorted(set(data_df['Channel number'])):
		fig.histogram(
			data_df.loc[data_df['Channel number']==ch,'Amplitude (V)']/data_df.loc[data_df['Channel number']==ch,'Noise (V)'], 
			density = True,
			label = f'Oscilloscope CH{ch}',
		)
		fig.KDE(
			data_df.loc[data_df['Channel number']==ch,'Amplitude (V)']/data_df.loc[data_df['Channel number']==ch,'Noise (V)'],
			alpha = .7,
			color = fig.traces[-1].color,
		)
	
	for ch in sorted(set(data_df['Channel number'])):
		fig = grafica.new()
		fig.plotly_figure = go.Figure(
			data = go.Splom(
				dimensions = [dict(label=col, values=data_df[col]) for col in data_df if col not in {'Event number','When','Channel number', 'Collected charge (C)'}],
				diagonal_visible = False,
				showupperhalf = False,
				marker = dict(
					# ~ opacity = .05,
					size = 2,
				)
			),
		)
		fig.title = f'Scatter matrix CH{ch}'
		fig.subtitle = f'Data set: {bureaucrat.measurement_name}'
	grafica.save_unsaved(mkdir = bureaucrat.processed_data_dir_path)

########################################################################

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description='Makes plots with the distributions of the quantities parsed by the script "parse_raw_data_of_single_beta_scan.py".')
	parser.add_argument('--dir',
		metavar = 'path', 
		help = 'Path to the base measurement directory.',
		required = True,
		dest = 'path',
		type = str,
	)
	parser.add_argument('--csv-sub-path',
		metavar = 'path', 
		help = 'Path to the CSV file containing the data within the "--dir" base path. If not provided the script will try to use data produced by the script "acquire_and_parse_with_oscilloscope.py".',
		required = False,
		dest = 'csv_path',
		type = str,
		default = None,
	)

	args = parser.parse_args()
	script_core(
		directory = args.path,
		csv_sub_path = args.csv_path,
	)
