from data_processing_bureaucrat.Bureaucrat import Bureaucrat
from pathlib import Path
from CAENpy.CAENDesktopHighVoltagePowerSupply import CAENDesktopHighVoltagePowerSupply, OneCAENChannel
from time import sleep
import grafica
import pandas

def script_core(
	directory, # Where to store the measured data. A new directory will be created.
	caen_channel, # An instance of OneCAENChannel (see https://github.com/SengerM/CAENpy) with the channel to measure.
	voltages: list, # List of float numbers specifying the voltage points to measure.
	current_compliance_amperes: float, # Compliance to set to the output, in amperes.
	caen_ramp_speed: float = 5, # Volts per second.
):
	if not isinstance(caen_channel, OneCAENChannel):
		raise TypeError(f'<caen_channel> must be an instance of {OneCAENChannel}, received object of type {type(caen_channel)}.')
	
	bureaucrat = Bureaucrat(
		directory,
		new_measurement = True,
		variables = locals(),
	)
	
	current_current_compliance = caen_channel.current_compliance
	current_voltage = caen_channel.V_set
	try:
		caen_channel.current_compliance = current_compliance_amperes
		csv_file_path = bureaucrat.processed_data_dir_path/Path('measured_data.csv')
		with open(csv_file_path, 'w') as ofile:
			print(f'Voltage (V),Current (A)', file = ofile)
			for v in voltages:
				caen_channel.ramp_voltage(
					voltage = v,
					ramp_speed_VperSec = caen_ramp_speed,
				)
				sleep(1) # Wait a couple of seconds.
				print(f'{caen_channel.V_mon},{caen_channel.I_mon}', file=ofile)
		df = pandas.read_csv(csv_file_path)
		fig = grafica.new(
			title = f'IV-curve measured with CAEN',
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = f'Voltage (V)',
			ylabel = f'Current (A)',
		)
		fig.scatter(
			df['Voltage (V)'],
			df['Current (A)'],
		)
		grafica.save_unsaved(mkdir=bureaucrat.processed_data_dir_path)
	except Exception as e:
		raise e
	finally:
		caen_channel.current_compliance = current_current_compliance
		caen_channel.ramp_voltage(
			voltage = current_voltage,
			ramp_speed_VperSec = caen_ramp_speed,
		)
	
if __name__ == '__main__':
	import numpy
	
	script_core(
		directory = Path.home()/Path('Desktop')/Path(input('Measurement name: ').replace(' ','_')),
		caen_channel = OneCAENChannel(
			caen = CAENDesktopHighVoltagePowerSupply(ip='130.60.165.119', timeout=11),
			channel_number = 3,
		),
		voltages = numpy.linspace(0,111,5),
		current_compliance_amperes = 1e-6,
	)

