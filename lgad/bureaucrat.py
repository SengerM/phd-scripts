import os
from myplotlib.utils import get_timestamp
import __main__
from shutil import copyfile

class Bureaucrat:
	def __init__(self, measurement_base_path: str):
		if not isinstance(measurement_base_path, str):
			raise TypeError(f'<measurement_base_path> must be a string.')
		
		
		self._timestamp = get_timestamp()
		self._measurement_base_path =  measurement_base_path
		self._raw_data_subdir = 'raw'
		self._processed_data_subdir = f'processed_by_script_{__main__.__file__.replace(".py","")}'
		
		if not os.path.isdir(measurement_base_path):
			raise ValueError(f'Directory {measurement_base_path} does not exist.')
		if ' ' in self.measurement_base_path:
			raise ValueError(f'Please place the data in an absolute path with no blank spaces. Sorry about this, but it is a pain to handle spaces in operating systems.')
		
		self.create_dir_structure_for_new_measurement()
		
		copyfile(
			os.path.join(os.getcwd(), __main__.__file__),
			os.path.join(self.scripts_dir_path, __main__.__file__)
		)
		with open(os.path.join(self.scripts_dir_path, __main__.__file__), 'r+') as f:
			content = f.read()
			f.seek(0, 0)
			f.write(f'# This is a copy of the script used to process data. This copy was automatically made by The Bureaucrat.\n# The timestamp at the moment this script was executed was {self.this_run_timestamp}.' +'\n' + content)
	
	def create_dir_structure_for_new_measurement(self):
		dirs = [
			self.raw_data_dir_path,
			self.processed_data_dir_path,
			self.scripts_dir_path,
		]
		for d in dirs:
			if not os.path.isdir(d):
				os.makedirs(d)
	
	@property
	def raw_data_dir_path(self):
		return os.path.join(self.measurement_base_path,self._raw_data_subdir)
	
	@property
	def processed_data_dir_path(self):
		return os.path.join(self.measurement_base_path,self._processed_data_subdir)
	
	@property
	def scripts_dir_path(self):
		return os.path.join(self.measurement_base_path, 'scripts')
	
	@property
	def measurement_base_path(self):
		return self._measurement_base_path
	
	@property
	def measurement_name(self):
		return self.measurement_base_path.split('/')[-1]
	
	@property
	def this_run_timestamp(self):
		return self._timestamp
