from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
from progressreporting.TelegramProgressReporter import TelegramProgressReporter
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
import zipfile
from lgadtools.LGADSignal import LGADSignal

FLUENCES = [
	'4e14',
	'8e14',
	'15e14',
	'25e14',
]

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	total_raw_files_2_process = 0
	for fluence in FLUENCES:
		for f in (bureaucrat.measurement_base_path/Path(f'raw/HPK_irradiated_{fluence}neqcm-2')).iterdir():
			if f.parts[-1] == 'pics': continue
			total_raw_files_2_process += len(zipfile.ZipFile(f).namelist())
	
	with TelegramProgressReporter(
		total_raw_files_2_process, 
		TelegramReportingInformation().token, 
		TelegramReportingInformation().chat_id, 
		f'HPK2 raw data parsing'
	) as reporter:
		with open(bureaucrat.processed_data_dir_path/Path('parsed_data.csv'), 'w') as ofile:
			print(f'Fluence (neq/cm^2),Vbias (V),Channel number,Event number,Amplitude (V),Noise (V),Rise time (s),Time over 20% (s),Time over noise (s),Collected charge (AU)', file=ofile)
			for fluence in FLUENCES:
				for f in sorted((bureaucrat.measurement_base_path/Path(f'raw/HPK_irradiated_{fluence}neqcm-2')).iterdir()):
					if f.parts[-1] == 'pics': continue
					current_bias_voltage = f.parts[-1].split('_')[-1].replace('V.zip','')
					zip_file = zipfile.ZipFile(f)
					for raw_fname in zipfile.ZipFile(f).namelist():
						current_channel_number = int(raw_fname.split("/")[-1].split("_")[0].replace("C",""))
						current_event_number = int(raw_fname.split("/")[-1].split("_")[1].replace(".txt",""))
						time = []
						volt = []
						ifile = zip_file.read(raw_fname).decode('UTF-8')
						for idx,line in enumerate(ifile.split('\n')):
							if idx<6 or line=='': continue
							time.append(float(line.split('\t')[0]))
							volt.append(float(line.split('\t')[1]))
						s = LGADSignal(
							time = time,
							samples = -1*np.array(volt),
						)
						print(f'Processing fluence {fluence}, Vbias {current_bias_voltage}, file {raw_fname}...')
						try:
							time_over_20_percent = s.find_time_over_threshold(20)
						except:
							time_over_20_percent = float('NaN')
						print(f'{fluence},{current_bias_voltage},{current_channel_number},{current_event_number},{s.amplitude},{s.noise},{s.rise_time},{time_over_20_percent},{s.time_over_noise},{s.collected_charge}' , file=ofile)
						if np.random.rand() < 222/total_raw_files_2_process:
							fig = mpl.manager.new(
								title = f'HPK2 {fluence}neqcm2 {current_bias_voltage}V {raw_fname.split("/")[-1]}',
								xlabel = 'Time (s)',
								ylabel = 'Amplitude (V)',
							)
							s.plot_myplotlib(fig)
							mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('plots of some processed signals'))
						reporter.update(1)
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the directory with the data.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)

