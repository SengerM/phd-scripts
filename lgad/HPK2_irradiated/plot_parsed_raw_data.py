from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
from progressreporting.TelegramProgressReporter import TelegramProgressReporter
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
from scipy.stats.kde import gaussian_kde
from scipy.optimize import minimize

def estimate_collected_charge(samples):
	return minimize(
		lambda x: -gaussian_kde(samples)(x), 
		x0 = np.nanmean(samples),
		method = 'nelder-mead',
		options = {'xatol': 1e-8}
	).x[0]

def script_core(directory, transimpedance=None):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	data = pandas.read_csv(bureaucrat.processed_by_script_dir_path('parse_raw_data.py')/Path('parsed_data.csv'))
	
	for channel in [min, max]:
		amplitude_fig = mpl.manager.new(
			title = f'Amplitude distribution' + (' CH 1' if channel==min else ' CH 2'),
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = 'Amplitude (V)',
			ylabel = 'Number of events',
		)
		SNR_fig = mpl.manager.new(
			title = f'SNR distribution' + (' CH 1' if channel==min else ' CH 2'),
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = 'SNR',
			ylabel = 'Number of events',
		)
		rise_time_fig = mpl.manager.new(
			title = f'Rise time distribution' + (' CH 1' if channel==min else ' CH 2'),
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = 'Rise time (s)',
			ylabel = 'Number of events',
		)
		time_over_20_fig = mpl.manager.new(
			title = f'Time over 20 % distribution' + (' CH 1' if channel==min else ' CH 2'),
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = 'Time over 20 % (s)',
			ylabel = 'Number of events',
		)
		time_over_noise_fig = mpl.manager.new(
			title = f'Time over noise distribution' + (' CH 1' if channel==min else ' CH 2'),
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = 'Time over noise (s)',
			ylabel = 'Number of events',
		)
		collected_charge_fig = mpl.manager.new(
			title = f'Charge distribution' + (' CH 1' if channel==min else ' CH 2'),
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = 'Collected charge (AU)' if transimpedance is None else 'Collected charge (C)',
			ylabel = 'Number of events',
		)
		
		collected_charge_vs_vbias = mpl.manager.new(
			title = f'Charge vs bias voltage' + (' CH 1' if channel==min else ' CH 2'),
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = 'Bias voltage (V)',
			ylabel = 'Collected charge (C)',
			yscale = 'log',
		)
		
		fluences = set(data['Fluence (neq/cm^2)'])
		for fluence in sorted(fluences):
			bias_voltages = sorted(set(data.loc[data['Fluence (neq/cm^2)']==fluence, 'Vbias (V)']))
			collected_charges_to_plot_with_bias_voltages = []
			for Vbias in sorted(bias_voltages):
				channel_numbers = set(data.loc[(data['Fluence (neq/cm^2)']==fluence) & (data['Vbias (V)']==Vbias), 'Channel number'])
				amplitude_fig.hist(
					data.loc[(data['Fluence (neq/cm^2)']==fluence) & (data['Vbias (V)']==Vbias) & (data['Channel number']==channel(channel_numbers)), 'Amplitude (V)'],
					label = f'{fluence:.1e} neq/cm<sup>2</sup>, {Vbias} V',
				)
				# ~ SNR_fig.hist(
					# ~ data.loc[(data['Fluence (neq/cm^2)']==fluence) & (data['Vbias (V)']==Vbias) & (data['Channel number']==channel(channel_numbers)), 'Amplitude (V)']/data.loc[(data['Fluence (neq/cm^2)']==fluence) & (data['Vbias (V)']==Vbias) & (data['Channel number']==channel(channel_numbers)), 'Noise (V)'],
					# ~ label = f'{fluence:.1e} neq/cm<sup>2</sup>, {Vbias} V',
				# ~ )
				rise_time_fig.hist(
					data.loc[(data['Fluence (neq/cm^2)']==fluence) & (data['Vbias (V)']==Vbias) & (data['Channel number']==channel(channel_numbers)), 'Rise time (s)'],
					label = f'{fluence:.1e} neq/cm<sup>2</sup>, {Vbias} V',
				)
				time_over_20_fig.hist(
					data.loc[(data['Fluence (neq/cm^2)']==fluence) & (data['Vbias (V)']==Vbias) & (data['Channel number']==channel(channel_numbers)), 'Time over 20% (s)'],
					label = f'{fluence:.1e} neq/cm<sup>2</sup>, {Vbias} V',
				)
				time_over_noise_fig.hist(
					data.loc[(data['Fluence (neq/cm^2)']==fluence) & (data['Vbias (V)']==Vbias) & (data['Channel number']==channel(channel_numbers)), 'Time over noise (s)'],
					label = f'{fluence:.1e} neq/cm<sup>2</sup>, {Vbias} V',
				)
				transimpedance = 1 if transimpedance is None else transimpedance
				collected_charges = data.loc[(data['Fluence (neq/cm^2)']==fluence) & (data['Vbias (V)']==Vbias) & (data['Channel number']==channel(channel_numbers)), 'Collected charge (AU)']/transimpedance
				collected_charges = collected_charges[~np.isnan(collected_charges)]
				color = tuple(np.random.rand(3))
				collected_charge_fig.hist(
					collected_charges,
					label = f'{fluence:.1e} neq/cm<sup>2</sup>, {Vbias} V',
					color = color,
					density = True,
				)
				collected_charges_to_plot_with_bias_voltages.append(estimate_collected_charge(collected_charges))
			collected_charge_vs_vbias.plot(
				bias_voltages,
				collected_charges_to_plot_with_bias_voltages,
				marker = '.',
				label = f'{fluence:.1e} n<sub>eq</sub>/cm<sup>2</sup>',
			)
		mpl.manager.save_all(mkdir=bureaucrat.processed_data_dir_path/Path(f'distributions plots'))
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the directory with the data.',
		required = True,
		dest = 'directory',
		type = str,
	)
	parser.add_argument(
		'--transimpedance',
		metavar = 'T', 
		help = 'Transimpedance to convert V→I (in Ohm).',
		dest = 'transimpedance',
		type = float,
	)
	args = parser.parse_args()
	script_core(args.directory, args.transimpedance)

