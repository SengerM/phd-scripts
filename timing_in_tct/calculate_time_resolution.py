from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramProgressBar
import numpy as np
import myplotlib as mpl
from lgadtools.LGADSignal import LGADSignal
from pathlib import Path
import pandas
from scipy.stats import norm

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	data = pandas.read_csv(
		bureaucrat.processed_by_script_dir_path('parse_splitted_signals.py')/Path('parsed_data.csv'),
		sep = '\t',
	)
	
	# First I will remove all the triggers that are not paired ---------
	for n_trigger in sorted(set(data['n_trigger'])):
		if 1 in set(data[data['n_trigger']==n_trigger]['n_pulse']) and 2 in set(data[data['n_trigger']==n_trigger]['n_pulse']): # If this happens, the trigger is paired.
			continue
		else: # If we are here, the trigger is not paired and the rows must be removed.
			print(f'Dropping n_trigger={n_trigger} because it is not paired.')
			data = data.drop(
				data[data['n_trigger']==n_trigger].index
			)
	
	k_cfds = []
	for column in data:
		if 't_' in column and '(s)' in column:
			k_cfds.append(int(column[2:4]))
	
	kk_cfd1, kk_cfd2 = np.meshgrid(k_cfds, k_cfds)
	time_difference_sigma = np.array(kk_cfd1).astype(float)
	time_difference_sigma[:] = float('NaN')
	for idx1,k1 in enumerate(k_cfds):
		for idx2,k2 in enumerate(k_cfds):
			times_first_pulse = np.array(data[data['n_pulse']==1][f't_{k1} (s)'])
			times_second_pulse = np.array(data[data['n_pulse']==2][f't_{k2} (s)'])
			Delta_ts = times_second_pulse - times_first_pulse
			# ~ Delta_ts = Delta_ts[Delta_ts.mean() - 2*Delta_ts.std() < Delta_ts]
			# ~ Delta_ts = Delta_ts[Delta_ts < Delta_ts.mean() + 2*Delta_ts.std()]
			mu, sigma = norm.fit(Delta_ts)
			time_difference_sigma[idx2,idx1] = sigma
			fitfig = mpl.manager.new(
				title = f'Time difference for kcfd1={k1} and kcfd2={k2}',
				subtitle = f'Data set: {bureaucrat.measurement_name}',
				xlabel = f'Δt (s)',
				ylabel = f'Density of events',
			)
			fitfig.hist(
				Delta_ts,
				density = True,
				label = 'Measured',
			)
			t_axis = np.linspace(min(Delta_ts), max(Delta_ts), 444)
			fitfig.plot(
				t_axis,
				norm.pdf(t_axis, loc = mu, scale = sigma),
				label = f'Fit (µ={mu:.1e} s, σ={sigma*1e12:.1f} ps)'
			)
			mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('Delta_t fit plots'))
	
	fig = mpl.manager.new(
		title = f'Time resolution at TCT',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = f'k_CFD for pulse 1 (%)',
		ylabel = f'k_CFD for pulse 2 (%)',
		aspect = 'equal',
	)
	fig.contour(
		x = kk_cfd1,
		y = kk_cfd2,
		z = time_difference_sigma/2**.5*1e12,
		colorscalelabel = f'Time resolution (σ/√2) (ps)'
	)
	fig.plot(
		kk_cfd1[time_difference_sigma==time_difference_sigma.min()],
		kk_cfd2[time_difference_sigma==time_difference_sigma.min()],
		marker = '.',
		linestyle = '',
		color = (1,0,0),
		label = f'Minimum σ/√2 = {time_difference_sigma.min()*1e12/2**.5:.1f} ps',
	)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)

########################################################################

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description='Calculates time resolution using data from "parse_splitted_signals.py".')
	parser.add_argument('--dir',
		metavar = 'path', 
		help = 'Path to the base measurement directory.',
		required = True,
		dest = 'path',
	)

	args = parser.parse_args()
	script_core(
		directory = args.path,
	)
