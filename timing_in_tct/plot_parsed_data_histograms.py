from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramProgressBar
import numpy as np
import myplotlib as mpl
from lgadtools.LGADSignal import LGADSignal
from pathlib import Path
import pandas

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	data = pandas.read_csv(
		bureaucrat.processed_by_script_dir_path('parse_splitted_signals.py')/Path('parsed_data.csv'),
		sep = '\t',
	)
	
	for column in data:
		if column in ['n_trigger', 'n_pulse']:
			continue
		fig = mpl.manager.new(
			title = column[:column.find(' (')],
			subtitle = f'Data set: {bureaucrat.measurement_name}',
			xlabel = column,
			ylabel = 'Number of events',
		)
		for n_pulse in sorted(set(data['n_pulse'])):
			fig.hist(
				data[data['n_pulse']==n_pulse][column],
				label = f'Pulse {n_pulse}',
			)
		mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
	

########################################################################

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description='Plot data parsed by "parse_splitted_signals.py" as histograms.')
	parser.add_argument('--dir',
		metavar = 'path', 
		help = 'Path to the base measurement directory.',
		required = True,
		dest = 'path',
	)

	args = parser.parse_args()
	script_core(
		directory = args.path,
	)
