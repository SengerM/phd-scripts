from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramProgressBar
import numpy as np
import myplotlib as mpl
from lgadtools.LGADSignal import LGADSignal
from pathlib import Path
import pandas

K_CFD_VALUES = [10,20,30,40,50,60,70,80,90]

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	splitted_files_directory = bureaucrat.processed_by_script_dir_path('split_raw_signals.py')/Path('splitted_signals')
	ofpath = bureaucrat.processed_data_dir_path/Path('parsed_data.csv')
	with open(ofpath, 'w') as ofile:
		print(
			f'n_trigger\tn_pulse\tAmplitude (V)\tNoise (V)\tRise time (s)\tCollected charge (a.u.)\tTime over 20 % threshold (s)\t' + '\t'.join([f't_{k} (s)' for k in K_CFD_VALUES]),
			file = ofile,
		)
	
	n_files_2_process = len(list(splitted_files_directory.iterdir()))
	with TelegramProgressBar(n_files_2_process, bureaucrat) as pbar:
		for fpath in sorted(splitted_files_directory.iterdir()):
			pbar.update(1)
			n_trigger = int(fpath.parts[-1][:5])
			n_pulse = int(fpath.parts[-1][6])
			data = np.genfromtxt(
				fpath,
				skip_header = 1,
				delimiter = '\t',
			).transpose()
			s = LGADSignal(
				time = data[0],
				samples = -1*data[1],
			)
			if not s.worth:
				fig = mpl.manager.new(
					title = f'Trigger {n_trigger} pulse {n_pulse}',
					subtitle = f'Data set: {bureaucrat.measurement_name}',
					xlabel = 'Time (s)',
					ylabel = 'Amplitude (V)',
					package = 'matplotlib',
				)
				fig.plot(s.t, s.s)
				mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('plots of signals unable to process'))
				continue
			with open(ofpath, 'a') as ofile:
				print(f'{n_trigger:0>5}', file = ofile, end = '\t')
				print(f'{n_pulse}', file = ofile, end = '\t')
				print(f'{s.amplitude:.6e}', file = ofile, end = '\t')
				print(f'{s.noise:.6e}', file = ofile, end = '\t')
				print(f'{s.risetime:.6e}', file = ofile, end = '\t')
				print(f'{s.collected_charge():.6e}', file = ofile, end = '\t')
				print(f'{s.time_over_threshold(20):.6e}', file = ofile, end = '\t')
				for k in K_CFD_VALUES:
					print(f'{s.time_at(k):.6e}', file = ofile, end = '')
					if k == K_CFD_VALUES[-1]:
						break
					print('\t', file = ofile, end = '')
				print('', file = ofile)
			if np.random.rand() < 10/n_files_2_process:
				fig = mpl.manager.new(
					title = f'Trigger {n_trigger} pulse {n_pulse}',
					subtitle = f'Data set: {bureaucrat.measurement_name}',
					xlabel = 'Time (s)',
					ylabel = 'Amplitude (V)',
				)
				s.plot_myplotlib(fig)
				for k in K_CFD_VALUES:
					fig.plot(
						[s.time_at(k)],
						[s.baseline + s.amplitude*k/100],
						marker = 'x',
						color = (0,0,0),
						label = f'k_CFD = {k} %',
					)
				mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('plots of some processed signals'))

########################################################################

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description='Parse signals that have been splitted by the script "split_raw_signals.py".')
	parser.add_argument('--dir',
		metavar = 'path', 
		help = 'Path to the base measurement directory.',
		required = True,
		dest = 'path',
	)

	args = parser.parse_args()
	script_core(
		directory = args.path,
	)
