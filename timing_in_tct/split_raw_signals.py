from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
import myplotlib as mpl
from lgadtools.LGADSignal import LGADSignal
from pathlib import Path

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	raw_files_directory = bureaucrat.measurement_base_path/Path('raw')
	
	n_files_2_process = len(list(raw_files_directory.iterdir()))
	for ifpath in sorted(raw_files_directory.iterdir()):
		current_signal_number = int(ifpath.parts[-1][11:-4])
		raw_data = np.genfromtxt(
			ifpath,
			skip_header = 5,
			delimiter = ',',
		)
		first_half = np.array(raw_data)[:int(len(raw_data)/2)]
		second_half = np.array(raw_data)[int(len(raw_data)/2):]
		
		ofiles_directory = bureaucrat.processed_data_dir_path/Path('splitted_signals')
		ofiles_directory.mkdir(exist_ok = True)
		with open(ofiles_directory/Path(f'{current_signal_number:0>5}_1.txt'), 'w') as ofile:
			print('Time (s)\tAmplitude (V)', file = ofile)
			for line in first_half:
				print(f'{line[0]}\t{line[1]}', file = ofile)
		with open(ofiles_directory/Path(f'{current_signal_number:0>5}_2.txt'), 'w') as ofile:
			print('Time (s)\tAmplitude (V)', file = ofile)
			for line in second_half:
				print(f'{line[0]}\t{line[1]}', file = ofile)

########################################################################

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description='Split signals taken with the delayed laser in the TCT in half.')
	parser.add_argument('--dir',
		metavar = 'path', 
		help = 'Path to the base measurement directory. The raw data must be in a sub directory called "raw".',
		required = True,
		dest = 'path',
	)

	args = parser.parse_args()
	script_core(
		directory = args.path,
	)
