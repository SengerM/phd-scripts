from lgadtools.LGADSignal import LGADSignal
from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
from plot_parsed_data import read_parsed_data, PARSED_VARS, SCENARIOS
import sys
sys.path.append('../xy_scan')
from build_lookup_table import k_yuta

CHANNELS = [f'CH{i+1}' for i in range(4)]

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	parsed = {}
	for scenario in SCENARIOS:
		parsed[scenario] = {}
		for ch in CHANNELS:
			parsed[scenario][ch] = read_parsed_data(bureaucrat.processed_by_script_dir_path('parse_raw_data.py')/Path(f'{scenario}_processed_data/{ch}.txt'))
	charge_fraction = {}
	kx = {}
	ky = {}
	for scenario in SCENARIOS:
		charge_fraction[scenario] = {}
		for ch in CHANNELS:
			if ch == 'CH1':
				total_charge = parsed[scenario][ch]['collected charge']
			else:
				total_charge = total_charge + parsed[scenario][ch]['collected charge']
				# ~ total_charge = 1
			
		for ch in CHANNELS:
			charge_fraction[scenario][ch] = parsed[scenario][ch]['collected charge']/total_charge
		
		kkx, kky = k_yuta(
			QRT = parsed[scenario]['CH3']['collected charge'],
			QRB = parsed[scenario]['CH2']['collected charge'],
			QLT = parsed[scenario]['CH4']['collected charge'],
			QLB = parsed[scenario]['CH1']['collected charge'],
		)
		
		kx[scenario] = kkx
		ky[scenario] = kky
		
	
	fig = mpl.manager.new(
		title = f'Charge fraction distribution',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'Charge fraction',
		ylabel = 'Number of occurrences',
	)
	for scenario in SCENARIOS:
		for ch in CHANNELS:
			fig.hist(
				charge_fraction[scenario][ch],
				label = f'{scenario} {ch}',
			)
	
	kx_fig = mpl.manager.new(
		title = f'kx',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'kx',
		ylabel = 'Number of occurrences',
	)
	ky_fig = mpl.manager.new(
		title = f'ky',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'ky',
		ylabel = 'Number of occurrences',
	)
	for scenario in SCENARIOS:
			kx_fig.hist(
				kx[scenario],
				label = f'{scenario}',
			)
			ky_fig.hist(
				ky[scenario],
				label = f'{scenario}',
			)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Parses the raw data to get parameters such as amplitude, collected charge, etc.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)
