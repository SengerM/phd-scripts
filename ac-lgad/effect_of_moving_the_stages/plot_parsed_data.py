from lgadtools.LGADSignal import LGADSignal
from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl

CHANNELS = [f'CH{i+1}' for i in range(4)]
LABELS = {
	'x': 'x (m)',
	'y': 'y (m)',
	'z': 'z (m)',
	'amplitude': 'Amplitude (V)',
	'noise': 'Noise (V)',
	'rise time': 'Rise time (s)',
	'collected charge': 'Collected charge (arbitrary units)',
	't 10 %': 'time at 10 % (s)',
	't 50 %': 'time at 50 % (s)',
	't 90 %': 'time at 90 % (s)',
}
PARSED_VARS = ['x','y','z','amplitude','noise','rise time','collected charge','t 10 %','t 50 %','t 90 %']
SCENARIOS = ['static_stages', 'non_static_stages']

def read_parsed_data(file: str):
	data = np.genfromtxt(Path(file)).transpose()
	parsed_stuff = {}
	for idx,variable in enumerate(PARSED_VARS):
		parsed_stuff[variable] = data[idx+1]
	return parsed_stuff

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	parsed = {}
	for scenario in SCENARIOS:
		parsed[scenario] = {}
		for ch in CHANNELS:
			parsed[scenario][ch] = read_parsed_data(bureaucrat.processed_by_script_dir_path('parse_raw_data.py')/Path(f'{scenario}_processed_data/{ch}.txt'))
	for variable in PARSED_VARS:
		for ch in CHANNELS:
			histogram = mpl.manager.new(
				title = f'{variable} for {ch}',
				subtitle = f'Data set: {bureaucrat.measurement_name}',
				xlabel = LABELS[variable],
				ylabel = f'Number of occurrences',
			)
			for scenario in SCENARIOS:
				histogram.hist(
					parsed[scenario][ch][variable],
					label = scenario,
				)
			mpl.manager.save_all(mkdir = str(bureaucrat.processed_data_dir_path/Path(variable)))
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Parses the raw data to get parameters such as amplitude, collected charge, etc.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)
