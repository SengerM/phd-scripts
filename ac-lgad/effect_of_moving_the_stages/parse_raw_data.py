from lgadtools.LGADSignal import LGADSignal
from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl

CHANNELS = [f'CH{i+1}' for i in range(4)]

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	for scenario in ['static_stages', 'non_static_stages']:
		(bureaucrat.processed_data_dir_path/Path(f'{scenario}_processed_data')).mkdir()
		for ch in CHANNELS:
			with (bureaucrat.processed_data_dir_path/Path(f'{scenario}_processed_data/{ch}.txt')).open('w') as ofile:
				print(f'# n\tx (m)\ty (m)\tz (m)\tAmplitude (V)\tNoise (V)\tRise time (s)\tCollected charge (arbitrary units)\tt_10 (s)\tt_50 (s)\tt_90 (s)', file = ofile)
		dir_with_raw_data = bureaucrat.processed_by_script_dir_path('measure_static_vs_non_static_stages.py')/Path(f'{scenario}_raw_data')
		n_files_2_process = len(list(dir_with_raw_data.iterdir()))
		for raw_file_path in sorted(dir_with_raw_data.iterdir()):
			n = int(raw_file_path.parts[-1].split('.')[0])
			with raw_file_path.open('r') as ifile:
				_coordinates_parsed = 0
				for line in ifile:
					if 'x_position' in line:
						x = float(line.split('=')[-1])
						_coordinates_parsed += 1
					if 'y_position' in line:
						y = float(line.split('=')[-1])
						_coordinates_parsed += 1
					if 'z_position' in line:
						z = float(line.split('=')[-1])
						_coordinates_parsed += 1
					if _coordinates_parsed == 3:
						break
			if _coordinates_parsed != 3:
				raise RuntimeError(f'Could not parse the three coordinates x,y,z for file {raw_file_path}.')
			raw_data = np.genfromtxt(raw_file_path).transpose()
			time = raw_data[0]
			for idx,channel in enumerate(CHANNELS):
				print(f'Processing {raw_file_path} channel {channel}...')
				s = LGADSignal(
					time = time,
					samples = raw_data[idx+1],
				)
				with (bureaucrat.processed_data_dir_path/Path(f'{scenario}_processed_data/{channel}.txt')).open('a') as ofile:
					print(f'{n}\t{x}\t{y}\t{z}\t{s.amplitude}\t{s.noise}\t{s.risetime}\t{s.collected_charge()}\t{s.time_at(10)}\t{s.time_at(50)}\t{s.time_at(90)}', file = ofile)
				if np.random.rand() < 20/n_files_2_process: # Plot!
					fig = mpl.manager.new(
						title = f'Signal from file {raw_file_path.parts[-1]} for {channel}',
						subtitle = f'Data set: {bureaucrat.measurement_name}',
						xlabel = 'Time (s)',
						ylabel = 'Amplitude (V)',
					)
					s.plot_myplotlib(fig)
					mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path(f'some_plots_{scenario}'), delete_all = True)

if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Parses the raw data to get parameters such as amplitude, collected charge, etc.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)
