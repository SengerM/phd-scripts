from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
from plot_parsed_data import read_parsed_data
from utils import ACLGADxyScan
import scipy.optimize as optimize

CHANNELS = [f'CH{i+1}' for i in range(4)]

def collected_charge_vs_mask_side(scan, sides_m: list, center_m=None):
	if center_m is None:
		center_m = scan.center_m
	collected_charge_inside_mask = {}
	for idx,side_m in enumerate(sides_m):
		mask = scan.swiss_flag_mask(side_m = side_m, center_m = center_m)
		for ch in CHANNELS:
			if ch not in collected_charge_inside_mask:
				collected_charge_inside_mask[ch] = []
			collected_charge_inside_mask[ch].append(
				np.nansum(scan.data[ch]['collected charge']*mask)
			)
	return collected_charge_inside_mask


def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	parsed = {}
	for ch in CHANNELS:
		parsed[ch] = read_parsed_data(bureaucrat.processed_by_script_dir_path('parse_raw_data.py')/Path(f'parsed_attributes_{ch}.txt'))
		parsed[ch]['collected charge'][np.isnan(parsed[ch]['collected charge'])] = 0
	scan = ACLGADxyScan(parsed_data = parsed)
	
	scan.center_m = (
		np.genfromtxt(f'{bureaucrat.processed_by_script_dir_path("fit_swiss_flag.py")}/fitted_swiss_flag_params.txt').transpose()[0].mean(),
		np.genfromtxt(f'{bureaucrat.processed_by_script_dir_path("fit_swiss_flag.py")}/fitted_swiss_flag_params.txt').transpose()[1].mean()
	)
	scan.side_m = np.genfromtxt(f'{bureaucrat.processed_by_script_dir_path("fit_swiss_flag.py")}/fitted_swiss_flag_params.txt').transpose()[2].mean()
	
	fig = mpl.manager.new(
		title = 'Swiss flag before optimization (mean of fitted Swiss flags)',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'x (m)',
		ylabel = 'y (m)',
	)
	scan.plot_data_and_fit(fig)
	
	def function_to_minimize(center_m):
		x_center_m_offset = center_m[0]
		y_center_m_offset = center_m[1]
		mask_sides = np.linspace(0, scan.side_m)
		collected_charge_inside_mask = collected_charge_vs_mask_side(scan, mask_sides, center_m = tuple(np.array(scan.center_m) + np.array((x_center_m_offset,y_center_m_offset))))
		slopes = []
		for ch in CHANNELS:
			ratio_of_collected_charges = np.array([c/ref for c,ref in zip(collected_charge_inside_mask[ch],collected_charge_inside_mask['CH1'])])
			indices_for_fit = np.isfinite(ratio_of_collected_charges) # I have to remove infinites and NaN values.
			fitted_coefs = np.polyfit(mask_sides[indices_for_fit], ratio_of_collected_charges[indices_for_fit], 1)
			slopes.append(fitted_coefs[0])
		return (np.array(slopes)**2).sum()
	
	optimization_plot = mpl.manager.new(
		title = 'Quantity to minimize',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		aspect = 'equal',
		xlabel = f'Center x shift (m)',
		ylabel = f'Center y shift (m)',
	)
	xx,yy = np.meshgrid(np.linspace(-1e-6,1e-6,99),np.linspace(-1e-6,1e-6,99))
	zz = np.array([[function_to_minimize((x,y)) for x,y in zip(xrow,yrow)] for xrow,yrow in zip(xx,yy)])
	optimization_plot.colormap(
		x = xx,
		y = yy,
		z = zz,
		norm = 'log',
	)
	optimized_center_m = np.array(scan.center_m) + np.array((xx.ravel()[np.argmin(zz)], yy.ravel()[np.argmin(zz)]))
	optimization_plot.plot(
		[optimized_center_m[0] - scan.center_m[0]],
		[optimized_center_m[1] - scan.center_m[1]],
		label = f'Optimal center shift',
		marker = 'x',
	)
	
	collected_charge_vs_mask_size_fig = mpl.manager.new(
		title = f'Collected charge ratios vs mask size after optimization',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'Swiss flag side (m)',
		ylabel = 'Q(CHi)/Q(CH1)',
	)
	mask_sides = np.linspace(0, scan.side_m)
	collected_charge_inside_mask = collected_charge_vs_mask_side(scan, mask_sides, center_m = optimized_center_m)
	ratio_of_collected_charges = {}
	for ch in CHANNELS:
		ratio_of_collected_charges[ch] = np.array([c/ref for c,ref in zip(collected_charge_inside_mask[ch],collected_charge_inside_mask['CH1'])])
		collected_charge_vs_mask_size_fig.plot(
			mask_sides,
			ratio_of_collected_charges[ch],
			label = f'Q(CH1)/Q({ch})',
			marker = '.',
		)
		indices_for_fit = np.isfinite(ratio_of_collected_charges[ch]) # I have to remove infinites and NaN values.
		polyvals = np.polyfit(mask_sides[indices_for_fit], ratio_of_collected_charges[ch][indices_for_fit], 1)
		collected_charge_vs_mask_size_fig.plot(
			mask_sides,
			np.poly1d(polyvals)(mask_sides[indices_for_fit]),
			label = f'Fit {ch}',
		)
	
	fig = mpl.manager.new(
		title = 'Swiss flag after optimization',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'x (m)',
		ylabel = 'y (m)',
	)
	scan.center_m = tuple(optimized_center_m)
	scan.plot_data_and_fit(fig)
	
	with open(f'{bureaucrat.processed_data_dir_path}/script_output.txt', 'w') as ofile:
		print(f'# charge collection optimum geometrical center x (m) = {optimized_center_m[0]}', file = ofile)
		print(f'# charge collection optimum geometrical center y (m) = {optimized_center_m[1]}', file = ofile)
		for ch in CHANNELS:
			print(f'# {ch} gain = {np.nanmean(ratio_of_collected_charges[ch])}', file = ofile)
	
	for ch in CHANNELS:
		collected_charge_fig = mpl.manager.new(
			title = f'Collected charge by {ch} without gain correction',
			subtitle = f'Data set: {bureaucrat.measurement_name}',
			xlabel = 'x (m)',
			ylabel = 'y (m)',
			aspect = 'equal',
		)
		collected_charge_fig.colormap(
			x = scan.xx_m,
			y = scan.yy_m,
			z = scan.data[ch]['collected charge']*scan.swiss_flag_mask(),
			colorscalelabel = f'Normalized collected charge (a.u.)',
		)
		collected_charge_fig = mpl.manager.new(
			title = f'Collected charge by {ch} with gain correction',
			subtitle = f'Data set: {bureaucrat.measurement_name}',
			xlabel = 'x (m)',
			ylabel = 'y (m)',
			aspect = 'equal',
		)
		collected_charge_fig.colormap(
			x = scan.xx_m,
			y = scan.yy_m,
			z = scan.data[ch]['collected charge']/np.nanmean(ratio_of_collected_charges[ch])*scan.swiss_flag_mask(),
			colorscalelabel = f'Normalized collected charge (a.u.)',
		)
		
	mpl.manager.save_all(mkdir = f'{bureaucrat.processed_data_dir_path}')
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Calibrates the gain from each channel such that the collected charge is the same for all of them.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)
