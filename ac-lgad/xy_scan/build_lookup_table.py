from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
from plot_parsed_data import read_parsed_data
from utils import ACLGADxyScan

CHANNELS = [f'CH{i+1}' for i in range(4)]

def k_yuta(QRT, QRB, QLT, QLB):
	kx = (QRT+QRB-QLT-QLB)/(QRT+QRB+QLT+QLB)
	ky = (QRT+QLT-QRB-QLB)/(QRT+QRB+QLT+QLB)
	return kx, ky

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	parsed = {}
	for ch in CHANNELS:
		parsed[ch] = read_parsed_data(bureaucrat.processed_by_script_dir_path('parse_raw_data.py')/Path(f'parsed_attributes_{ch}.txt'))
	scan = ACLGADxyScan(parsed_data = parsed)
	
	scan.gain = {}
	with open(f'{bureaucrat.processed_by_script_dir_path("calibrate_gain.py")}/script_output.txt', 'r') as ifile:
		for line in ifile:
			for ch in CHANNELS:
				if ch in line and 'gain' in line:
					scan.gain[ch] = float(line.split(' = ')[-1].replace('\n',''))
					scan.data[ch]['collected charge'] /= scan.gain[ch]
	
	total_collected_charge = np.zeros(scan.data['CH1']['collected charge'].shape)
	for ch in CHANNELS:
		total_collected_charge += scan.data[ch]['collected charge']
	for ch in CHANNELS:
		scan.data[ch]['charge share fraction'] = scan.data[ch]['collected charge']/total_collected_charge
		charge_share_fraction_fig = mpl.manager.new(
			title = f'Charge share fraction for {ch}',
			subtitle = f'Data set: {bureaucrat.measurement_name}',
			xlabel = 'x (m)',
			ylabel = 'y (m)',
			aspect = 'equal',
		)
		charge_share_fraction_fig.contour(
			x = scan.xx_m,
			y = scan.yy_m,
			z = scan.data[ch]['charge share fraction']*scan.swiss_flag_mask(),
			colorscalelabel = 'Charge share fraction',
		)
	
	kx, ky = k_yuta(
		QRT = scan.data['CH3']['collected charge'],
		QRB = scan.data['CH2']['collected charge'],
		QLT = scan.data['CH4']['collected charge'],
		QLB = scan.data['CH1']['collected charge'],
	)
	kx_fig = mpl.manager.new(
		title = 'k_x Yuta',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'x (m)',
		ylabel = 'y (m)',
		aspect = 'equal',
	)
	kx_fig.contour(
		x = scan.xx_m,
		y = scan.yy_m, 
		z = kx*scan.swiss_flag_mask(),
		colorscalelabel = 'k_x',
	)
	kx_fig = mpl.manager.new(
		title = 'k_y Yuta',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'x (m)',
		ylabel = 'y (m)',
		aspect = 'equal',
	)
	kx_fig.contour(
		x = scan.xx_m,
		y = scan.yy_m, 
		z = ky*scan.swiss_flag_mask(),
		colorscalelabel = 'k_y',
	)
	
	np.savetxt(bureaucrat.processed_data_dir_path/Path('kx.txt'), kx)
	np.savetxt(bureaucrat.processed_data_dir_path/Path('ky.txt'), ky)
	np.savetxt(bureaucrat.processed_data_dir_path/Path('x.txt'), scan.xx_m)
	np.savetxt(bureaucrat.processed_data_dir_path/Path('y.txt'), scan.yy_m)
	
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('plots'))

if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Callibrates the gain from each channel such that the collected charge is the same for all of them.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)
