import numpy as np
import lmfit
from scipy import interpolate

CHANNELS = [f'CH{i+1}' for i in range(4)]

def draw_square(fig, center, side, **kwargs):
	fig.plot(
		[center[0]-side/2] + 2*[center[0]+side/2] + [center[0]-side/2] + [center[0]-side/2],
		2*[center[1]-side/2] + 2*[center[1]+side/2] + [center[1]-side/2],
		**kwargs,
	)

def draw_swiss_flag(fig, center, side, **kwargs):
	fig.plot(
		np.array(2*[3/2*side] + 2*[side/2] + 2*[-side/2] + 2*[-3/2*side] + 2*[-side/2] + 2*[side/2] + 2*[3/2*side]) + center[0],
		np.array([0] + 2*[side/2] + 2*[3/2*side] + 2*[side/2] + 2*[-side/2] + 2*[-3/2*side] + 2*[-side/2] + [0]) + center[1],
		**kwargs,
	)

def swiss_flag(x, y, center: tuple, side: float, fill_with_nans=False):
	# x, y numpy arrays.
	if x.shape != y.shape:
		raise ValueError(f'<x> and <y> must have the same shape!')
	flag = np.zeros(x.shape)
	if fill_with_nans:
		flag[:] = float('NaN')
	flag[(center[0]-side/2<x)&(x<center[0]+side/2)&(center[1]-3/2*side<y)&(y<center[1]+3/2*side)] = 1
	flag[(center[1]-side/2<y)&(y<center[1]+side/2)&(center[0]-3/2*side<x)&(x<center[0]+3/2*side)] = 1
	return flag

def fit_swiss_flag(data_img, guessed_center, guessed_side):
	def wrapper_for_lmfit(x, x_pixels, y_pixels, function_2D_to_wrap, *params):
		pixel_number = x # This is the pixel number in the data array
		# x_pixels and y_pixels are the number of pixels that the image has. This is needed to make the mapping.
		if (pixel_number > x_pixels*y_pixels - 1).any():
			raise ValueError('pixel_number (x) > x_pixels*y_pixels - 1')
		x = np.array([int(p%x_pixels) for p in pixel_number])
		y = np.array([int(p/x_pixels) for p in pixel_number])
		return function_2D_to_wrap(x, y, *params)
	data = data_img
	data -= data.min().min()
	data = data/data.max().max()
	model = lmfit.Model(lambda x, xc, yc, side: wrapper_for_lmfit(x, data_img.shape[1], data_img.shape[0], swiss_flag, (xc,yc), side))
	params = model.make_params()
	params['xc'].set(value = guessed_center[0], min = 0, max = data.shape[1])
	params['yc'].set(value = guessed_center[1], min = 0, max = data.shape[0])
	params['side'].set(value = guessed_side, min = 0, max = max(data.shape))
	fit_results = model.fit(
		data.ravel(), 
		params, 
		x = [i for i in range(len(data.ravel()))],
		method = 'differential_evolution', # https://stackoverflow.com/questions/65455591/lmfit-stepped-functions-and-step-size/65456063#65456063
	)
	return {'xc':fit_results.params['xc'], 'yc':fit_results.params['yc'], 'side':fit_results.params['side']}

class ACLGADxyScan:
	def __init__(self, parsed_data):
		self.data = parsed_data
		xx, yy = np.meshgrid(parsed_data['CH1']['x'][0], parsed_data['CH1']['y'][:,0])
		self._xx_m = xx
		self._yy_m = yy
		
		self.px2x = interpolate.interp1d([i for i in range(self.xx_m.shape[1])], self.xx_m[0])
		self.px2y = interpolate.interp1d([i for i in range(self.yy_m.shape[0])], self.yy_m.transpose()[0])
	
	def fit_swiss_flag(self):
		raw_total_collected_charge = np.zeros(self.data['CH1']['collected charge'].shape)
		for ch in CHANNELS:
			raw_total_collected_charge += self.data[ch]['collected charge']
		self._guessed_params = {}
		self._guessed_params['xc'] = raw_total_collected_charge.sum(axis=0).argmax()
		self._guessed_params['yc'] = raw_total_collected_charge.sum(axis=1).argmax()
		self._guessed_params['side'] = np.prod(raw_total_collected_charge.shape)**.5*.3
		self._fitted_params = fit_swiss_flag(
			data_img = raw_total_collected_charge, 
			guessed_center = (self._guessed_params['xc'], self._guessed_params['yc']),
			guessed_side = self._guessed_params['side'],
		)
	
	@property
	def fitted_center_px(self):
		if not hasattr(self, '_fitted_params'):
			self.fit_swiss_flag()
		return (self._fitted_params['xc'], self._fitted_params['yc'])
	@property
	def fitted_side_px(self):
		if not hasattr(self, '_fitted_params'):
			self.fit_swiss_flag()
		return self._fitted_params['side']
	@property
	def fitted_center_m(self):
		return (self.px2x(self.fitted_center_px[0]), self.px2y(self.fitted_center_px[1]))
	@property
	def fitted_side_m(self):
		return self.px2x(self.fitted_side_px) - self.px2x(0)
	
	@property
	def center_m(self):
		if hasattr(self, '_center_m'):
			return self._center_m
		else:
			return self.fitted_center_m
	@center_m.setter
	def center_m(self, val):
		if not isinstance(val, tuple) or len(val) != 2 or not all([isinstance(n, float) or isinstance(n, int) for n in val]):
			raise TypeError(f'Must be a tuple of two float values (xc, yc), received {val} of type {type(val)}.')
		self._center_m = val
	@property
	def side_m(self):
		if hasattr(self, '_side_m'):
			return self._side_m
		else:
			return self.fitted_side_m
	@side_m.setter
	def side_m(self, val):
		if not (isinstance(val, float) or isinstance(val, int)):
			raise TypeError(f'Must be a float, received {val} of type {type(val)}.')
		self._side_m = val
	
	@property
	def xx_m(self):
		return self._xx_m
	@property
	def yy_m(self):
		return self._yy_m 
	
	def plot_data_and_fit(self, fig):
		fig.set(aspect = 'equal')
		raw_total_collected_charge = np.zeros(self.data['CH1']['collected charge'].shape)
		for ch in CHANNELS:
			raw_total_collected_charge += self.data[ch]['collected charge']
		fig.colormap(
			x = self.xx_m,
			y = self.yy_m,
			z = raw_total_collected_charge,
			colorscalelabel = 'Raw collected charge',
		)
		draw_swiss_flag(
			fig, 
			self.center_m, 
			self.side_m, 
			label = 'Fitted Swiss flag',
			color = (1,)*3,
		)
	
	def swiss_flag_mask(self, center_m=None, side_m=None):
		if center_m is None:
			center_m = self.center_m
		if side_m is None:
			side_m = self.side_m
		return swiss_flag(self.xx_m, self.yy_m, center = center_m, side = side_m, fill_with_nans=True)
