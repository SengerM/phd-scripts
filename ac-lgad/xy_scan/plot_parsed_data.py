from lgadtools.LGADSignal import LGADSignal
from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl

CHANNELS = [f'CH{i+1}' for i in range(4)]
LABELS = {
	'amplitude': 'Amplitude (V)',
	'noise': 'Noise (V)',
	'rise time': 'Rise time (s)',
	'collected charge': 'Collected charge (arbitrary units)',
}

def read_parsed_data(file: str):
	data = np.genfromtxt(Path(file)).transpose()
	nx = data[0].astype(int)
	ny = data[1].astype(int)
	parsed_stuff = {}
	for variable in ['x','y','z','amplitude','noise','rise time','collected charge']:
		parsed_stuff[variable] = np.empty((nx.max()+1,ny.max()+1))
		parsed_stuff[variable][:] = float('NaN')
	for k in range(len(nx)):
		for idx,variable in enumerate(['x','y','z','amplitude','noise','rise time','collected charge']):
			parsed_stuff[variable][ny[k],nx[k]] = data[idx+2][k]
	return parsed_stuff

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	parsed = {}
	for ch in CHANNELS:
		parsed[ch] = read_parsed_data(bureaucrat.processed_by_script_dir_path('parse_raw_data.py')/Path(f'parsed_attributes_{ch}.txt'))
		for variable in ['amplitude','noise','rise time','collected charge']:
			cmap = mpl.manager.new(
				title = f'{variable} for {ch}',
				subtitle = f'Data set: {bureaucrat.measurement_name}',
				xlabel = f'x position (mm)',
				ylabel = f'y position (mm)',
				aspect = 'equal',
			)
			cmap.colormap(
				x = parsed[ch]['x']*1e3,
				y = parsed[ch]['y']*1e3,
				z = parsed[ch][variable],
				colorscalelabel = LABELS[variable],
			)
			mpl.manager.save_all(mkdir = str(bureaucrat.processed_data_dir_path/Path(variable)))
	
	for variable in ['amplitude','noise','rise time','collected charge']:
		hist = mpl.manager.new(
			title = f'{variable} distribution',
			subtitle = f'Data set: {bureaucrat.measurement_name}',
			xlabel = LABELS[variable],
			ylabel = 'Number of occurrences',
		)
		for ch in CHANNELS:
			hist.hist(
				parsed[ch][variable],
				label = ch,
				bins = 222,
			)
		mpl.manager.save_all(mkdir = str(bureaucrat.processed_data_dir_path/Path(variable)))
	
	total_collected_charge = np.zeros(parsed['CH1']['collected charge'].shape)
	for ch in CHANNELS:
		total_collected_charge += parsed[ch]['collected charge']/parsed[ch]['amplitude'].max()
	# ~ cmap = mpl.manager.new(
		# ~ title = f'Total collected charge after amplitude normalization',
		# ~ subtitle = f'Data set: {bureaucrat.measurement_name}',
		# ~ xlabel = f'x position (mm)',
		# ~ ylabel = f'y position (mm)',
		# ~ aspect = 'equal',
	# ~ )
	# ~ cmap.colormap(
		# ~ x = parsed['CH1']['x']*1e3,
		# ~ y = parsed['CH1']['y']*1e3,
		# ~ z = total_collected_charge,
		# ~ colorscalelabel = 'Collected charge/max(amplitude) (arbitrary units)',
	# ~ )
	# ~ cmap.show()

if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Parses the raw data to get parameters such as amplitude, collected charge, etc.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)
