from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
from plot_parsed_data import read_parsed_data
from utils import ACLGADxyScan

CHANNELS = [f'CH{i+1}' for i in range(4)]

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	parsed = {}
	for ch in CHANNELS:
		parsed[ch] = read_parsed_data(bureaucrat.processed_by_script_dir_path('parse_raw_data.py')/Path(f'parsed_attributes_{ch}.txt'))
		parsed[ch]['collected charge'][np.isnan(parsed[ch]['collected charge'])] = 0
	
	scan = ACLGADxyScan(parsed_data = parsed)
	
	fig = mpl.manager.new(
		title = 'Total collected charge without gain correction',
		subtitle = f'Measurement: {bureaucrat.measurement_name}',
		xlabel = 'x (m)',
		ylabel = 'y (m)',
		aspect = 'equal',
	)
	fig.colormap(
		x = scan.xx_m,
		y = scan.yy_m,
		z = sum([scan.data[ch]['collected charge'] for ch in CHANNELS]),
		colorscalelabel = 'Collected charge without gain correction (a.u.)',
	)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
	
	do_fits = True
	if Path(f'{bureaucrat.processed_data_dir_path}/fitted_swiss_flag_params.txt').exists():
		path = Path(f'{bureaucrat.processed_data_dir_path}/fitted_swiss_flag_params.txt')
		if input(f'There is already existent data processed by this script in "{path}", do you want to append new data to it? (yes) ') != 'yes':
			do_fits = False
	else:
		with open(f'{bureaucrat.processed_data_dir_path}/fitted_swiss_flag_params.txt', 'w') as ofile:
			print(f'# xc (m)\tyc (m)\tside (m)', file = ofile)
	
	if do_fits == True:
		for k in range(99):
			scan.fit_swiss_flag() # Force a new fit.
			with open(f'{bureaucrat.processed_data_dir_path}/fitted_swiss_flag_params.txt', 'a') as ofile:
				print(f'{scan.fitted_center_m[0]}\t{scan.fitted_center_m[1]}\t{scan.fitted_side_m}', file = ofile)
			fig = mpl.manager.new(
				title = f'Fit number {k+1}',
				xlabel = 'x (m)',
				ylabel = 'y (m)',
				package = 'matplotlib',
			)
			scan.plot_data_and_fit(fig)
			mpl.manager.save_all(mkdir = f'{bureaucrat.processed_data_dir_path}/fitted_swiss_flags_plots')
		fig = mpl.manager.new(
			title = f'Fitted Swiss flag',
			xlabel = 'x (m)',
			ylabel = 'y (m)',
			package = 'plotly',
		)
		scan.plot_data_and_fit(fig)
	
	center_histogram = mpl.manager.new(
		title = f'Distribution of the center of the fitted swiss flags',
		xlabel = 'x (m) or y (m)',
		ylabel = 'Number of occurrences',
	)
	xcenters = np.genfromtxt(f'{bureaucrat.processed_data_dir_path}/fitted_swiss_flag_params.txt').transpose()[0]
	ycenters = np.genfromtxt(f'{bureaucrat.processed_data_dir_path}/fitted_swiss_flag_params.txt').transpose()[1]
	center_histogram.hist(
		xcenters - xcenters.mean(),
		label = 'x center',
	)
	center_histogram.hist(
		ycenters - ycenters.mean(),
		label = 'y center',
	)
	
	side_histogram = mpl.manager.new(
		title = f'Distribution of the side of the fitted swiss flags',
		xlabel = 'side (m)',
		ylabel = 'Number of occurrences',
	)
	sides = np.genfromtxt(f'{bureaucrat.processed_data_dir_path}/fitted_swiss_flag_params.txt').transpose()[2]
	side_histogram.hist(
		sides,
		label = 'Side'
	)
	mpl.manager.save_all(mkdir = f'{bureaucrat.processed_data_dir_path}')
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Fits a Swiss flag to the raw data to determine the geometry.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)
