from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
from progressreporting.TelegramProgressReporter import TelegramProgressReporter
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
from scipy.stats import norm
# ~ from time_resolution_at_single_point import script_core as time_resolution_at_single_point_script

CHANNELS = [f'CH{i}' for i in [1,2,3,4]]

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	data = pandas.read_csv(
		bureaucrat.processed_by_script_dir_path('scan_xy_two_pulses_laser_splitter.py')/Path('measured_data.csv'),
		sep = '\t',
	)
	nx_values = sorted(set(data['n_x']))
	ny_values = sorted(set(data['n_y']))
	
	with TelegramProgressReporter(len(nx_values)*len(ny_values), TelegramReportingInformation().token, TelegramReportingInformation().chat_id, f'AC-LGAD timing analysis {bureaucrat.measurement_name} (timestamp of this analysis: {bureaucrat.this_run_timestamp})') as reporter:
		for nx in nx_values:
			for ny in ny_values:
				time_resolution_at_single_point_script(
					directory = directory,
					nx = nx,
					ny = ny,
				)
				reporter.update(1)
	
	timing_data_df = pandas.read_csv(bureaucrat.processed_by_script_dir_path('time_resolution_at_single_point.py')/Path('time resolution data.csv'))
	x_values = sorted(set(data['x (m)']))
	y_values = sorted(set(data['y (m)']))
	xx, yy = np.meshgrid(x_values, y_values)
	for ch in CHANNELS:
		fig = mpl.manager.new(
			title = f'Time resolution vs position channel {ch[-1]}',
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = 'x (m)',
			ylabel = 'y (m)',
			aspect = 'equal',
		)
		zz = np.array(timing_data_df.pivot_table(f'σ{ch[-1]} (s)', 'n_x', 'n_y')/2**.5)
		zz[zz>100e-12] = float('NaN')
		fig.colormap(
			x = xx,
			y = yy,
			z = zz,
			colorscalelabel = f'Time resolution σ/√2 (s)',
		)
		mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
	
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Plots every thing measured in an xy scan.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)

