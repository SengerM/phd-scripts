from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
from scipy.stats import norm

CHANNELS = [f'CH{i}' for i in [1,2,3,4]]
K_CFDs = [10, 20, 30, 40, 50, 60, 70, 80, 90]
COLORS = {
	'CH1': (1,.2,.2),
	'CH2': (0,.8,0),
	'CH3': (.1,.1,1),
	'CH4': (1,.5,0),
}

def script_core(directory, nx, ny):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	data = pandas.read_csv(
		bureaucrat.processed_by_script_dir_path('scan_xy_two_pulses_laser_splitter.py')/Path('measured_data.csv'),
		sep = '\t',
	)
	
	x_values = sorted(set(data['x (m)']))
	y_values = sorted(set(data['y (m)']))
	xx, yy = np.meshgrid(x_values, y_values)
	amplitudes = {}
	for ch in CHANNELS:
		amplitudes[ch] = data.loc[(data['n_pulse']==1)&(data['n_channel']==int(ch[-1]))].pivot_table('Amplitude (V)', 'n_x', 'n_y', aggfunc = np.nanmean)
	fig = mpl.manager.new(
		title = f'Amplitude map for time resolution at single point nx {nx} ny {ny}',
		subtitle = f'Dataset: {bureaucrat.measurement_name}',
		xlabel = 'x (m)',
		ylabel = 'y (m)',
		aspect = 'equal',
	)
	fig.colormap(
		x = xx,
		y = yy,
		z = sum([amplitudes[key] for key in amplitudes]),
		colorscalelabel = 'Sum of amplitudes of the four channels (V)',
	)
	fig.plot(
		list(set(data.loc[data['n_x']==nx, 'x (m)'])),
		list(set(data.loc[data['n_y']==ny, 'y (m)'])),
		linestyle = '',
		marker = '.',
		color = (0,1,0),
		label = 'Time resolution was analyzed at this point',
	)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path(f'nx {nx} ny {ny}'))
	
	data = data[(data['n_x']==nx) & (data['n_y']==ny)] # Drop everything I will not use.
	
	
	xx_k_CFD, yy_k_CFD = np.meshgrid(K_CFDs, K_CFDs)
	zz_time_resolutions = {}
	for ch in CHANNELS:
		zz_time_resolutions[ch] = np.zeros(xx_k_CFD.shape)
		zz_time_resolutions[ch][:] = float('NaN')
	for nk1,k_CFD_pulse_1 in enumerate(K_CFDs):
		for nk2,k_CFD_pulse_2 in enumerate(K_CFDs):
			fig = mpl.manager.new(
				title = f'Time difference at nx {nx} ny {ny} k_CFD1 {k_CFD_pulse_1} % k_CFD2 {k_CFD_pulse_2} %',
				subtitle = f'Dataset: {bureaucrat.measurement_name}',
				xlabel = 'Δt (s)',
				ylabel = 'Density of events',
				package = 'matplotlib',
			)
			for ch in CHANNELS:
				n_channel = int(ch[-1])
				t1 = data.loc[(data['n_pulse']==1) & (data['n_channel']==n_channel), f'tr{k_CFD_pulse_1} (s)'].to_numpy()
				t2 = data.loc[(data['n_pulse']==2) & (data['n_channel']==n_channel), f'tr{k_CFD_pulse_2} (s)'].to_numpy()
				Delta_t = t2 - t1
				Delta_t = Delta_t[~np.isnan(Delta_t)] # Remove NaN values.
				µ, σ = norm.fit(Delta_t)
				
				zz_time_resolutions[ch][nk2,nk1] = σ/2**.5
				
				fig.hist(
					Delta_t,
					label = f'Channel {n_channel}',
					density = True,
					color = COLORS[ch],
				)
				
				t_axis = np.linspace(min(Delta_t), max(Delta_t), 99)
				fig.plot(
					t_axis,
					norm.pdf(t_axis, µ, σ),
					label = f'Fit (µ={µ*1e9:.2f} ns, σ={σ*1e12:.2f} ps)',
					color = COLORS[ch],
				)
			mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path(f'nx {nx} ny {ny}')/Path('Gaussian fits plots'))
	
	for ch in CHANNELS:
		fig = mpl.manager.new(
			title = f'Time resolution vs k_CFD map for channel {ch[-1]} at nx {nx} ny {ny}',
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = f'k_CFD for pulse 1 (%)',
			ylabel = f'k_CFD for pulse 2 (%)',
			aspect = 'equal',
		)
		fig.contour(
			x = xx_k_CFD,
			y = yy_k_CFD,
			z = zz_time_resolutions[ch]*1e12,
			colorscalelabel = f'Time resolution σ/√2 (ps)',
		)
		fig.plot(
			[xx_k_CFD.ravel()[np.argmin(zz_time_resolutions[ch].ravel())]],
			[yy_k_CFD.ravel()[np.argmin(zz_time_resolutions[ch].ravel())]],
			label = f'Time resolution = σ/√2 = {zz_time_resolutions[ch].min()*1e12:.2f} ps',
			marker = '.',
			linestyle = '',
			color = (1,0,0),
		)
		mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path(f'nx {nx} ny {ny}'))
	
	ofilePath = bureaucrat.processed_data_dir_path/Path('time resolution data.csv')
	ofilemode = 'w' if ofilePath.is_file()==False else 'a'
	with open(ofilePath, ofilemode) as ofile:
		if ofilemode == 'w':
			print('n_x,n_y,σ1 (s),σ2 (s),σ3 (s),σ4 (s)', file = ofile)
		print(f'{nx},{ny},' + ','.join([str(zz_time_resolutions[ch].min()*2**.5) for ch in CHANNELS]), file = ofile)
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Plots every thing measured in an xy scan.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	parser.add_argument(
		'--nx',
		metavar = 'nx', 
		help = 'Value of <nx> of the point for which to calculate the time resolution.',
		required = True,
		dest = 'nx',
		type = int,
	)
	parser.add_argument(
		'--ny',
		metavar = 'ny', 
		help = 'Value of <ny> of the point for which to calculate the time resolution.',
		required = True,
		dest = 'ny',
		type = int,
	)
	args = parser.parse_args()
	script_core(args.directory, args.nx, args.ny)

