from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
import scipy.optimize as optimize
import pandas
from fit_swiss_flag import CHANNELS, ACLGADxyScan

def calculate_collected_charge_within_flag_vs_flag_side(scan, sides: list, center):
	collected_charge_inside_mask = {}
	for idx,side in enumerate(sides):
		mask = scan.swiss_flag_mask(
			center = center, 
			side = side,
		)
		for ch in CHANNELS:
			if ch not in collected_charge_inside_mask: # Initialize.
				collected_charge_inside_mask[ch] = []
			collected_charge_inside_mask[ch].append(
				np.nansum(scan.average_matrices[ch]*mask)
			)
	return collected_charge_inside_mask

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	data = pandas.read_csv(
		bureaucrat.processed_by_script_dir_path('scan_xy_4_channels_many_triggers_per_point.py')/Path('measured_data.csv'),
		delimiter = '\t',
	)
	collected_charge_scan = ACLGADxyScan(data, 'Collected charge (a.u.)')
	
	fig = mpl.manager.new(
		title = 'Swiss flag before optimization',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'x (m)',
		ylabel = 'y (m)',
		aspect = 'equal',
	)
	collected_charge_scan.plot_fitted_swiss_flag(fig)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('plots'))
	
	def function_to_minimize(center):
		x_center_offset = center[0]
		y_center_offset = center[1]
		mask_sides = np.linspace(0, collected_charge_scan.fitted_side)
		collected_charge_inside_masks = calculate_collected_charge_within_flag_vs_flag_side(
			collected_charge_scan, 
			sides = mask_sides, 
			center = tuple(np.array(collected_charge_scan.fitted_center) + np.array((x_center_offset,y_center_offset))),
		)
		slopes = []
		for ch in CHANNELS:
			ratio_of_collected_charges = np.array([c/ref for c,ref in zip(collected_charge_inside_masks[ch],collected_charge_inside_masks['CH1'])])
			indices_for_fit = np.isfinite(ratio_of_collected_charges) # I have to remove infinites and NaN values.
			fitted_coefs = np.polyfit(mask_sides[indices_for_fit], ratio_of_collected_charges[indices_for_fit], 1)
			slopes.append(fitted_coefs[0])
		return (np.array(slopes)**2).sum()
	
	optimization_plot = mpl.manager.new(
		title = 'Quantity to minimize',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		aspect = 'equal',
		xlabel = f'Center x shift (m)',
		ylabel = f'Center y shift (m)',
	)
	xx,yy = np.meshgrid(np.linspace(-1e-6,1e-6,99),np.linspace(-1e-6,1e-6,99))
	zz = np.array([[function_to_minimize((x,y)) for x,y in zip(xrow,yrow)] for xrow,yrow in zip(xx,yy)])
	optimization_plot.colormap(
		x = xx,
		y = yy,
		z = zz,
		norm = 'log',
	)
	optimized_center = np.array(collected_charge_scan.fitted_center) + np.array((xx.ravel()[np.argmin(zz)], yy.ravel()[np.argmin(zz)]))
	optimization_plot.plot(
		[optimized_center[0] - collected_charge_scan.fitted_center[0]],
		[optimized_center[1] - collected_charge_scan.fitted_center[1]],
		label = f'Optimal center shift',
		marker = 'x',
	)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('plots'))
	
	collected_charge_vs_mask_size_fig = mpl.manager.new(
		title = f'Collected charge ratios vs mask size after optimization',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'Swiss flag side (m)',
		ylabel = 'Q(CHi)/Q(CH1)',
	)
	mask_sides = np.linspace(0, collected_charge_scan.fitted_side)
	collected_charge_inside_mask = calculate_collected_charge_within_flag_vs_flag_side(collected_charge_scan, mask_sides, center = optimized_center)
	ratio_of_collected_charges = {}
	for ch in CHANNELS:
		ratio_of_collected_charges[ch] = np.array([c/ref for c,ref in zip(collected_charge_inside_mask[ch],collected_charge_inside_mask['CH1'])])
		collected_charge_vs_mask_size_fig.plot(
			mask_sides,
			ratio_of_collected_charges[ch],
			label = f'Q(CH1)/Q({ch})',
			marker = '.',
		)
		indices_for_fit = np.isfinite(ratio_of_collected_charges[ch]) # I have to remove infinites and NaN values.
		polyvals = np.polyfit(mask_sides[indices_for_fit], ratio_of_collected_charges[ch][indices_for_fit], 1)
		collected_charge_vs_mask_size_fig.plot(
			mask_sides[indices_for_fit],
			np.poly1d(polyvals)(mask_sides[indices_for_fit]),
			label = f'Fit {ch}',
		)
	
	fig = mpl.manager.new(
		title = 'Swiss flag after optimization',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'x (m)',
		ylabel = 'y (m)',
		aspect = 'equal',
	)
	collected_charge_scan.plot_swiss_flag(
		fig,
		center = optimized_center,
		side = collected_charge_scan.fitted_side,
	)
	
	with open(f'{bureaucrat.processed_data_dir_path}/script_output.txt', 'w') as ofile:
		print(f'# charge collection optimum geometrical center x (m) = {optimized_center[0]}', file = ofile)
		print(f'# charge collection optimum geometrical center y (m) = {optimized_center[1]}', file = ofile)
		for ch in CHANNELS:
			print(f'# {ch} gain = {np.nanmean(ratio_of_collected_charges[ch])}', file = ofile)
	
	gain_corrected_collected_charges = {}
	for ch in CHANNELS:
		gain_corrected_collected_charges[ch] = collected_charge_scan.average_matrices[ch]/np.nanmean(ratio_of_collected_charges[ch])
	for ch in CHANNELS:
		for package in ['plotly', 'matplotlib']:
			collected_charge_fig = mpl.manager.new(
				title = f'Charge fraction by {ch} without gain correction',
				subtitle = f'Data set: {bureaucrat.measurement_name}',
				xlabel = 'x (m)',
				ylabel = 'y (m)',
				aspect = 'equal',
				package = package,
			)
			swiss_flag_mask = collected_charge_scan.swiss_flag_mask(center=collected_charge_scan.fitted_center, side=collected_charge_scan.fitted_side)
			collected_charge_fig.contour(
				x = collected_charge_scan.xx,
				y = collected_charge_scan.yy,
				z = collected_charge_scan.average_matrices[ch]/sum([collected_charge_scan.average_matrices[c] for c in CHANNELS])*swiss_flag_mask,
				colorscalelabel = f'Normalized collected charge (a.u.)',
			)
			collected_charge_fig.plot(
				[collected_charge_scan.fitted_center[0]],
				[collected_charge_scan.fitted_center[1]],
				marker = 'x',
				linestyle = '',
				color = (0,0,0),
			)
			collected_charge_fig = mpl.manager.new(
				title = f'Charge fraction by {ch} with gain correction',
				subtitle = f'Data set: {bureaucrat.measurement_name}',
				xlabel = 'x (m)',
				ylabel = 'y (m)',
				aspect = 'equal',
				package = package,
			)
			collected_charge_fig.contour(
				x = collected_charge_scan.xx,
				y = collected_charge_scan.yy,
				z = gain_corrected_collected_charges[ch]/sum([gain_corrected_collected_charges[c] for c in CHANNELS])*swiss_flag_mask,
				colorscalelabel = f'Normalized collected charge (a.u.)',
			)
			collected_charge_fig.plot(
				[collected_charge_scan.fitted_center[0]],
				[collected_charge_scan.fitted_center[1]],
				marker = 'x',
				linestyle = '',
				color = (0,0,0),
			)
		
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('plots'))
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Calibrates the gain from each channel such that the collected charge is the same for all of them.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)
