from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
import sys
sys.path.append('../xy_scan')
from utils import fit_swiss_flag, draw_swiss_flag, swiss_flag
import pandas
from scipy import interpolate

CHANNELS = [f'CH{i}' for i in [1,2,3,4]]

def calculate_mean_value_of_column_as_2D_matrix(data, column):
	mean_values_matrices = {}
	nx_values = sorted(set(data['n_x']))
	ny_values = sorted(set(data['n_y']))
	nnx, nny = np.meshgrid(nx_values,ny_values)
	for ch in CHANNELS:
		mean_values_matrices[ch] = np.zeros(nnx.shape)
		mean_values_matrices[ch][:] = float('NaN')
	for ch in CHANNELS:
		for nx in nx_values:
			for ny in ny_values:
				mean_values_matrices[ch][ny,nx] = np.nanmean(data[(data['n_x']==nx)&(data['n_y']==ny)&(data['n_channel']==int(ch[-1]))][column])
	return mean_values_matrices

class ACLGADxyScan:
	def __init__(self, data, column_to_use: str):
		self.data = data
		x_values = sorted(set(data['x (m)']))
		y_values = sorted(set(data['y (m)']))
		self.xx, self.yy = np.meshgrid(x_values,y_values)
		self.average_matrices = calculate_mean_value_of_column_as_2D_matrix(data, column_to_use)
		self.column_to_use = column_to_use
		
		self.nx2x = interpolate.interp1d([i for i in range(self.xx.shape[1])], self.xx[0])
		self.ny2y = interpolate.interp1d([i for i in range(self.yy.shape[0])], self.yy.transpose()[0])
	
	def fit_swiss_flag(self):
		totals = sum([self.average_matrices[ch] for ch in CHANNELS])
		totals[np.isnan(totals)] = 0 # Replace NaN --> 0 because the fitting methods fail otherwise.
		self._guessed_params = {}
		self._guessed_params['xc'] = totals.sum(axis=0).argmax()
		self._guessed_params['yc'] = totals.sum(axis=1).argmax()
		self._guessed_params['side'] = np.prod(totals.shape)**.5*.3
		self._fitted_params = fit_swiss_flag(
			data_img = totals, 
			guessed_center = (self._guessed_params['xc'], self._guessed_params['yc']),
			guessed_side = self._guessed_params['side'],
		)
	
	@property
	def fitted_center_n(self):
		if not hasattr(self, '_fitted_params'):
			self.fit_swiss_flag()
		return (self._fitted_params['xc'], self._fitted_params['yc'])
	@property
	def fitted_side_n(self):
		if not hasattr(self, '_fitted_params'):
			self.fit_swiss_flag()
		return self._fitted_params['side']
	@property
	def fitted_center(self):
		return (self.nx2x(self.fitted_center_n[0]), self.ny2y(self.fitted_center_n[1]))
	@property
	def fitted_side(self):
		return self.nx2x(self.fitted_side_n) - self.nx2x(0)
	
	def swiss_flag_mask(self, center, side):
		return swiss_flag(self.xx, self.yy, center = center, side = side, fill_with_nans=True)
	
	def plot_swiss_flag(self, fig, center, side):
		fig.colormap(
			x = self.xx,
			y = self.yy,
			z = sum([self.average_matrices[ch] for ch in CHANNELS]),
			colorscalelabel = f'Average {self.column_to_use[0].lower()}{self.column_to_use[1:]}',
		)
		draw_swiss_flag(
			fig, 
			center = center, 
			side = side,
			label = f"Fitted Swiss flag (center = ({center[0]:.3e}, {center[1]:.3e}) m, side = {side:.3e} m)",
			color = (0,0,0),
		)
	
	def plot_fitted_swiss_flag(self, fig):
		self.plot_swiss_flag(fig, center = self.fitted_center, side = self.fitted_side)

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	data = pandas.read_csv(
		bureaucrat.processed_by_script_dir_path('scan_xy_4_channels_many_triggers_per_point.py')/Path('measured_data.csv'),
		delimiter = '\t',
	)
	x_values = sorted(set(data['x (m)']))
	y_values = sorted(set(data['y (m)']))
	xx, yy = np.meshgrid(x_values,y_values)
	collected_charge_scan = ACLGADxyScan(data, 'Collected charge (a.u.)')
	amplitude_scan = ACLGADxyScan(data, 'Amplitude (V)')
	scans = {'using amplitude': amplitude_scan, 'using collected charge': collected_charge_scan}
	for ch in CHANNELS:
		fig = mpl.manager.new(
			title = f'Mean collected charge {ch}',
			subtitle = f'Data set: {bureaucrat.measurement_name}',
			xlabel = 'x (m)',
			ylabel = 'y (m)',
			aspect = 'equal',
		)
		fig.colormap(
			x = xx,
			y = yy,
			z = scans['using collected charge'].average_matrices[ch],
			colorscalelabel = 'Average collected charge (a.u.)',
		)
		fig = mpl.manager.new(
			title = f'Mean amplitude {ch}',
			subtitle = f'Data set: {bureaucrat.measurement_name}',
			xlabel = 'x (m)',
			ylabel = 'y (m)',
			aspect = 'equal',
		)
		fig.colormap(
			x = xx,
			y = yy,
			z = scans['using amplitude'].average_matrices[ch],
			colorscalelabel = 'Average amplitude (V)',
		)
		mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('plots'))
	
	with open(bureaucrat.processed_data_dir_path/Path('fitted_swiss_flag_parameters.txt'), 'w') as ofile:
		for scenario in scans:
			print(f'x_center {scenario} (m) = {scans[scenario].fitted_center[0]}', file = ofile)
			print(f'y_center {scenario} (m) = {scans[scenario].fitted_center[1]}', file = ofile)
			print(f'side {scenario} (m) = {scans[scenario].fitted_side}', file = ofile)
	
	for scenario in ['using amplitude', 'using collected charge']:
		fig = mpl.manager.new(
			title = f'Fitted swis flag {scenario}',
			subtitle = f'Data set: {bureaucrat.measurement_name}',
			xlabel = 'x (m)',
			ylabel = 'y (m)',
			aspect = 'equal',
		)
		scans[scenario].plot_fitted_swiss_flag(fig)
		mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('plots'))
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Fits a Swiss flag to the raw data to determine the geometry.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)
