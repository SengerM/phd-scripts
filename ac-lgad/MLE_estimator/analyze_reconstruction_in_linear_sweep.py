from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
from progressreporting.TelegramProgressReporter import TelegramProgressReporter
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
import pickle
from scipy.stats import norm

def script_core(dir_with_reconstructor_measurement, analysis_name):
	SQUARE_SIZE = 4e-6
	START_POINT = (.0261668,.03221792-20e-6)
	END_POINT = (.02616881,.03197912+20e-6)
	STEP = 1e-6
	
	bureaucrat = Bureaucrat(
		dir_with_reconstructor_measurement,
		variables = locals(),
	)
	with open(bureaucrat.processed_by_script_dir_path('MLE_position_reconstruction_analysis.py')/Path(analysis_name)/Path('metadata.txt'), 'r') as ifile:
		for line in ifile:
			if 'dataset_path' in line:
				dataset_path = Path(line.split(' = ')[-1].replace('\n',''))
	with open(bureaucrat.processed_by_script_dir_path('MLE_training_script.py')/Path('MLE_position_reconstructor.pkl'), 'rb') as ifile:
		reconstructor = pickle.load(ifile)
	
	analysis_data_df = pandas.read_csv(bureaucrat.processed_by_script_dir_path('MLE_position_reconstruction_analysis.py')/Path(analysis_name)/Path('reconstruction_data.csv'))
	analysis_data_df['Reconstruction error (m)'] = ((analysis_data_df['real_x (m)']-analysis_data_df['reconstructed_x (m)'])**2 + (analysis_data_df['real_y (m)']-analysis_data_df['reconstructed_y (m)'])**2)**.5
	
	START_POINT = np.array(START_POINT)
	END_POINT = np.array(END_POINT)
	points_to_analyze = [START_POINT + (END_POINT-START_POINT)/np.linalg.norm(END_POINT-START_POINT)*STEP*i for i in range(int(np.linalg.norm(END_POINT-START_POINT)/STEP))]
	
	nx_values = sorted(set(analysis_data_df['n_x']))
	ny_values = sorted(set(analysis_data_df['n_y']))
	x_values = sorted(set(analysis_data_df['real_x (m)']))
	y_values = sorted(set(analysis_data_df['real_y (m)']))
	xx, yy = np.meshgrid(x_values, y_values)
	reconstruction_error = {}
	reconstruction_error = np.zeros(xx.shape)
	reconstruction_error[:] = float('NaN')
	reconstruction_error = analysis_data_df.pivot_table('Reconstruction error (m)', 'n_y', 'n_x', aggfunc = np.nanstd)
	reconstruction_error[reconstruction_error > 22e-6] = float('NaN')
	
	fig = mpl.manager.new(
		title = f'Analyzed points in color map',
		subtitle = f'Reconstructor: {bureaucrat.measurement_name}\nReconstructed: {dataset_path.parts[-2]}',
		xlabel = 'x (m)',
		ylabel = 'y (m)',
		aspect = 'equal',
	)
	fig.colormap(
		x = xx,
		y = yy,
		z = reconstruction_error,
		colorscalelabel = 'Reconstruction error std (m)',
	)
	# ~ fig.plot(
		# ~ reconstructor.xx.ravel(),
		# ~ reconstructor.yy.ravel(),
		# ~ linestyle = '',
		# ~ marker = 'o',
		# ~ color = (0,0,0),
		# ~ label = 'Training points',
	# ~ )
	fig.plot(
		[p[0] for p in points_to_analyze],
		[p[1] for p in points_to_analyze],
		color = (0,1,1),
		marker = '.',
		label = 'Analyzed slice',
	)
	p = points_to_analyze[0]
	fig.plot(
		[p[0]-SQUARE_SIZE/2, p[0]+SQUARE_SIZE/2,p[0]+SQUARE_SIZE/2,p[0]-SQUARE_SIZE/2,p[0]-SQUARE_SIZE/2],
		[p[1]-SQUARE_SIZE/2, p[1]-SQUARE_SIZE/2,p[1]+SQUARE_SIZE/2,p[1]+SQUARE_SIZE/2,p[1]-SQUARE_SIZE/2],
		color = (0,0,0),
		label = 'Moving region',
	)
	
	analyzed_data = pandas.DataFrame({'point': points_to_analyze})
	for idx,point in enumerate(points_to_analyze):
		this_point_indices = (analysis_data_df['real_x (m)']>=point[0]-SQUARE_SIZE/2) & (analysis_data_df['real_x (m)']<=point[0]+SQUARE_SIZE/2) & (analysis_data_df['real_y (m)']>=point[1]-SQUARE_SIZE/2) & (analysis_data_df['real_y (m)']<=point[1]+SQUARE_SIZE/2)
		analyzed_data.loc[analyzed_data.index==idx,'Distance (m)'] = np.linalg.norm(analyzed_data.loc[0,'point'] - point)
		analyzed_data.loc[analyzed_data.index==idx,'x reconstruction error (m)'] = np.nanstd((analysis_data_df.loc[this_point_indices,f'real_x (m)'] - analysis_data_df.loc[this_point_indices,f'reconstructed_x (m)']).to_numpy())
		analyzed_data.loc[analyzed_data.index==idx,'y reconstruction error (m)'] = np.nanstd((analysis_data_df.loc[this_point_indices,f'real_y (m)'] - analysis_data_df.loc[this_point_indices,f'reconstructed_y (m)']).to_numpy())
	
	for xy in ['x', 'y']:
		fig = mpl.manager.new(
			title = f'{xy} reconstruction error along line',
			subtitle = f'Reconstructor: {bureaucrat.measurement_name}\nReconstructed: {dataset_path.parts[-2]}',
			xlabel = f'Distance along line (m)',
			ylabel = f'{xy} reconstruction error std (m)',
		)
		fig.plot(
			analyzed_data['Distance (m)'],
			analyzed_data[f'{xy} reconstruction error (m)'],
			marker = '.',
		)
		
	mpl.manager.save_all(mkdir=bureaucrat.processed_data_dir_path)
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='')
	parser.add_argument(
		'--dir-with-reconstructor-measurement',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement which has already been used to train a reconstructor.',
		required = True,
		dest = 'directory_reconstructor',
		type = str,
	)
	parser.add_argument(
		'--name-of-analysis',
		metavar = 'path', 
		help = 'Name of one directory inside "processed_by_script_MLE_position_reconstruction_analysis", for example "20210227161803_reconstruction_analysis".',
		required = True,
		dest = 'analysis_name',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory_reconstructor, args.analysis_name)
