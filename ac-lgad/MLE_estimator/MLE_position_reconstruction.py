from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
from progressreporting.TelegramProgressReporter import TelegramProgressReporter
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
from MLE4ACLGAD import MLE4ACLGAD

def script_core(train_data_dir_path, test_data_dir_path, reconstruction_name=None):
	bureaucrat = Bureaucrat(
		test_data_dir_path,
		variables = locals(),
	)
	
	this_analysis_dir_name = f'{bureaucrat.this_run_timestamp}'
	if reconstruction_name is not None:
		this_analysis_dir_name += f'_{reconstruction_name.replace(" ", "_")}'
	this_analysis_dir_path = bureaucrat.processed_data_dir_path/Path(this_analysis_dir_name)
	this_analysis_dir_path.mkdir(parents=True)
	
	train_data_dir_path = Path(train_data_dir_path)
	test_data_dir_path = Path(test_data_dir_path)
	
	print('Reading data...')
	training_observed_df = pandas.read_feather(train_data_dir_path/Path('pre_process_data_for_MLE')/Path('data for MLE observed variables.fd'))
	training_answers_df = pandas.read_feather(train_data_dir_path/Path('pre_process_data_for_MLE')/Path('data for MLE answers.fd'))
	test_observed_df = pandas.read_feather(Path(test_data_dir_path)/Path('pre_process_data_for_MLE')/Path('data for MLE observed variables.fd'))
	test_answers_df = pandas.read_feather(Path(test_data_dir_path)/Path('pre_process_data_for_MLE')/Path('data for MLE answers.fd'))
	
	with open(this_analysis_dir_path/Path('metadata.txt'), 'w') as ofile:
		print(f'train_data_dir_path = {train_data_dir_path}', file = ofile)
		print(f'test_data_dir_path = {test_data_dir_path}', file = ofile)
		with pandas.option_context('display.max_columns', None, 'display.width', None):
			print('\nThe training_observed_df data looks like this:', file = ofile)
			print(training_observed_df, file = ofile)
			print('\nThe training_answers_df data looks like this:', file = ofile)
			print(training_answers_df, file = ofile)
			print('\nThe test_observed_df data looks like this:', file = ofile)
			print(test_observed_df, file = ofile)
			print('\nThe test_answers_df data looks like this:', file = ofile)
			print(test_answers_df, file = ofile)

	print('Training MLE regression algorithm...')
	regressor = MLE4ACLGAD()
	regressor.train(
		observed_df = training_observed_df,
		answers_df = training_answers_df,
	)
	
	with TelegramProgressReporter(len(test_observed_df.index), TelegramReportingInformation().token, TelegramReportingInformation().chat_id, f'MLE position reconstruction analysis using {bureaucrat.measurement_name} (timestamp of this analysis: {bureaucrat.this_run_timestamp})') as reporter:
		with open(this_analysis_dir_path/Path('reconstruction_data.csv'), 'w') as ofile:
			print('n_x,n_y,n_trigger,real_x (m),real_y (m),reconstructed_x (m),reconstructed_y (m)', file = ofile)
			for index, row_observed_df in test_observed_df.iterrows():
				row_answers_df = test_answers_df.loc[index, :]
				nx = int(row_observed_df['n_x'])
				ny = int(row_observed_df['n_y'])
				n_trigger = int(row_observed_df['n_trigger'])
				print(f'Reconstructing event nx {nx} ny {ny} ntrigger {n_trigger}')
				if row_observed_df['n_x'] != row_answers_df['n_x'] or row_observed_df['n_y'] != row_answers_df['n_y'] or row_observed_df['n_trigger'] != row_answers_df['n_trigger']:
					# This should never happen, but just in case...
					raise RuntimeError(f'The events in the <test_observed_df> and <test_answers_df> do not coincide, specifically for row {index}.')
				observed_variables = {}
				for col in test_observed_df:
					if col in ['n_x','n_y','n_trigger']:
						continue
					observed_variables[col] = float(test_observed_df.loc[(test_observed_df['n_x']==nx)&(test_observed_df['n_y']==ny)&(test_observed_df['n_trigger']==n_trigger), col])
				reconstructed_position = regressor.reconstruct_position(observed_variables)
				real_position = tuple([float(test_answers_df.loc[(test_answers_df['n_x']==nx)&(test_answers_df['n_y']==ny)&(test_answers_df['n_trigger']==n_trigger), col]) for col in ['x (m)', 'y (m)']])
				print(f'{nx},{ny},{n_trigger},{real_position[0]},{real_position[1]},{reconstructed_position[0]},{reconstructed_position[1]}', file = ofile)
				try:
					if np.random.random() < 44/len(test_observed_df.index):
						fig = mpl.manager.new(
							title = f'Reconstruction nx {nx} ny {ny} ntrigger {n_trigger}',
							subtitle = f'Training: {train_data_dir_path.parts[-1]}\nTesting: {test_data_dir_path.parts[-1]}',
						)
						regressor.do_reconstruction_plot(fig, observed_variables, real_position)
						mpl.manager.save_all(mkdir = this_analysis_dir_path/Path('reconstruction example plots'))
				except:
					pass
				reporter.update(1)
	
	print(f'Converting data to feather format...')
	pandas.read_csv(this_analysis_dir_path/Path('reconstruction_data.csv')).reset_index(drop=True).to_feather(this_analysis_dir_path/Path('reconstruction_data.fd').with_suffix('.fd'))
	(this_analysis_dir_path/Path('reconstruction_data.csv')).unlink() # Delete the CSV.
	
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='')
	parser.add_argument(
		'--dir-with-train-data',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement which has the train data.',
		required = True,
		dest = 'train_data_dir',
		type = str,
	)
	parser.add_argument(
		'--dir-with-test-data',
		metavar = 'path', 
		help = 'Path to a base directory of a measurement which has the test data.',
		required = True,
		dest = 'test_data_dir',
		type = str,
	)
	args = parser.parse_args()
	reconstruction_name = input('Reconstruction analysis name? ')
	script_core(
		train_data_dir_path = args.train_data_dir, 
		test_data_dir_path = args.test_data_dir, 
		reconstruction_name = reconstruction_name if reconstruction_name != '' else None,
	)
