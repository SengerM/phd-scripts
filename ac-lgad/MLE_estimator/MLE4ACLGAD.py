from ExperimentalParametricDensity import ExperimentalParametricDensity
import numpy
import myplotlib as mpl
import lmfit
import numpy as np

class MLE4ACLGAD:
	def __init__(self):
		pass
	
	@property
	def xx(self):
		if hasattr(self, '_trained_xx'):
			return self._trained_xx
		else:
			return None
	
	@property
	def yy(self):
		if hasattr(self, '_trained_yy'):
			return self._trained_yy
		else:
			return None
	
	def train(self, observed_df, answers_df):
		if hasattr(self, '_experimental_parametric_densities'):
			raise RuntimeError(f'This instance of {type(self)} has already been trained, cannot be trained again.')
		# <observed_df> and <answers_df> are pandas dataframes created by the script "pre_process_data_for_MLE.py".
		x_values = sorted(set(answers_df['x (m)']))
		y_values = sorted(set(answers_df['y (m)']))
		self._trained_xx, self._trained_yy = numpy.meshgrid(x_values, y_values)
		samples = {} # This is in accordance with the <samples> parameter for ExperimentalParametricDensity.__init__.
		for column in observed_df:
			if column in ['n_x', 'n_y', 'n_trigger']:
				continue
			samples[column] = {}
			for x in x_values:
				samples[column][x] = {}
				for y in y_values:
					samples[column][x][y] = list(observed_df[(answers_df['x (m)']==x) & (answers_df['y (m)']==y)][column])
					if any(np.isnan(samples[column][x][y])):
						raise ValueError(f"The training data contains NaN values which I don't know how to handle. Specifically the <observed_df> for x={x}, y={x} at the column {column}.")
					# ~ samples[column][x][y] = [s for s in samples[column][x][y] if not numpy.isnan(s)] # Remove NaN values because ExperimentalParametricDensity cannot handle them.
		
		self._experimental_parametric_densities = {}
		for column in samples:
			self._experimental_parametric_densities[column] = ExperimentalParametricDensity(samples[column])
	
	def likelihood(self, x, y, observed_variables: dict):
		# <observed_variables> must match the variables used for training.
		if not hasattr(self, '_experimental_parametric_densities'):
			raise RuntimeError(f'This instance of {type(self)} has not been trained, cannot reconstruct.')
		for idx, key in enumerate(observed_variables):
			if key not in self._experimental_parametric_densities:
				raise ValueError(f'The <observed_variables> arguments must match those used for training. I received {key} which is not in the variables used for training, which are {list(self._experimental_parametric_densities.keys())}.')
			if idx == 0:
				product = self._experimental_parametric_densities[key].likelihood(x=x, y=y, q=observed_variables[key])
			else:
				product *= self._experimental_parametric_densities[key].likelihood(x=x, y=y, q=observed_variables[key])
		return product
	
	def reconstruct_position(self, observed_variables: dict):
		params = lmfit.Parameters()
		params.add('x', value = numpy.mean(self._trained_xx), min = self._trained_xx.min(), max = self._trained_xx.max())
		params.add('y', value = numpy.mean(self._trained_yy), min = self._trained_yy.min(), max = self._trained_yy.max())
		def func2minimize(pars):
			parvals = pars.valuesdict()
			return -self.likelihood(parvals['x'],parvals['y'],observed_variables)
		minimizer = lmfit.Minimizer(
			func2minimize,
			params,
		)
		minimizer_result = minimizer.minimize(method = 'brute')
		minimizer = lmfit.Minimizer(
			func2minimize,
			minimizer_result.params,
		)
		minimizer_result = minimizer.minimize(method = 'nelder')
		
		return minimizer_result.params['x'].value, minimizer_result.params['y'].value
	
	def do_reconstruction_plot(self, fig, observed_variables: dict, real_position:tuple=None):
		# <real_position> = (x_real, y_real)
		reconstructed_x, reconstructed_y = self.reconstruct_position(observed_variables)
		reconstructed_position = (reconstructed_x, reconstructed_y)
		del(reconstructed_x)
		del(reconstructed_y)
		x_training_step = np.diff(sorted(set(self.xx.ravel())))[0]
		y_training_step = np.diff(sorted(set(self.yy.ravel())))[0]
		if real_position is None:
			start_x = reconstructed_position[0] - x_training_step
			end_x = reconstructed_position[0] + x_training_step
			start_y = reconstructed_position[1] - y_training_step
			end_y = reconstructed_position[1] + y_training_step
		else:
			start_x = min((reconstructed_position[0],real_position[0])) - x_training_step
			end_x = max((reconstructed_position[0],real_position[0])) + x_training_step
			start_y = min((reconstructed_position[1],real_position[1])) - y_training_step
			end_y = max((reconstructed_position[1],real_position[1])) + y_training_step
		x_values = np.linspace(start_x, end_x, int((end_x-start_x)/(x_training_step/10)))
		y_values = np.linspace(start_y, end_y, int((end_y-start_y)/(y_training_step/10)))
		x_values = x_values[(x_values >= self.xx.min())&(x_values <= self.xx.max())]
		y_values = y_values[(y_values >= self.yy.min())&(y_values <= self.yy.max())]
		xx, yy = np.meshgrid(
			x_values,
			y_values,
		)
		fig.set(
			xlabel = 'x (m)',
			ylabel = 'y (m)',
			aspect = 'equal',
		)
		likelihood = self.likelihood(xx, yy, observed_variables)
		if real_position is not None:
			likelihood[likelihood<.1*self.likelihood(real_position[0], real_position[1], observed_variables)] = float('NaN')
		fig.contour(
			x = xx,
			y = yy,
			z = likelihood,
			colorscalelabel = 'Likelihood function',
			norm = 'log',
		)
		fig.plot(
			x = self.xx.ravel(),
			y = self.yy.ravel(),
			linestyle = '',
			marker = 'o',
			label = 'Training points',
			color = (0,0,0),
		)
		if real_position is not None:
			fig.plot(
				[real_position[0], reconstructed_position[0]],
				[real_position[1], reconstructed_position[1]],
				color = (0,0,0),
				label = f'Reconstruction error: {np.linalg.norm(np.array(reconstructed_position)-np.array(real_position)):.2e} m',
			)
			fig.plot(
				[real_position[0]],
				[real_position[1]],
				linestyle = '',
				marker = '+',
				color = (0,1,0),
				label = f'Real position',
			)
		fig.plot(
			[reconstructed_position[0]],
			[reconstructed_position[1]],
			linestyle = '',
			marker = 'x',
			color = (0,0,0),
			label = f'Reconstructed position',
		)
		

if __name__ == '__main__':
	import pandas
	import myplotlib as mpl
	
	observed_df = pandas.read_csv('/home/alf/cernbox/measurements/AC-LGAD/20210224075953_AC-LGAD_Paul_MIP_10um_555trigs/processed_by_script_pre_process_data_for_MLE/data for MLE observed variables.csv')
	answers_df = pandas.read_csv('/home/alf/cernbox/measurements/AC-LGAD/20210224075953_AC-LGAD_Paul_MIP_10um_555trigs/processed_by_script_pre_process_data_for_MLE/data for MLE answers.csv')
	
	reconstructor = MLE4ACLGAD()
	reconstructor.train(observed_df, answers_df)
	
	nx = 9
	ny = 6
	n_trigger = 44
	
	observed_variables = {f'Charge fraction for CH{i}': float(observed_df.loc[(observed_df['n_x']==nx)&(observed_df['n_y']==ny)&(observed_df['n_trigger']==n_trigger), f'Charge fraction for CH{i}']) for i in [1,2,3,4]}
	real_position = tuple([float(answers_df.loc[(answers_df['n_x']==nx)&(answers_df['n_y']==ny)&(answers_df['n_trigger']==n_trigger), col]) for col in ['x (m)', 'y (m)']])

	fig = mpl.manager.new()
	reconstructor.do_reconstruction_plot(
		fig, 
		observed_variables,
		real_position = real_position,
	)
	fig.show()
