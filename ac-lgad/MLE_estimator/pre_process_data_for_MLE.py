from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas

CHANNELS = [f'CH{i}' for i in [1,2,3,4]]

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	try:
		data = pandas.read_feather(bureaucrat.processed_by_script_dir_path('scan_xy_4_channels_many_triggers_per_point.py')/Path('measured_data.fd'))
	except FileNotFoundError:
		data = pandas.read_csv(
			bureaucrat.processed_by_script_dir_path('scan_xy_4_channels_many_triggers_per_point.py')/Path('measured_data.csv'),
			sep = '\t',
		)
	
	# Create data frame for the MLE algorithm ----
	data_for_MLE = {}
	data_for_MLE['observed variables'] = data.loc[data['n_channel']==1,['n_x', 'n_y', 'n_trigger']].copy().reset_index(drop=True)
	data_for_MLE['answers'] = data.loc[data['n_channel']==1,['n_x', 'n_y', 'n_trigger', 'x (m)', 'y (m)']].copy().reset_index(drop=True)
	
	total_charges = sum([np.array(data.loc[data['n_channel']==n_channel, 'Collected charge (a.u.)']) for n_channel in sorted(set(data['n_channel']))])
	for ch in CHANNELS:
		data_for_MLE['observed variables'][f'Collected charge {ch} (a.u.)'] = np.array(data[data['n_channel']==int(ch[-1])]['Collected charge (a.u.)'])
		# ~ data_for_MLE['observed variables'][f'Charge fraction {ch}'] = np.array(data[data['n_channel']==int(ch[-1])]['Collected charge (a.u.)'])/total_charges
	
	data_for_MLE['observed variables'] = data_for_MLE['observed variables'].dropna()
	
	for key in data_for_MLE:
		data_for_MLE[key].loc[data_for_MLE['observed variables'].index,:].reset_index(drop=True).to_feather(bureaucrat.processed_data_dir_path/Path(f'data for MLE {key}.fd'))
	
	x_values = sorted(set(data_for_MLE['answers']['x (m)']))
	y_values = sorted(set(data_for_MLE['answers']['y (m)']))
	xx, yy = np.meshgrid(x_values, y_values)
	for col in data_for_MLE['observed variables']:
		if col in ['n_x', 'n_y', 'n_trigger', 'x (m)', 'y (m)', 'z (m)', 'n_channel', 'n_pulse']:
			continue
		fig = mpl.manager.new(
			title = f'xy map for {col}',
			subtitle = f'Dataset {bureaucrat.measurement_name}',
			xlabel = 'x (m)',
			ylabel = 'y (m)',
			aspect = 'equal',
			# ~ package = 'ds9',
		)
		fig.colormap(
			x = xx,
			y = yy,
			z = data_for_MLE['observed variables'].pivot_table(col, 'n_y', 'n_x', aggfunc = np.nanmean),
			colorscalelabel = col,
		)
		mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('colormap plots for observation data'.replace(' ','_')))
		
		fig = mpl.manager.new(
			title = f'Distribution of {col}',
			subtitle = f'Dataset: {bureaucrat.measurement_name}',
			xlabel = col,
			ylabel = 'Number of events',
		)
		fig.hist(
			data_for_MLE['observed variables'][col],
		)
		mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('distributions plots for observation data'.replace(' ', '_')))
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Takes the measured data (e.g. collected charge) and produces the data to feed into the MLE position reconstructor (e.g. charge fraction) either for training or reconstructing.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)

