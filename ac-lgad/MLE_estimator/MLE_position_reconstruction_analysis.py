from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
from progressreporting.TelegramProgressReporter import TelegramProgressReporter
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
import pickle

def script_core(dir_with_reconstructor_measurement, dataset_path, reconstruction_name):
	bureaucrat = Bureaucrat(
		dir_with_reconstructor_measurement,
		variables = locals(),
	)
	
	this_analysis_dir_path = bureaucrat.processed_data_dir_path/Path(f'{bureaucrat.this_run_timestamp}_reconstruction_analysis_{reconstruction_name.replace(" ", "_")}')
	this_analysis_dir_path.mkdir(parents=True)
	
	print('Reading data for reconstruction...')
	observed_df = pandas.read_csv(Path(dataset_path)/Path('data for MLE observed variables.csv'))
	answers_df = pandas.read_csv(Path(dataset_path)/Path('data for MLE answers.csv'))
	
	for fname_____ in ['data for MLE observed variables.csv', 'data for MLE answers.csv']:
		(this_analysis_dir_path/Path(fname_____)).write_text((Path(dataset_path)/Path(fname_____)).read_text())
	
	print(f'Loading reconstructor...')
	with open(bureaucrat.processed_by_script_dir_path('MLE_training_script.py')/Path('MLE_position_reconstructor.pkl'), 'rb') as ifile:
		reconstructor = pickle.load(ifile)
	
	print(f'Starting reconstruction of events...')
	nx_values = sorted(set(observed_df['n_x']))
	ny_values = sorted(set(observed_df['n_y']))
	n_trigger_values = sorted(set(observed_df['n_trigger']))
	
	with open(this_analysis_dir_path/Path('metadata.txt'), 'w') as ofile:
		print(f'dataset_path = {dataset_path}', file = ofile)
	
	with TelegramProgressReporter(len(observed_df.index), TelegramReportingInformation().token, TelegramReportingInformation().chat_id, f'MLE position reconstruction analysis using {bureaucrat.measurement_name} (timestamp of this analysis: {bureaucrat.this_run_timestamp})') as reporter:
		with open(this_analysis_dir_path/Path('reconstruction_data.csv'), 'w') as ofile:
			print('n_x,n_y,n_trigger,real_x (m),real_y (m),reconstructed_x (m),reconstructed_y (m)', file = ofile)
		for index, row_observed_df in observed_df.iterrows():
			row_answers_df = answers_df.loc[index, :]
			nx = int(row_observed_df['n_x'])
			ny = int(row_observed_df['n_y'])
			n_trigger = int(row_observed_df['n_trigger'])
			print(f'Reconstructing event nx {nx} ny {ny} ntrigger {n_trigger}')
			if row_observed_df['n_x'] != row_answers_df['n_x'] or row_observed_df['n_y'] != row_answers_df['n_y'] or row_observed_df['n_trigger'] != row_answers_df['n_trigger']:
				# This should never happen, but just in case...
				raise RuntimeError(f'The events in the <observed_df> and <answers_df> do not coincide, specifically for row {index}.')
			observed_variables = {}
			for col in observed_df:
				if col in ['n_x','n_y','n_trigger']:
					continue
				observed_variables[col] = float(observed_df.loc[(observed_df['n_x']==nx)&(observed_df['n_y']==ny)&(observed_df['n_trigger']==n_trigger), col])
			reconstructed_position = reconstructor.reconstruct_position(observed_variables)
			real_position = tuple([float(answers_df.loc[(answers_df['n_x']==nx)&(answers_df['n_y']==ny)&(answers_df['n_trigger']==n_trigger), col]) for col in ['x (m)', 'y (m)']])
			with open(this_analysis_dir_path/Path('reconstruction_data.csv'), 'a') as ofile:
				print(f'{nx},{ny},{n_trigger},{real_position[0]},{real_position[1]},{reconstructed_position[0]},{reconstructed_position[1]}', file = ofile)
			if np.random.random() < 44/len(observed_df.index):
				fig = mpl.manager.new(
					title = f'Reconstruction nx {nx} ny {ny} ntrigger {n_trigger}',
					subtitle = f'Reconstructor: {bureaucrat.measurement_name}\nDataset: {Path(dataset_path).parts[-2]}',
				)
				reconstructor.do_reconstruction_plot(fig, observed_variables, real_position)
				mpl.manager.save_all(mkdir = this_analysis_dir_path/Path('reconstruction example plots'))
			reporter.update(1)
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='')
	parser.add_argument(
		'--dir-with-reconstructor-measurement',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement which has already been used to train a reconstructor.',
		required = True,
		dest = 'directory_reconstructor',
		type = str,
	)
	parser.add_argument(
		'--dataset-for-analysis',
		metavar = 'path', 
		help = 'Path to a directory created by the script "pre_process_data_for_MLE.py" which contains the observed data and the answers for reconstruction.',
		required = True,
		dest = 'dataset_path',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory_reconstructor, args.dataset_path, input('Reconstruction analysis name? '))

