from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
from progressreporting.TelegramProgressReporter import TelegramProgressReporter
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
import pickle
from scipy.stats import norm
import plotly.graph_objects as go

STATS = ['mean', 'std']
RECTANGLES_CORNERS = [
# For not using any rectangle region just set RECTANGLE_CORNERS = None
	(.02612977, .03196616, .02618292, .03201021),
	(.02612977, .03214535, .02618292, .03220742),
	(.02602548, .03205325, .02607763, .03211031),
	(.02621802, .03205325, .0262802, .03211031),
]
RECTANGLES_COLORS = [
	(22/255, 171/255, 9/255), 
	(1,0,0),
	(25/255, 255/255, 247/255),
	(244/255, 250/255, 60/255),
]

def script_core(dir_with_measurement, analysis_name = None):
	bureaucrat = Bureaucrat(
		dir_with_measurement,
		variables = locals(),
	)
	if analysis_name is None:
		analysis_name = sorted([f for f in bureaucrat.processed_by_script_dir_path('MLE_position_reconstruction.py').iterdir() if f.is_dir()])[-1].parts[-1]
	with open(bureaucrat.processed_by_script_dir_path('MLE_position_reconstruction.py')/Path(analysis_name)/Path('metadata.txt'), 'r') as ifile:
		for line in ifile:
			if 'train_data_dir_path' in line:
				mle_training_dataset_path = Path(line.replace('train_data_dir_path = ','').replace('\n',''))
				break
	
	try:
		analysis_data_df = pandas.read_feather(bureaucrat.processed_by_script_dir_path('MLE_position_reconstruction.py')/Path(analysis_name)/Path('reconstruction_data.fd'))
	except FileNotFoundError:
		analysis_data_df = pandas.read_csv(bureaucrat.processed_by_script_dir_path('MLE_position_reconstruction.py')/Path(analysis_name)/Path('reconstruction_data.csv'))
	analysis_data_df['Reconstruction error (m)'] = ((analysis_data_df['real_x (m)']-analysis_data_df['reconstructed_x (m)'])**2 + (analysis_data_df['real_y (m)']-analysis_data_df['reconstructed_y (m)'])**2)**.5
	
	# Special region manual analysis ---
	OUTER_SQUARE_CORNERS = [.02600342+11e-6, .03193713+11e-6, .02629925-5e-6, .03222243-5e-6]
	INNER_SQUARE_CORNERS = [.02610068, .03202522, .02620899, .03213634]
	SPECIAL_REGION_COLOR = (0,0,0)
	x = analysis_data_df['real_x (m)']
	y = analysis_data_df['real_y (m)']
	events_in_special_region = analysis_data_df.loc[
		((x>OUTER_SQUARE_CORNERS[0]) & (x<OUTER_SQUARE_CORNERS[2]) & (y>OUTER_SQUARE_CORNERS[1]) & (y<OUTER_SQUARE_CORNERS[3])) & # Select events inside outer square.
		(~((x>INNER_SQUARE_CORNERS[0]) & (x<INNER_SQUARE_CORNERS[2]) & (y>INNER_SQUARE_CORNERS[1]) & (y<INNER_SQUARE_CORNERS[3]))) # Remove events outside inner square.
	]
	del(x, y)
	
	x_values = sorted(set(analysis_data_df['real_x (m)']))
	y_values = sorted(set(analysis_data_df['real_y (m)']))
	xx, yy = np.meshgrid(x_values, y_values)
	reconstruction_error = {}
	for stat in STATS:
		reconstruction_error[stat] = np.zeros(xx.shape)
		reconstruction_error[stat][:] = float('NaN')
		if stat == 'mean':
			reconstruction_error[stat] = analysis_data_df.pivot_table('Reconstruction error (m)', 'n_y', 'n_x', aggfunc = np.nanmean)
		elif stat == 'std':
			reconstruction_error[stat] = analysis_data_df.pivot_table('Reconstruction error (m)', 'n_y', 'n_x', aggfunc = np.nanstd)
		# ~ reconstruction_error[stat][reconstruction_error[stat] > 20e-6] = float('NaN')
		
		fig = mpl.manager.new(
			title = f'Reconstruction error {stat}',
			subtitle = f'Dataset: {bureaucrat.measurement_name}\nMLE training: {mle_training_dataset_path.parts[-1]}',
			xlabel = 'x (m)',
			ylabel = 'y (m)',
			aspect = 'equal',
		)
		fig.colormap(
			x = xx,
			y = yy,
			z = reconstruction_error[stat],
			colorscalelabel = f'{"Average reconstruction error" if stat=="mean" else "Reconstruction error std"} (m)',
		)
		fig.plotly_fig['data'][0]['zmin'] = 0
		# ~ fig.plotly_fig['data'][0]['zmax'] = 33e-6
		if RECTANGLES_CORNERS is not None:
			k = 0
			for rectangle_corners, rectangle_color in zip(RECTANGLES_CORNERS, RECTANGLES_COLORS):
				k += 1
				fig.plot(
					[rectangle_corners[0], rectangle_corners[2], rectangle_corners[2], rectangle_corners[0], rectangle_corners[0]],
					[rectangle_corners[1], rectangle_corners[1], rectangle_corners[3], rectangle_corners[3], rectangle_corners[1]],
					color = rectangle_color,
					label = f'Region {k}',
				)
		fig.plot(
			[OUTER_SQUARE_CORNERS[0], OUTER_SQUARE_CORNERS[2], OUTER_SQUARE_CORNERS[2], OUTER_SQUARE_CORNERS[0], OUTER_SQUARE_CORNERS[0]] + [INNER_SQUARE_CORNERS[0], INNER_SQUARE_CORNERS[2], INNER_SQUARE_CORNERS[2], INNER_SQUARE_CORNERS[0], INNER_SQUARE_CORNERS[0]],
			[OUTER_SQUARE_CORNERS[1], OUTER_SQUARE_CORNERS[1], OUTER_SQUARE_CORNERS[3], OUTER_SQUARE_CORNERS[3], OUTER_SQUARE_CORNERS[1]] + [INNER_SQUARE_CORNERS[1], INNER_SQUARE_CORNERS[1], INNER_SQUARE_CORNERS[3], INNER_SQUARE_CORNERS[3], INNER_SQUARE_CORNERS[1]],
			color = SPECIAL_REGION_COLOR,
			label = f'Main region',
		)
		mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path(analysis_name))

	fig = mpl.manager.new(
		title = f'Reconstruction error distribution',
		subtitle = f'Dataset: {bureaucrat.measurement_name}\nMLE training: {mle_training_dataset_path.parts[-1]}',
		xlabel = f'Reconstruction error (m)',
		ylabel = 'Number of events',
	)
	bins = np.arange(0,555e-6,5e-6)
	colors = [(.3,.3,.9), (189/255, 21/255, 21/255), (255/255, 153/255, 153/255), (16/255, 173/255, 18/255), (156/255, 255/255, 157/255)]
	fig.hist(
		analysis_data_df['Reconstruction error (m)'],
		bins = bins,
		label = 'All data',
		color = colors[0],
	)
	
	if RECTANGLES_CORNERS is not None:
		k = 0
		for rectangle_corners, rectangle_color in zip(RECTANGLES_CORNERS, RECTANGLES_COLORS):
			k += 1
			rectangle_rows = (analysis_data_df['real_x (m)'] <= rectangle_corners[2]) & (analysis_data_df['real_x (m)'] >= rectangle_corners[0]) & (analysis_data_df['real_y (m)'] >= rectangle_corners[1]) & (analysis_data_df['real_y (m)'] <= rectangle_corners[3])
			fig.hist(
				analysis_data_df.loc[rectangle_rows, 'Reconstruction error (m)'],
				bins = bins,
				label = f'Region {k}',
				color = rectangle_color,
			)
	fig.hist(
		events_in_special_region['Reconstruction error (m)'],
		bins = bins,
		label = f'Main region',
		color = SPECIAL_REGION_COLOR,
	)
	
	analyses_results = {
		'region number': list(np.array(range(len(RECTANGLES_CORNERS)))+1),
		'x_mean (m)': [None]*len(RECTANGLES_CORNERS),
		'y_mean (m)': [None]*len(RECTANGLES_CORNERS),
		'x_std (m)': [None]*len(RECTANGLES_CORNERS),
		'y_std (m)': [None]*len(RECTANGLES_CORNERS),
	}
	for idx,corner in enumerate(['x1','y1','x2','y2']):
		analyses_results[corner] = [rect[idx] for rect in RECTANGLES_CORNERS]
	analyses_results = pandas.DataFrame(analyses_results)
	for xy in ['x', 'y']:
		fig = mpl.manager.new(
			title = f'{xy} reconstruction error distribution',
			subtitle = f'Dataset: {bureaucrat.measurement_name}\nMLE training: {mle_training_dataset_path.parts[-1]}',
			xlabel = f'{xy} reconstruction error (m)',
			ylabel = 'Number of events',
			yscale = 'log',
		)
		xy_reconstruction_error = (analysis_data_df[f'real_{xy} (m)'] - analysis_data_df[f'reconstructed_{xy} (m)']).to_numpy()
		bins = np.linspace(xy_reconstruction_error.min(), xy_reconstruction_error.max(), int((xy_reconstruction_error.max()-xy_reconstruction_error.min())/1e-6))
		fig.hist(
			xy_reconstruction_error,
			bins = bins,
			label = 'All data',
			color = colors[0],
		)
		if RECTANGLES_CORNERS is not None:
			k = 0
			for rectangle_corners, rectangle_color in zip(RECTANGLES_CORNERS, RECTANGLES_COLORS):
				k += 1
				rectangle_rows = (analysis_data_df['real_x (m)'] <= rectangle_corners[2]) & (analysis_data_df['real_x (m)'] >= rectangle_corners[0]) & (analysis_data_df['real_y (m)'] >= rectangle_corners[1]) & (analysis_data_df['real_y (m)'] <= rectangle_corners[3])
				fig.hist(
					xy_reconstruction_error[rectangle_rows],
					bins = bins,
					label = f'Region {k}',
					color = rectangle_color,
				)
				rows_for_fit = rectangle_rows & (xy_reconstruction_error<99e-6)&(xy_reconstruction_error>-99e-6)
				µ, σ = norm.fit(xy_reconstruction_error[rows_for_fit])
				x_axis_for_plot = np.linspace(xy_reconstruction_error[rows_for_fit].min(), xy_reconstruction_error[rows_for_fit].max(), 99)
				yvals_fit = norm.pdf(x_axis_for_plot, µ, σ)*len(xy_reconstruction_error[rectangle_rows])*np.diff(bins)[0]
				fig.plot(
					x_axis_for_plot[yvals_fit>1],
					yvals_fit[yvals_fit>1],
					label = f'Fit (µ={µ*1e6:.2f} µm, σ={σ*1e6:.2f} µm)',
					color = rectangle_color,
				)
				analyses_results.loc[k-1,f'{xy}_mean (m)'] = µ
				analyses_results.loc[k-1,f'{xy}_std (m)'] = σ
		
		xy_reconstruction_error = (events_in_special_region[f'real_{xy} (m)'] - events_in_special_region[f'reconstructed_{xy} (m)']).to_numpy()
		fig.hist(
			xy_reconstruction_error,
			bins = bins,
			label = f'Main region',
			color = SPECIAL_REGION_COLOR,
		)
		print(f'events_in_special_region sigma_{xy} = {xy_reconstruction_error.std()} m')
		rows_for_fit = (xy_reconstruction_error<99e-6)&(xy_reconstruction_error>-99e-6)
		µ, σ = norm.fit(xy_reconstruction_error[rows_for_fit])
		x_axis_for_plot = np.linspace(xy_reconstruction_error[rows_for_fit].min(), xy_reconstruction_error[rows_for_fit].max(), 99)
		yvals_fit = norm.pdf(x_axis_for_plot, µ, σ)*len(xy_reconstruction_error)*np.diff(bins)[0]
		fig.plot(
			x_axis_for_plot[yvals_fit>1],
			yvals_fit[yvals_fit>1],
			label = f'Fit (µ={µ*1e6:.2f} µm, σ={σ*1e6:.2f} µm)',
			color = SPECIAL_REGION_COLOR,
		)
		
	analyses_results.to_csv(bureaucrat.processed_data_dir_path/Path(analysis_name)/Path('analyses_results.csv'), index=False)
	
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path(analysis_name))
	
	# ~ # Covariance matrix plot ---
	
	# ~ df_for_covariance_plot = analysis_data_df.drop(['n_x','n_y','n_trigger'], axis=1)
	# ~ for xy in {'x','y'}:
		# ~ df_for_covariance_plot[f'{xy}_error (m)'] = df_for_covariance_plot[f'real_{xy} (m)'] - df_for_covariance_plot[f'reconstructed_{xy} (m)']
		# ~ df_for_covariance_plot.drop([f'reconstructed_{xy} (m)'], axis=1, inplace=True)
	# ~ df_for_covariance_plot = df_for_covariance_plot.sample(n=399999)
	
	# ~ fig = mpl.manager.new()
	# ~ fig.plotly_figure = go.Figure(
		# ~ data = go.Splom(
			# ~ dimensions = [dict(label=col, values=df_for_covariance_plot[col]) for col in df_for_covariance_plot],
			# ~ diagonal_visible = False,
			# ~ showupperhalf = False,
			# ~ marker = dict(
				# ~ size = 1,
			# ~ )
		# ~ ),
	# ~ ).show()
	# ~ fig.set(
		# ~ title = f'Scatter matrix plot',
		# ~ subtitle = f'Data set: {bureaucrat.measurement_name}',
	# ~ )
	# ~ mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path(analysis_name))
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement which has already been analyzed with some MLE algorithm.',
		required = True,
		dest = 'dir',
		type = str,
	)
	parser.add_argument(
		'--name-of-analysis',
		metavar = 'path', 
		help = 'Name of one directory inside "MLE_position_reconstruction", for example "20210227161803_reconstruction_analysis". If omitted, the last analysis in the directory "MLE_position_reconstruction" is processed.',
		required = False,
		dest = 'analysis_name',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.dir, args.analysis_name)

