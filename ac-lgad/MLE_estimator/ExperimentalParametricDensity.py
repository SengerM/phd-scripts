import numpy as np
from scipy.stats.kde import gaussian_kde
from scipy import interpolate

def check_number_or_numpy_array(var, name: str):
	if not (isinstance(var,int) or isinstance(var,float) or isinstance(var, np.ndarray)):
		raise TypeError(f'<{name}> must be a number (int or float) or a numpy array, received instead <{name}> of type {type(var)}.')

class ExperimentalParametricDensity:
	def __init__(self, samples: dict):
		# <samples> is a dict that contains the data in each xy sampled point, for example samples[x][y] = samples_array_at_x_y.
		# Assuming that the x,y points are distributed in a rectangular mesh, i.e. it is just a matrix.
		self._samples = samples
		self._x_vals = sorted([x for x in samples])
		self._y_vals = sorted([y for y in samples[self._x_vals[0]]]) # Here I am using that the x,y points are distributed in a rectangular mesh.
		
	@property
	def means(self):
		if not hasattr(self, '_means'):
			self._means = np.zeros((len(self._y_vals), len(self._x_vals)))
			self._means[:] = float('NaN')
			for nx,x in enumerate(self._x_vals):
				for ny,y in enumerate(self._y_vals):
					self._means[ny,nx] = np.nanmean(self._samples[x][y])
		return self._means
	
	@property
	def xx(self):
		if not hasattr(self, '_xx'):
			xx,yy = np.meshgrid(self._x_vals, self._y_vals)
			self._xx = xx
			self._yy = yy
		return self._xx
	
	@property
	def yy(self):
		if not hasattr(self, '_yy'):
			self.xx
		return self._yy
	
	@property
	def kde(self):
		# Returns a dictionary with the KDE at each sampled x,y. This means that returns the same as self._samples but instead of having samples it has the KDE functions. Each KDE function has the signature KDE(q).
		if not hasattr(self, '_kde'):
			self._kde = {}
			for x in self._x_vals:
				self._kde[x] = {}
				for y in self._y_vals:
					self._kde[x][y] = gaussian_kde(self._samples[x][y])
		return self._kde
	
	def __call__(self, q, x, y):
		try:
			x = float(x)
		except:
			raise TypeError(f'<x> must be a number (int or float), received instead <x> of type {type(x)}.')
		try:
			y = float(y)
		except:
			raise TypeError(f'<y> must be a number (int or float), received instead <y> of type {type(y)}.')
		if not min(self._x_vals) <= x <= max(self._x_vals):
			raise ValueError(f'<x> must be bounded within the sampling region. The minimum value for <x> is {min(self._x_vals)} and the maximum {max(self._x_vals)}, received x = {x}.')
		if not min(self._y_vals) <= y <= max(self._y_vals):
			raise ValueError(f'<y> must be bounded within the sampling region. The minimum value for <y> is {min(self._y_vals)} and the maximum {max(self._y_vals)}, received y = {y}.')
		check_number_or_numpy_array(q, 'q')
		# If we are here is because x,y are both numbers and within the sampling range.
		if x == self._x_vals[0]: idx_prev_x = 0
		else: idx_prev_x = np.where((np.array(self._x_vals)<x))[0][-1]
		if y == self._y_vals[0]: idx_prev_y = 0
		else: idx_prev_y = np.where((np.array(self._y_vals)<y))[0][-1]
		x0 = self._x_vals[idx_prev_x]
		y0 = self._y_vals[idx_prev_y]
		x1 = self._x_vals[idx_prev_x+1]
		y1 = self._y_vals[idx_prev_y+1]
		µ00 = self.means[idx_prev_y,idx_prev_x]
		µ01 = self.means[idx_prev_y+1,idx_prev_x]
		µ10 = self.means[idx_prev_y,idx_prev_x+1]
		µ11 = self.means[idx_prev_y+1,idx_prev_x+1]
		f00 = self.kde[x0][y0]
		f01 = self.kde[x0][y1]
		f10 = self.kde[x1][y0]
		f11 = self.kde[x1][y1]
		µ = interpolate.interp2d(x=[x0,x0,x1,x1], y=[y0,y1,y0,y1], z=[µ00,µ01,µ10,µ11])(x,y)
		return (f00(q+µ00-µ)*(x1-x)*(y1-y) + f01(q+µ01-µ)*(x1-x)*(y-y0) + f10(q+µ10-µ)*(x-x0)*(y1-y) + f11(q+µ11-µ)*(x-x0)*(y-y0))/(x1-x0)/(y1-y0)
	
	def likelihood(self, x, y, q):
		if not (isinstance(q,int) or isinstance(q,float)):
			raise TypeError(f'<q> must be a number (int or float), received instead <q> of type {type(q)}.')
		check_number_or_numpy_array(x, 'x')
		check_number_or_numpy_array(y, 'y')
		if isinstance(x, float) or isinstance(x, int):
			x = np.array([x])
		if isinstance(y, float) or isinstance(y, int):
			y = np.array([y])
		if x.shape != y.shape:
			raise ValueError(f'The shape of <x> and <y> must be the same. Received x.shape = {x.shape} and y.shape = {y.shape}.')
		ll = []
		for xx,yy in zip(x.ravel(),y.ravel()):
			ll.append(self(q,xx,yy))
		ll = np.reshape(np.array(ll),x.shape)
		if x.shape == 1:
			ll = ll[0]
		return ll
		

if __name__ == '__main__':
	import myplotlib as mpl # https://github.com/SengerM/myplotlib
	import lmfit
	import palettable
	
	def generate_samples(x,y):
		return list(np.random.randn(99999)*(1+x)+y+x) + list((np.random.rand(9999)-.5)*3*(1+x)+y+x)

	X_VALS = [0,1,2,3]
	Y_VALS = [0,1,2,3]

	data = {}
	qmin = 0
	qmax = 0
	for x in X_VALS:
		data[x] = {}
		for y in Y_VALS:
			data[x][y] = generate_samples(x,y)
			qmin = min(qmin, min(data[x][y]))
			qmax = max(qmax, max(data[x][y]))

	epd = ExperimentalParametricDensity(data)
	
	fig = mpl.manager.new(
		title = 'Measured and estimated distributions at each x,y',
		xlabel = 'q',
		ylabel = 'Probability density',
	)
	qaxis = np.linspace(qmin, qmax,99)
	x0 = 1.1
	y0 = 2.6
	n = 0
	for x in X_VALS:
		for y in Y_VALS:
			n += 1
			color = tuple(np.array(palettable.tableau.Tableau_20.colors[n%20])/255)
			fig.hist(
				data[x][y],
				label = f'Measured q at x={x}, y={y}',
				density = True,
				color = color
			)
			fig.plot(
				qaxis,
				epd.kde[x][y](qaxis),
				label = f'KDE at x={x}, y={y}',
				color = color,
				linestyle = '--',
			)
	fig.plot(
		qaxis,
		epd(qaxis, x0, y0),
		label = f'Interpolation example at x={x0}, y={y0}',
	)

	fig = mpl.manager.new(
		title = f'Likelihood plot',
		xlabel = 'x',
		ylabel = 'y',
		aspect = 'equal',
	)
	q0 = 6
	xaxis = np.linspace(min(X_VALS), max(X_VALS))
	yaxis = np.linspace(min(Y_VALS), max(Y_VALS))
	xx,yy = np.meshgrid(xaxis,yaxis)
	fig.contour(
		x = xx,
		y = yy,
		z = epd.likelihood(xx, yy, q0),
		colorscalelabel = f'Likelihood(q={q0}|x,y)',
	)
	xx,yy = np.meshgrid(X_VALS,Y_VALS)
	fig.plot(
		x = xx.ravel(),
		y = yy.ravel(),
		marker = '.',
		linestyle = '',
		color = (0,0,0),
	)
	
	# Now let's find the maximum of the likelihood:
	params = lmfit.Parameters()
	params.add('x', value = 0, min = min(X_VALS), max = max(X_VALS))
	params.add('y', value = 0, min = min(Y_VALS), max = max(Y_VALS))
	def func2minimize(pars):
		parvals = pars.valuesdict()
		return 1/epd.likelihood(parvals['x'],parvals['y'],q0)
	minimizer = lmfit.Minimizer(
		func2minimize,
		params,
	)
	minimizer_result = minimizer.minimize(method = 'nelder')
	print(minimizer_result.params)
	
	fig.plot(
		[minimizer_result.params['x'].value],
		[minimizer_result.params['y'].value],
		marker = '.',
		color = (1,0,0),
	)

	mpl.manager.show()
