from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
import pickle
from MLE4ACLGAD import MLE4ACLGAD

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	print('Reading training data...')
	try:
		observed_df = pandas.read_featehr(bureaucrat.processed_by_script_dir_path('pre_process_data_for_MLE.py')/Path('data for MLE observed variables.fd'))
		answers_df = pandas.read_feather(bureaucrat.processed_by_script_dir_path('pre_process_data_for_MLE.py')/Path('data for MLE answers.fd'))
	except:
		observed_df = pandas.read_csv(bureaucrat.processed_by_script_dir_path('pre_process_data_for_MLE.py')/Path('data for MLE observed variables.csv'))
		answers_df = pandas.read_csv(bureaucrat.processed_by_script_dir_path('pre_process_data_for_MLE.py')/Path('data for MLE answers.csv'))
	
	for fname_____ in ['data for MLE observed variables.csv', 'data for MLE answers.csv']:
		(bureaucrat.processed_data_dir_path/Path(fname_____)).write_text((bureaucrat.processed_by_script_dir_path('pre_process_data_for_MLE.py')/Path(fname_____)).read_text())
	
	print('Training...')
	reconstructor = MLE4ACLGAD()
	reconstructor.train(observed_df, answers_df)
	
	print('Succesfully trained, saving the reconstructor...')
	with open(bureaucrat.processed_data_dir_path/Path('MLE_position_reconstructor.pkl'), 'wb') as ofile:
		pickle.dump(reconstructor, ofile)
	
	print('Making some example reconstruction plots...')
	nx_values = sorted(set(observed_df['n_x']))
	ny_values = sorted(set(observed_df['n_y']))
	n_trigger_values = sorted(set(observed_df['n_trigger']))
	for k in range(9):
		nx = nx_values[np.random.randint(len(nx_values))]
		ny = ny_values[np.random.randint(len(ny_values))]
		n_trigger = n_trigger_values[np.random.randint(len(n_trigger_values))]
		fig = mpl.manager.new(
			title = f'Reconstruction example for nx {nx} ny {ny} n_trigger {n_trigger}',
			subtitle = f'Training and reconstruction dataset: {bureaucrat.measurement_name}',
		)
		observed_variables = {}
		for col in observed_df:
			if col in ['n_x','n_y','n_trigger']:
				continue
			observed_variables[col] = float(observed_df.loc[(observed_df['n_x']==nx)&(observed_df['n_y']==ny)&(observed_df['n_trigger']==n_trigger), col])
		
		real_position = tuple([float(answers_df.loc[(answers_df['n_x']==nx)&(answers_df['n_y']==ny)&(answers_df['n_trigger']==n_trigger), col]) for col in ['x (m)', 'y (m)']])
		reconstructor.do_reconstruction_plot(
			fig,
			observed_variables,
			real_position,
		)
		mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('reconstruction plot examples'))
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Creates and trains a MLE position reconstructor from a data set.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)

