from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas

CHANNELS = [f'CH{i}' for i in [1,2,3,4]]

def script_core(directory, analysis_name=None):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	if analysis_name is None:
		analysis_name = sorted([fpath for fpath in bureaucrat.processed_by_script_dir_path('MLE_position_reconstruction_1D.py').iterdir() if fpath.is_dir()])[-1].parts[-1]
	data_df = pandas.read_csv(bureaucrat.processed_by_script_dir_path('MLE_position_reconstruction_1D.py')/Path(analysis_name)/Path('reconstruction_data.csv'))
	
	fig = mpl.manager.new(
		title = '1D position reconstruction',
		subtitle = f'Dataset: {bureaucrat.measurement_name}\nReconstruction: {analysis_name}',
		xlabel = 'Real distance (m)',
		ylabel = 'Reconstructed distance (m)',
	)
	real_distance = data_df.groupby(['n_pos']).mean()['real_distance (m)']
	reconstructed_mean = data_df.groupby(['n_pos']).mean()['reconstructed_distance (m)']
	reconstructed_std = data_df.groupby(['n_pos']).std()['reconstructed_distance (m)']
	fig.error_band(
		x = real_distance,
		y = reconstructed_mean,
		ytop = reconstructed_mean + reconstructed_std,
		ylow = reconstructed_mean - reconstructed_std,
		label = 'Data',
	)
	fig.plot(
		real_distance,
		real_distance,
		label = 'y = x',
		color = (0,0,0),
		linewidth = .5,
	)
	
	fig = mpl.manager.new(
		title = '1D position reconstruction std',
		subtitle = f'Dataset: {bureaucrat.measurement_name}\nReconstruction: {analysis_name}',
		xlabel = 'Real distance (m)',
		ylabel = 'Reconstructed distance std (m)',
		yscale = 'log',
	)
	fig.error_band(
		x = real_distance,
		y = reconstructed_std,
		ytop = reconstructed_std,
		ylow = reconstructed_std*0,
		label = 'Measured',
	)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path(analysis_name))
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Takes the measured data (e.g. collected charge) and produces the data to feed into the MLE position reconstructor (e.g. charge fraction) either for training or reconstructing.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	parser.add_argument(
		'--analysis-name',
		metavar = 'str', 
		help = 'Name of one sub-directory inside "MLE_position_reconstruction_1D". If not provided, the last one is analyzed.',
		dest = 'analysis_name',
		type = str,
		default = None,
	)
	args = parser.parse_args()
	script_core(args.directory, args.analysis_name)

