from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
from progressreporting.TelegramProgressReporter import TelegramProgressReporter
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
from MLE1D import MLE4ACLGAD1D
from scipy import interpolate

def script_core(train_data_dir_path, test_data_dir_path, reconstruction_name=None):
	bureaucrat = Bureaucrat(
		test_data_dir_path,
		variables = locals(),
	)
	
	this_analysis_dir_name = f'{bureaucrat.this_run_timestamp}'
	if reconstruction_name is not None:
		this_analysis_dir_name += f'_{reconstruction_name.replace(" ", "_")}'
	this_analysis_dir_path = bureaucrat.processed_data_dir_path/Path(this_analysis_dir_name)
	this_analysis_dir_path.mkdir(parents=True)
	
	print('Reading data for reconstruction and training MLE regressor...')
	training_observed_df = pandas.read_csv(Path(train_data_dir_path)/Path('pre_process_data_for_MLE_1D')/Path('data for MLE observed variables.csv'))
	training_answers_df = pandas.read_csv(Path(train_data_dir_path)/Path('pre_process_data_for_MLE_1D')/Path('data for MLE answers.csv'))
	regressor = MLE4ACLGAD1D(
		observed_df = training_observed_df,
		answers_df = training_answers_df,
	)
	
	print('Reading test data...')
	test_observed_df = pandas.read_csv(Path(test_data_dir_path)/Path('pre_process_data_for_MLE_1D')/Path('data for MLE observed variables.csv'))
	test_answers_df = pandas.read_csv(Path(test_data_dir_path)/Path('pre_process_data_for_MLE_1D')/Path('data for MLE answers.csv'))
	
	with open(this_analysis_dir_path/Path('metadata.txt'), 'w') as ofile:
		print(f'train_data_dir_path = {train_data_dir_path}', file = ofile)
		print(f'test_data_dir_path = {test_data_dir_path}', file = ofile)
		with pandas.option_context('display.max_columns', None, 'display.width', None):
			print('\nThe training_observed_df data looks like this:', file = ofile)
			print(training_observed_df, file = ofile)
			print('\nThe training_answers_df data looks like this:', file = ofile)
			print(training_answers_df, file = ofile)
			print('\nThe test_observed_df data looks like this:', file = ofile)
			print(test_observed_df, file = ofile)
			print('\nThe test_answers_df data looks like this:', file = ofile)
			print(test_answers_df, file = ofile)
	
	print(f'Starting reconstruction of events...')
	distances = [0]
	for n_pos in sorted(set(test_answers_df['n_pos'])):
		if n_pos == 0:
			# Already have distance[0] = 0
			continue
		x_prev = test_answers_df.loc[test_answers_df['n_pos']==n_pos-1, 'x (m)'].values[0]
		y_prev = test_answers_df.loc[test_answers_df['n_pos']==n_pos-1, 'y (m)'].values[0]
		x = test_answers_df.loc[test_answers_df['n_pos']==n_pos, 'x (m)'].values[0]
		y = test_answers_df.loc[test_answers_df['n_pos']==n_pos, 'y (m)'].values[0]
		ds = ((x-x_prev)**2+(y-y_prev)**2)**.5
		distances.append(distances[-1] + ds)
	print()
	map_n_pos_to_distance = interpolate.interp1d(sorted(set(test_answers_df['n_pos'])), distances)
	with TelegramProgressReporter(len(test_observed_df.index), TelegramReportingInformation().token, TelegramReportingInformation().chat_id, f'MLE position reconstruction analysis using {bureaucrat.measurement_name} (timestamp of this analysis: {bureaucrat.this_run_timestamp})') as reporter:
		with open(this_analysis_dir_path/Path('reconstruction_data.csv'), 'w') as ofile:
			print('n_pos,n_trigger,real_distance (m),reconstructed_distance (m)', file = ofile)
			for index in test_observed_df.index:
				observed = test_observed_df.iloc[index]
				answer = test_answers_df.iloc[index]
				n_pos = test_observed_df.loc[index,'n_pos']
				n_trigger = int(observed['n_trigger'])
				print(f'Reconstructing event n_pos {n_pos}, ntrigger {n_trigger}...')
				if observed['n_pos'] != answer['n_pos'] or observed['n_trigger'] != answer['n_trigger']: # This should never happen, but just in case...
					raise RuntimeError(f'The events in the <test_observed_df> and <test_answers_df> do not coincide, specifically for row {index}.')
				reconstructed_position = regressor.reconstruct_position(observed[[col for col in dict(observed) if col not in {'n_pos','n_trigger'}]])
				real_position = map_n_pos_to_distance(n_pos)
				print(f'{n_pos},{n_trigger},{real_position},{reconstructed_position}', file = ofile)
				reporter.update(1)
	print('Converting output data file to feather format...')
	df = pandas.read_csv(this_analysis_dir_path/Path('reconstruction_data.csv'))
	df.reset_index(drop=True).to_feather(this_analysis_dir_path/Path('reconstruction_data.fd'))
	(this_analysis_dir_path/Path('reconstruction_data.csv')).unlink()
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='')
	parser.add_argument(
		'--dir-with-train-data',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement which has the train data.',
		required = True,
		dest = 'train_data_dir',
		type = str,
	)
	parser.add_argument(
		'--dir-with-test-data',
		metavar = 'path', 
		help = 'Path to a base directory of a measurement which has the test data.',
		required = True,
		dest = 'test_data_dir',
		type = str,
	)
	args = parser.parse_args()
	reconstruction_name = input('Reconstruction analysis name? ')
	script_core(
		train_data_dir_path = args.train_data_dir, 
		test_data_dir_path = args.test_data_dir, 
		reconstruction_name = reconstruction_name if reconstruction_name != '' else None,
	)

