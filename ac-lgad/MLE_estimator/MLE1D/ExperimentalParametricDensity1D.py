import numpy as np
from scipy.stats.kde import gaussian_kde
from scipy import interpolate

class ExperimentalParametricDensity1D:
	def __init__(self, samples: dict):
		"""Q is a random variable whose distribution depends on the continuous
		parameter x. Given a set of samples of Q at discrete points of
		x, this class estimates the distribution of Q at each sampled
		x via KDE estimation and then interpolates (assuming the distribution
		of Q does not change too much from one x to the next one and that 
		it is unimodal) to produce a continuous f(q|x) both in q and x.
		
		samples: A dictionary of the form {x0: [samples at x0], x1: [samples at x1], ...}."""
		self._sampled_xs = np.array(sorted([x for x in samples]))
		self._KDEs = np.array([gaussian_kde(samples[x]) for x in self._sampled_xs])
		self._mean_values = np.array([np.mean(samples[x]) for x in self._sampled_xs])
		self._mean_interpolation = interpolate.interp1d(self._sampled_xs, self._mean_values)
		
	def __call__(self, q, x):
		"""When we evaluate the probability density, we are interested in a
		single value of x and (probably) many values of q. So this is 
		vectorized for q."""
		try:
			x = float(x)
		except:
			raise TypeError(f'<x> must be a number (int or float), received instead {x} of type {type(x)}.')
		if not min(self._sampled_xs) <= x <= max(self._sampled_xs):
			raise ValueError(f'<x> must be bounded within the sampled region. The minimum value for <x> is {min(self._sampled_xs)} and the maximum {max(self._sampled_xs)}, received x = {x}.')
		# If we are here is because x,y are both numbers and within the sampling range.
		i = np.where(self._sampled_xs<=x)[0][-1]
		if i == len(self._sampled_xs)-1: # There is no need to interpolate, and indeed i+1 is out of range.
			return self._KDEs[-1](q)
		µ = self._mean_interpolation(x)
		return (self._KDEs[i](q+self._mean_values[i]-µ)*(self._sampled_xs[i+1]-x)+self._KDEs[i+1](q+self._mean_values[i+1]-µ)*(x-self._sampled_xs[i]))/(self._sampled_xs[i+1]-self._sampled_xs[i])
	
	def likelihood(self, x, q):
		"""Opposed to __call__, here we are evaluating the same function
		f(q|x) but now our variable is x and q is "a single observation",
		meaning that we are interested in evaluating the function for many
		different x values and few q. So the function must be efficiently
		vectorized in x. One non-efficient solution is just
			np.array([self(q,X) for X in x]).ravel()"""
		try:
			q = float(q)
		except:
			raise TypeError(f'<q> must be a number (int or float), received instead {q} of type {type(q)}.')
		if any((x<self._sampled_xs[0])|(x>self._sampled_xs[-1])):
			raise ValueError(f'At least one value in <x> is outside the valid range given by the "sampled x\'s" when creating this instance.')
		i = np.searchsorted(self._sampled_xs, x)-1 # All places where i = -1 mean that x[such_place] = min(self._sampled_xs) so the likelihood is trivial, no need to interpolate anything.
		i_need_interpolation = i>=0
		i_dont_need_interpolation = i<0
		# Pre-calculate each of the quantities we will need ---
		µ = self._mean_interpolation(x[i_need_interpolation]) # As this calculation may be expensive, I only do it where it is needed.
		µi = self._mean_values[i[i_need_interpolation]] # Pre-select the values I will use.
		µim1 = self._mean_values[i[i_need_interpolation]+1] # Pre-select the values I will use.
		fi = np.array([F(q+Ui-U) for F,Ui,U in zip(self._KDEs[i[i_need_interpolation]],µi,µ)]).ravel() # As this calculation may be expensive, I only do it where it is needed.
		fim1 = np.array([F(q+Uim1-U) for F,Uim1,U in zip(self._KDEs[i[i_need_interpolation]+1],µim1,µ)]).ravel() # As this calculation may be expensive, I only do it where it is needed.
		likelihood = np.empty(x.shape)
		likelihood[:] = float('NaN') # Just for safety.
		likelihood[i_need_interpolation] = (fi*(self._sampled_xs[i[i_need_interpolation]+1]-x[i_need_interpolation])+fim1*(x[i_need_interpolation]-self._sampled_xs[i[i_need_interpolation]]))/(self._sampled_xs[i[i_need_interpolation]+1]-self._sampled_xs[i[i_need_interpolation]])
		likelihood[i_dont_need_interpolation] = np.array([self._KDEs[I](q)[0] for I in i[i_dont_need_interpolation]])
		return likelihood
	
	def loglikelihood(self, x, q):
		return np.log(self.likelihood(x,q))
	
if __name__ == '__main__':
	import myplotlib as mpl # https://github.com/SengerM/myplotlib
	
	np.random.seed(10)
	
	SAMPLED_Xs = [0.0,1.0,2.0]
	
	def generate_samples(x):
		N_SAMPLES = 9999
		return np.random.randn(N_SAMPLES)*(x+1)**2 + (x+1)**3 if x <= 1 else np.random.rand(N_SAMPLES)*(x+1)**2+2**3-x**2

	Q_measurements = {x: generate_samples(x) for x in SAMPLED_Xs}
	
	epd = ExperimentalParametricDensity1D(Q_measurements)
	
	fig = mpl.manager.new(
		title = 'Density',
		xlabel = 'Q',
		ylabel = 'Density',
	)
	for x,Qs in Q_measurements.items():
		fig.hist(
			Qs,
			label = f'Q|x={x}',
			density = True,
		)
	q_axis = np.linspace(min([min(Q) for _,Q in Q_measurements.items()]), max([max(Q) for _,Q in Q_measurements.items()]),99)
	for x in sorted(SAMPLED_Xs + [.5,.8,1.2,1.5,1.8]):
		fig.plot(
			q_axis,
			epd(q_axis, x),
			label = f'epd(q,x={x})',
		)
	
	fig = mpl.manager.new(
		title = 'Likelihood',
		xlabel = 'x',
		ylabel = 'Likelihood',
	)
	x_axis = np.linspace(min(SAMPLED_Xs),max(SAMPLED_Xs),99)
	for q in [1,2,3,5,8]:
		fig.plot(
			x_axis,
			epd.likelihood(x_axis, q),
			label = f'q = {q}',
		)
	
	fig = mpl.manager.new(
		title = 'Colormap of distribution',
		xlabel = 'q',
		ylabel = 'x',
	)
	qq,xx = np.meshgrid(q_axis,x_axis)
	fig.colormap(
		x = qq,
		y = xx,
		z = [epd(q=qq[0], x=x[0]) for x in xx],
		colorscalelabel = 'f<sub>Q|x</sub>(q)',
	)
	
	mpl.manager.show()
