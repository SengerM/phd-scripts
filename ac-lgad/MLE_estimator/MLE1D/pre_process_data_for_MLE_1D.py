from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas

CHANNELS = [f'CH{i}' for i in [1,2,3,4]]

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	data = pandas.read_csv(
		bureaucrat.processed_by_script_dir_path('linear_scan_many_triggers_per_point.py')/Path('measured_data.csv'),
		sep = '\t',
	)
	
	# Create data frame for the MLE algorithm ----
	data_for_MLE = {}
	data_for_MLE['observed variables'] = data.loc[data['n_channel']==1,['n_pos', 'n_trigger']].copy().reset_index(drop=True)
	data_for_MLE['answers'] = data.loc[data['n_channel']==1,['n_pos', 'n_trigger', 'x (m)', 'y (m)']].copy().reset_index(drop=True)
	
	# Using charge fraction ----
	# ~ total_charges = sum([np.array(data.loc[data['n_channel']==n_channel, 'Collected charge (a.u.)']) for n_channel in sorted(set(data['n_channel']))])
	for ch in CHANNELS:
		data_for_MLE['observed variables'][f'Collected charge {ch} (a.u.)'] = np.array(data[data['n_channel']==int(ch[-1])]['Collected charge (a.u.)'])
	
	data_for_MLE['observed variables'] = data_for_MLE['observed variables'].dropna()
	
	for key in data_for_MLE:
		data_for_MLE[key].loc[data_for_MLE['observed variables'].index,:].to_csv(bureaucrat.processed_data_dir_path/Path(f'data for MLE {key}.csv'), index=False)
	
	n_poss = sorted(set(data['n_pos']))
	distance = [None]*len(n_poss)
	for n_pos in n_poss:
		if n_pos == 0: 
			distance[n_pos] = 0
			continue
		distance[n_pos] = distance[n_pos-1] + np.linalg.norm(data.loc[data['n_pos']==n_pos,['x (m)', 'y (m)']].iloc[0]-data.loc[data['n_pos']==n_pos-1,['x (m)', 'y (m)']].iloc[0])
	
	for col in data_for_MLE['observed variables']:
		if col in ['n_pos', 'n_trigger', 'x (m)', 'y (m)', 'z (m)', 'n_channel', 'n_pulse']:
			continue
		fig = mpl.manager.new(
			title = f'{col}',
			subtitle = f'Dataset {bureaucrat.measurement_name}',
			xlabel = 'x (m)',
			ylabel = col,
		)
		fig.error_band(
			distance,
			y = data_for_MLE['observed variables'][['n_pos',col]].groupby(['n_pos']).mean()[col],
			ytop = data_for_MLE['observed variables'][['n_pos',col]].groupby(['n_pos']).mean()[col] + data_for_MLE['observed variables'][['n_pos',col]].groupby(['n_pos']).std()[col],
			ylow = data_for_MLE['observed variables'][['n_pos',col]].groupby(['n_pos']).mean()[col] - data_for_MLE['observed variables'][['n_pos',col]].groupby(['n_pos']).std()[col],
		)
		mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path/Path('observed variables plots'.replace(' ','_')))
		
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Takes the measured data (e.g. collected charge) and produces the data to feed into the MLE position reconstructor (e.g. charge fraction) either for training or reconstructing.')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory)

