from ExperimentalParametricDensity1D import ExperimentalParametricDensity1D
import numpy
import myplotlib as mpl
import lmfit
import numpy as np
from scipy import interpolate
import sys
import pandas

class MLE4ACLGAD1D:
	def __init__(self, observed_df, answers_df):
		"""The training process is done at here.
		observed_df: DataFrame with columns 'n_pos', 'n_trigger', and the rest of the columns are the measured variables. Each row is an event.
		answers_df: DataFrame with columns 'n_pos', 'n_trigger', 'x (m)' and 'y (m)'. Each row is an event and match the observed_df."""
		self._n_positions_trained = sorted(set(answers_df['n_pos']))
		self._x_positions_trained = [answers_df.loc[answers_df['n_pos']==n,'x (m)'].values[0] for n in self._n_positions_trained]
		self._y_positions_trained = [answers_df.loc[answers_df['n_pos']==n,'y (m)'].values[0] for n in self._n_positions_trained]
		self._map_n_to_x = interpolate.interp1d(self._n_positions_trained, self._x_positions_trained)
		self._map_n_to_y = interpolate.interp1d(self._n_positions_trained, self._y_positions_trained)
		self.distance = [0]
		for i,(x,y) in enumerate(zip(self._x_positions_trained, self._y_positions_trained)):
			if i == 0:
				# Already have distance[0] = 0
				continue
			x_prev = self._x_positions_trained[i-1]
			y_prev = self._y_positions_trained[i-1]
			ds = ((x-x_prev)**2+(y-y_prev)**2)**.5
			self.distance.append(self.distance[-1] + ds)
		self._map_n_to_distance = interpolate.interp1d(self._n_positions_trained, self.distance)
		self._map_distance_to_n = interpolate.interp1d(self.distance, self._n_positions_trained) # distance is monotonically increasing so this will always work
		self._observed_variables = [col for col in observed_df if col not in {'n_pos', 'n_trigger'}]
		self._observed_variables_set = frozenset(self._observed_variables)
		# For each "observed variable" I create one EPD1D object ---
		self._EPDs = {}
		for variable in self._observed_variables:
			self._EPDs[variable] = ExperimentalParametricDensity1D(
				samples = {n: observed_df.loc[observed_df['n_pos']==n,variable].values for n in self._n_positions_trained}
			)
		# Training is complete.
	
	def loglikelihood(self, distance, observed_event):
		"""Calculate the likelihood function.
		observed_event: dict or pandas.Series with the variables observed for the event.
		distance: An array of distances on which to calculate the likelihood function for this observed_event."""
		if isinstance(observed_event, pandas.Series):
			observed = observed_event.to_dict()
		elif isinstance(observed_event, dict):
			observed = dict(observed_event)
		else:
			raise TypeError(f'<observed_event> must be either a dict or a pandas.Series object. Received an object of type {type(observed_event)}.')
		if isinstance(distance, float) or isinstance(distance, int):
			distance = np.array([distance])
		if set(observed) != self._observed_variables_set:
			raise ValueError(f'You want to calculate the likelihood of an event using an <observed_event> with observed variables {set(observed)}, however this reconstructor was trained using variables {self._observed_variables_set}.')
		# If we are here we know that the variables names in <observed_event> match what we expect.
		value = np.sum(np.array([self._EPDs[var].loglikelihood(x = self._map_distance_to_n(distance), q = observed[var]) for var in self._observed_variables_set]), axis=0)
		return value
	
	def reconstruct_position(self, observed_variables):
		"""Given an event, the most likely distance for such event is calculated.
		observed_event: dict or pandas.Series with the variables observed for the event."""
		params = lmfit.Parameters()
		params.add('distance', value = numpy.mean(self.distance), min = min(self.distance), max = max(self.distance))
		def func2minimize(pars):
			parvals = pars.valuesdict()
			values = self.loglikelihood(distance=parvals['distance'],observed_event=observed_variables)
			values[values==float('-inf')] = np.log(sys.float_info.min) # This is because the algorithms cannot handle NaN values (and infinities).
			return -values
		# Because the likelihood has such a strange shape, first I run a "brute" method and then I use the result of this method as input for a more precise method for soft functions.
		minimizer_result = lmfit.minimize(
			fcn = func2minimize,
			params = params,
			method = 'brute',
		)
		minimizer_result = lmfit.minimize(
			fcn = func2minimize,
			params = minimizer_result.params,
			method = 'differential_evolution',
		)
		return minimizer_result.params['distance'].value

if __name__ == '__main__':
	import pandas
	import myplotlib as mpl
	
	observed_df = pandas.read_csv('/home/alf/cernbox/measurements_data/AC-LGAD/20210624121947_BigFoot_Linear_Training_199V_1MIP_11um_3333/pre_process_data_for_MLE_1D_scan/data for MLE observed variables.csv')
	answers_df = pandas.read_csv('/home/alf/cernbox/measurements_data/AC-LGAD/20210624121947_BigFoot_Linear_Training_199V_1MIP_11um_3333/pre_process_data_for_MLE_1D_scan/data for MLE answers.csv')
	
	# ~ observed_df.drop([f'Collected charge CH{i} (a.u.)' for i in [2,3]], axis=1, inplace=True)
	
	regressor = MLE4ACLGAD1D(observed_df, answers_df)
	fig = mpl.manager.new(
		title = 'Likelihood example',
		xlabel = 'Distance (m)',
		ylabel = 'Log likelihood',
	)
	x_axis = np.linspace(min(regressor.distance),max(regressor.distance),99)
	N_TRIGGER = 59
	for n in [9,10,16,20]:
		loglikelihood = regressor.loglikelihood(x_axis, observed_df.loc[(observed_df['n_pos']==n)&(observed_df['n_trigger']==N_TRIGGER),[col for col in observed_df if col not in {'n_pos','n_trigger'}]].squeeze())
		fig.plot(
			x_axis,
			loglikelihood,
			label = f'n={n}, distance={regressor.distance[n]*1e6:.0f} µm',
		)
		reconstruction = regressor.reconstruct_position(observed_df.loc[(observed_df['n_pos']==n)&(observed_df['n_trigger']==N_TRIGGER),[col for col in observed_df if col not in {'n_pos','n_trigger'}]].squeeze())
		fig.plot(
			[reconstruction],
			regressor.loglikelihood(reconstruction, observed_df.loc[(observed_df['n_pos']==n)&(observed_df['n_trigger']==N_TRIGGER),[col for col in observed_df if col not in {'n_pos','n_trigger'}]].squeeze()),
			marker = '.',
			label = f'n={n}, reconstruction={reconstruction*1e6:.0f} µm',
		)
		print(f"Real = {regressor.distance[n]:.2e} m | Prediction = {reconstruction:.2e} m | Error = {regressor.distance[n]-reconstruction:.1e} m")
	mpl.manager.show()
