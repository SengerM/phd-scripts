import pandas
from data_processing_bureaucrat.Bureaucrat import Bureaucrat
from pathlib import Path
from dnn_training_script import script_core as dnn_training
from dnn_position_reconstruction import script_core as dnn_position_reconstruction
from dnn_position_reconstruction_analysis_and_plots import script_core as analysis_script
import myplotlib as mpl

DEFAULT_N_EPOCHS = 1
DEFAULT_N_HIDDEN_LAYERS = 1
DEFAULT_N_NODES_PER_HIDDEN_LAYER = 5
DEFAULT_BATCH_SIZE = 111

DEFAULT_VALUES = {
	'n_epochs': DEFAULT_N_EPOCHS,
	'n_hidden_layers': DEFAULT_N_HIDDEN_LAYERS,
	'n_nodes_per_hidden_layer': DEFAULT_N_NODES_PER_HIDDEN_LAYER,
	'batch_size': DEFAULT_BATCH_SIZE,
}

def script_core(training_dir, testing_dir, parameters_to_test):
	"""
	parameters_to_test: Iterable of dictionaries of the form {'n_epochs':int, 'n_hidden_layers':int, 'n_nodes_per_hidden_layer':int, 'batch_size':int}. Missing parameters are assigned default values (see source code).
	"""
	bureaucrat = Bureaucrat(
		testing_dir,
		variables = locals(),
	)
	
	data_df = pandas.DataFrame(parameters_to_test)
	for column in {'n_epochs','n_hidden_layers','n_nodes_per_hidden_layer','batch_size','dnn_regressor_path','reconstruction_results_path','reconstruction_analysis_path'}:
		if column not in data_df:
			data_df[column] = None
	for idx,net_params in enumerate(parameters_to_test):
		for column in {'n_epochs','n_hidden_layers','n_nodes_per_hidden_layer','batch_size'}:
			if data_df.loc[idx,column] is None:
				data_df.loc[idx,column] = DEFAULT_VALUES[column]
		print(f'Training net {idx+1}/{len(parameters_to_test)}...')
		dnn_training(
			directory = training_dir, 
			n_epochs = data_df.loc[idx,'n_epochs'],
			n_hidden_layers = data_df.loc[idx,'n_hidden_layers'],
			n_nodes_per_hidden_layer = data_df.loc[idx,'n_nodes_per_hidden_layer'],
			batch_size = data_df.loc[idx,'batch_size'],
		)
		print(f'Testing net {idx+1}/{len(parameters_to_test)}...')
		data_df.loc[idx,'dnn_regressor_path'] = sorted((Path(training_dir)/Path(bureaucrat.processed_by_script_dir_path('dnn_training_script.py').parts[-1])/Path('DNNs')).iterdir())[-1]
		dnn_position_reconstruction(
			regressor_path = data_df.loc[idx,'dnn_regressor_path'],
			dataset_path = Path(testing_dir), 
			reconstruction_name = f'DNN-{data_df.loc[idx,"dnn_regressor_path"].parts[-1].replace(".pkl","")}',
		)
		data_df.loc[idx,'reconstruction_results_path'] = sorted((Path(testing_dir)/Path(bureaucrat.processed_by_script_dir_path('dnn_position_reconstruction.py').parts[-1])).iterdir())[-2]
		analysis_script(
			dir_with_measurement = testing_dir,
			analysis_name = data_df.loc[idx,'reconstruction_results_path'].parts[-1],
		)
		data_df.loc[idx,'reconstruction_analysis_path'] = sorted((Path(testing_dir)/Path(bureaucrat.processed_by_script_dir_path('dnn_position_reconstruction_analysis_and_plots.py').parts[-1])).iterdir())[-2]
		data_df.to_csv(bureaucrat.processed_data_dir_path/Path('explored_DNNs_list.csv'), index=False)
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Creates, trains and processes-data-with many DNNs with different parameters in order to see how this affects the reconstruction precision.')
	parser.add_argument(
		'--training-dir',
		metavar = 'path',
		help = 'Path to the base directory of a measurement which will be used to train the DNNs.',
		required = True,
		dest = 'training_dir',
		type = str,
	)
	parser.add_argument(
		'--testing-dir',
		metavar = 'path',
		help = 'Path to the base directory of a measurement which will be used to test the trained DNNs and evaluate the results.',
		required = True,
		dest = 'testing_dir',
		type = str,
	)
	args = parser.parse_args()
	script_core(
		testing_dir = args.testing_dir, 
		training_dir = args.training_dir,
		parameters_to_test = [{'n_hidden_layers':1, 'n_nodes_per_hidden_layer':nodes, 'n_epochs':2} for nodes in [2,3,4,5,6,7,8,9,10,22,33,44,55,66,77,88]],
	)
