from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
import pickle

def script_core(regressor_path, dataset_path, reconstruction_name, silent=True, output_format='feather'):
	OUTPUT_FORMATS = {'csv','feather'}
	if output_format not in OUTPUT_FORMATS:
		raise ValueError(f'<output_format> must be one of {OUTPUT_FORMATS}, received {output_format}.')
	bureaucrat = Bureaucrat(
		dataset_path,
		variables = locals(),
	)
	
	this_analysis_dir_path = bureaucrat.processed_data_dir_path/Path(f'{bureaucrat.this_run_timestamp}_reconstruction_{reconstruction_name.replace(" ", "_")}')
	this_analysis_dir_path.mkdir(parents=True)
	
	with open(this_analysis_dir_path/Path('metadata.txt'), 'w') as ofile:
		print(f'Regressor used in this reconstruction: {regressor_path}', file=ofile)
	
	if not silent:
		print('Reading data for reconstruction...')
	observed_df = pandas.read_feather(Path(dataset_path)/bureaucrat.processed_by_script_dir_path('pre_process_data_for_dnn.py')/Path('observed.fd'))
	answers_df  = pandas.read_feather(Path(dataset_path)/bureaucrat.processed_by_script_dir_path('pre_process_data_for_dnn.py')/Path('answers.fd'))
	
	if not silent:
		print(f'Loading DNN regressor...')
	with open(regressor_path, 'rb') as ifile:
		regressor = pickle.load(ifile)
	
	if not silent:
		print(f'Starting reconstruction of events...')
	
	reconstructed_df = regressor.reconstruct_events(observed_df.drop(['n_x','n_y','n_trigger'],1))
	results_df = answers_df[['n_x','n_y','n_trigger']]
	for xy in ['x','y']:
		results_df[f'real_{xy} (m)'] = answers_df[f'{xy} (m)']
		results_df[f'reconstructed_{xy} (m)'] = reconstructed_df[f'{xy} (m)']
	if output_format == 'csv':
		results_df.to_csv(this_analysis_dir_path/Path('reconstruction_data.csv'), index=False)
	elif output_format == 'feather':
		results_df.reset_index(drop=True).to_feather(this_analysis_dir_path/Path('reconstruction_data.fd'))
	else:
		raise RuntimeError("Don't know in which format to save the data.")
	
	if not silent:
		print(f'Data successfully processed, results saved to {this_analysis_dir_path/Path("reconstruction_data.csv")}')
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Uses a previously trained DNN to process and analyze the data from a measurement.')
	parser.add_argument(
		'--regressor',
		metavar = 'path', 
		help = 'Path to a "ACLGADDNNRegressor.pkl" file produced by the script "dnn_training_script.py".',
		required = True,
		dest = 'dnn_path',
		type = str,
	)
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to a directory containing the measurement to analyze.',
		required = True,
		dest = 'dataset_path',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.dnn_path, args.dataset_path, input('Reconstruction analysis name? '), silent=False)

