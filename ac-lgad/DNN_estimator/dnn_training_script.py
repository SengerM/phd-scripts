from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
from pathlib import Path
import myplotlib as mpl
import pandas
from DNN4ACLGAD import ACLGADDNNRegressor, ParameterizedFeedForwardNeuralNetwork
import pickle
import datetime

def script_core(directory, n_epochs:int=1, n_hidden_layers:int=2, n_nodes_per_hidden_layer:int=22, batch_size:int=111, silent=True):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	if not silent:
		print('Reading training data...')
	observed_df = pandas.read_feather(bureaucrat.processed_by_script_dir_path('pre_process_data_for_dnn.py')/Path('observed.fd'))
	answers_df = pandas.read_feather(bureaucrat.processed_by_script_dir_path('pre_process_data_for_dnn.py')/Path('answers.fd'))
	
	n_inputs = len(set(observed_df.columns)-{'n_x','n_y','n_trigger'})
	n_outputs = len(set(answers_df.columns)-{'n_x','n_y','n_trigger'})
	dnn = ParameterizedFeedForwardNeuralNetwork(
		n_inputs = n_inputs,
		n_outputs = n_outputs,
		n_hidden_layers = int(n_hidden_layers), # Choose yourself.
		n_nodes_per_hidden_layer = int(n_nodes_per_hidden_layer), # Choose yourself.
	)
	regressor = ACLGADDNNRegressor(dnn)
	
	if not silent:
		print(f'Training...')
	regressor.train(
		observed_df = observed_df.drop(['n_x','n_y','n_trigger'],1),
		target_df = answers_df.drop(['n_x','n_y','n_trigger'],1),
		epochs = n_epochs,
		batch_size = batch_size,
	)
	if not silent:
		print(f'Saving trained model...')
	path_to_DNNs_folder = bureaucrat.processed_data_dir_path/Path('DNNs')
	path_to_DNNs_folder.mkdir(parents=True, exist_ok=True)
	timestamp = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
	this_dnn_file_path = path_to_DNNs_folder/Path(f'{timestamp}_{n_inputs}_{n_outputs}_{n_hidden_layers}_{n_nodes_per_hidden_layer}_ACLGADDNNRegressor.pkl')
	with open(this_dnn_file_path, 'wb') as ofile:
		pickle.dump(regressor, ofile)
	if not silent:
		print(f'The DNN regressor was succesfully trained and saved in {this_dnn_file_path}')
	
if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Creates and trains a DNN position reconstructor from a data set produced by the script "pre_process_data_for_MLE.py".')
	parser.add_argument(
		'--dir',
		metavar = 'path', 
		help = 'Path to the base directory of a measurement.',
		required = True,
		dest = 'directory',
		type = str,
	)
	args = parser.parse_args()
	script_core(args.directory, silent=False)

