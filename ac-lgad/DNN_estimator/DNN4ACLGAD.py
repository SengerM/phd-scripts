import torch
import numpy as np
import pandas

class LinearElementWiseTransformation:
	def __init__(self, std, mean):
		self.std = std
		self.mean = mean
	
	def __call__(self, x):
		return (x-self.mean)/self.std
	
	def __repr__(self):
		return f'y=(x-{self.mean})/{self.std}'
	
	def inverse(self, y):
		return y*self.std+self.mean

class ParameterizedFeedForwardNeuralNetwork(torch.nn.Module):
	def __init__(self, n_inputs: int, n_outputs: int, n_hidden_layers: int, n_nodes_per_hidden_layer: int):
		self.n_inputs = int(n_inputs) # Store for future reference.
		self.n_outputs = int(n_outputs) # Store for future reference.
		self.n_hidden_layers = int(n_hidden_layers) # Store for future reference.
		self.n_nodes_per_hidden_layer = int(n_nodes_per_hidden_layer) # Store for future reference.
		if any([i<=0 for i in [n_inputs,n_outputs,n_hidden_layers,n_nodes_per_hidden_layer]]):
			raise ValueError(f'All n_inputs, n_outputs, n_hidden_layers and n_nodes_per_hidden_layer must be greater than 0.')
		super().__init__()
		self.input_layer = torch.nn.Linear(n_inputs, n_nodes_per_hidden_layer)
		self.hidden_layers = [torch.nn.Linear(n_nodes_per_hidden_layer, n_nodes_per_hidden_layer) for i in range(n_hidden_layers)]
		self.output_layer = torch.nn.Linear(n_nodes_per_hidden_layer, n_outputs)

	def forward(self, x):
		activation_function = torch.nn.functional.relu
		x = activation_function(self.input_layer(x))
		for idx,layer in enumerate(self.hidden_layers):
			x = activation_function(layer(x))
		x = self.output_layer(x)
		return x

class ACLGADDataset(torch.utils.data.Dataset):
	# https://pytorch.org/tutorials/beginner/basics/data_tutorial.html#creating-a-custom-dataset-for-your-files
	def __init__(self, data_df, transform=None, target_transform=None):
		self._data_df = data_df
		self.transform = transform
		self.target_transform = target_transform
	
	def __len__(self):
		return len(self._data_df.index)
	
	def __getitem__(self, idx):
		observed = torch.tensor(list(self._data_df.loc[idx,'observed']))
		answers = torch.tensor(list(self._data_df.loc[idx,'answers']))
		if self.transform is not None:
			observed = self.transform(observed)
		if self.target_transform is not None:
			answers = self.target_transform(answers)
		return observed, answers

def train_loop(nnetwork, dataset: ACLGADDataset, epochs: int = 1, batch_size: int = 111, num_workers: int = 8, verbose: bool = False):
	if not isinstance(dataset, ACLGADDataset):
		raise TypeError(f'<dataset> must be an instance of ACLGADDataset.')
	dataloader = torch.utils.data.DataLoader(
		dataset, 
		batch_size = batch_size, 
		shuffle = True,
		num_workers = num_workers,
	) # https://pytorch.org/tutorials/beginner/basics/data_tutorial.html#preparing-your-data-for-training-with-dataloaders
	optimizer = torch.optim.Adam(nnetwork.parameters())
	loss_fn = torch.nn.MSELoss()
	for epoch in range(epochs):
		for observed_tensor,answers_tensor in dataloader:
			predicted_tensor = nnetwork(observed_tensor)
			loss = loss_fn(predicted_tensor, answers_tensor)
			optimizer.zero_grad()
			loss.backward()
			optimizer.step()
			if verbose:
				with torch.no_grad():
					# Perform a reconstruction of a random event and print the error.
					test_obs, test_ans = dataset[np.random.randint(len(dataset))]
					reconstructed = nnetwork(test_obs)
					if dataset.target_transform is not None:
						test_ans = dataset.target_transform.inverse(test_ans)
						reconstructed = dataset.target_transform.inverse(reconstructed)
					error_in_meters = sum((test_ans - reconstructed)**2)**.5
					print(f'Epoch {epoch} | Error = {error_in_meters:.2e} m | Reconstructed: {[f"{f.item():.3e}" for f in reconstructed]}')
	return nnetwork

class ACLGADDNNRegressor:
	def __init__(self, dnn: torch.nn.Module):
		if not isinstance(dnn, torch.nn.Module):
			raise TypeError(f'<dnn> must be an instance of "torch.nn.Module". Received type is {type(dnn)}.')
		self._dnn = dnn
	
	def train(self, observed_df, target_df, **kwargs):
		"""Trains the DNN for later use.
		
		Parameters
		----------
			observed_df: A Pandas dataframe whose columns contain the
			             raw inputs.
		
			target_df: A Pandas dataframe whose columns contain the 
			           answers.
		
		Each row in `observed_df` must match each row in `target_df`.
		
		This method can only be called once for each instance. Information
		about the training process is stored in self.training_metadata.
		"""
		if hasattr(self, 'training_metadata'):
			raise RuntimeError('This regressor has already been trained.')
		self.training_metadata = kwargs
		self._dnn.train() # Put it in train mode.
		# Store what are the observed values so later on I can check.
		self._observed_variables_names = observed_df.columns
		self._target_variables_names = target_df.columns
		# Find linear transformations that "normalize" inputs and outputs. (I store the transformations so I can use them later on when this object is in "production mode".)
		self._transform = LinearElementWiseTransformation(
			mean = torch.tensor([observed_df[col].mean() for col in observed_df]),
			std = torch.tensor([observed_df[col].std() for col in observed_df]),
		)
		self._target_transform = LinearElementWiseTransformation(
			mean = torch.tensor([target_df[col].mean() for col in target_df]),
			std = torch.tensor([target_df[col].std() for col in target_df]),
		)
		# Prepare training data matching observations with answers ---
		data_df = pandas.DataFrame()
		data_df['observed'] = observed_df[[col for col in observed_df]].values.tolist() # Each row contains a list with all the 'observed values'.
		data_df['answers'] = target_df[[col for col in target_df]].values.tolist() # Each row contains a list with all the 'answers'.
		training_dataset = ACLGADDataset(
			data_df,
			transform = self._transform,
			target_transform = self._target_transform,
		)
		self._dnn = train_loop(self._dnn, training_dataset, **kwargs)
	
	def reconstruct_events(self, observed_df, batch_size: int = 111, num_workers: int = 8, verbose: bool = False):
		"""Reconstructs a bunch of events, it is way faster than <reconstruct_single_event>.
		observed_df: A Pandas dataframe containing in its columns the observed variables, each row is an event.
		"""
		if set(observed_df.columns) != set(self._observed_variables_names):
			raise RuntimeError(f'This regressor was trained using the followin input variables {list(self._observed_variables_names)} and now you want to reconstruct an event using variables {list(observed_df.columns)}. You must use the same variables.')
		self._dnn.eval() # Put the model in "no train mode".
		# Create a "pytorch dataset" ---
		data_df = pandas.DataFrame()
		data_df['observed'] = observed_df[[col for col in observed_df]].values.tolist() # Each row contains a list with all the 'observed values'.
		data_df['answers'] = [[0]*len(self._target_variables_names)]*len(observed_df.index) # Create dumy answers as we don't know them, and we don't care.
		test_dataset = ACLGADDataset(
			data_df,
			transform = self._transform,
		)
		test_dataloader = torch.utils.data.DataLoader(
			test_dataset, 
			batch_size = batch_size, 
			num_workers = num_workers,
		)
		reconstructed = torch.zeros((len(observed_df.index),len(self._target_variables_names))) # Prealocate in an attempt to go faster.
		with torch.no_grad():
			for idx,(data,_) in enumerate(test_dataloader):
				reconstructed[idx*batch_size:(idx+1)*batch_size] = self._dnn(data)
		if self._target_transform is not None:
			reconstructed = self._target_transform.inverse(reconstructed)
		return pandas.DataFrame(reconstructed.numpy(), columns=self._target_variables_names)
