from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramProgressBar
import numpy as np
import myplotlib as mpl
from lgadtools.LGADSignal import LGADSignal
from pathlib import Path
import pandas
from scipy.stats import norm
from scipy.stats import moyal

COLUMNS_TO_PLOT = [
	'Amplitude (V)',
	'Noise (V)',
	'Rise time (s)',
	'Collected charge (a.u.)',
	'Time over 20 % threshold (s)',
]

def script_core(directory, path_tct, path_beta):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	data_tct = pandas.read_csv(
		Path(path_tct),
		sep = '\t',
	)
	
	data_beta = pandas.read_csv(
		Path(path_beta),
		sep = '\t',
	)
	
	laser_pulse_widths_to_plot = input(f'Which laser pulse widths to plot? (Enter numbers separated by ", ")\nAvailable options: {sorted(set(data_tct["Laser pulse width (%)"]))}\nYour answer: ').split(', ')
	laser_pulse_widths_to_plot = [float(qwe) for qwe in laser_pulse_widths_to_plot]
	
	fig = mpl.manager.new(
		title = 'Collected charge beta source vs TCT',
		xlabel = 'Collected charge (a.u.)',
		ylabel = 'Density of events',
	)
	fig.hist(
		data_beta['Collected charge (a.u.)'],
		label = 'Beta source',
		density = True,
		color = (0,0,0),
	)
	loc, scale = moyal.fit(data_beta['Collected charge (a.u.)'])
	q_axis = np.linspace(
			data_beta['Collected charge (a.u.)'].min(), 
			data_beta['Collected charge (a.u.)'].max(), 
			99,
		)
	fig.plot(
		q_axis,
		moyal.pdf(q_axis, loc, scale),
		label = 'Fit beta source',
		color = (0,0,0),
	)
	for pw in sorted(laser_pulse_widths_to_plot):
		color = np.random.rand(3)
		fig.hist(
			data_tct[(data_tct['Laser pulse width (%)']==pw)]['Collected charge (a.u.)'],
			label = f'TCT, LPW = {pw} %',
			density = True,
			bins = 99,
			color = tuple(color),
		)
		loc, scale = norm.fit(data_tct[(data_tct['Laser pulse width (%)']==pw)]['Collected charge (a.u.)'])
		q_axis = np.linspace(
			data_tct[(data_tct['Laser pulse width (%)']==pw)]['Collected charge (a.u.)'].min(), 
			data_tct[(data_tct['Laser pulse width (%)']==pw)]['Collected charge (a.u.)'].max(), 
			99,
		)
		fig.plot(
			q_axis,
			norm.pdf(q_axis, loc, scale,),	
			label = f'Fit, LPW = {pw} %',
			linestyle = '--',
			color = tuple(color),
		)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
	

########################################################################

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description='Plot data from TCT and beta source together.')
	parser.add_argument('--data-TCT',
		metavar = 'path', 
		help = 'Path to the base measurement directory containing the data from the TCT laser pulse width scan.',
		required = True,
		dest = 'path_tct',
	)
	parser.add_argument('--data-beta',
		metavar = 'path', 
		help = 'Path to the base measurement directory containing the data from the beta source.',
		required = True,
		dest = 'path_beta',
	)
	parser.add_argument('--dir',
		metavar = 'path', 
		help = 'Path to the base measurement directory. The raw data must be in a sub directory called "raw".',
		required = True,
		dest = 'path',
	)

	args = parser.parse_args()
	script_core(
		path_tct = args.path_tct,
		path_beta = args.path_beta,
		directory = args.path,
	)
