from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramProgressBar
import numpy as np
import myplotlib as mpl
from lgadtools.LGADSignal import LGADSignal
from pathlib import Path
import pandas
from scipy.stats import norm

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	data = 	pandas.read_csv(
		bureaucrat.processed_by_script_dir_path('laser_pulse_width_scan.py')/Path('measured_data.csv'),
		sep = '\t',
	)
	
	n_laser_pulse_widths = sorted(list(set(data['n_pulse_width'])))
	
	µs = []
	σs = []
	fig = mpl.manager.new(
		title = f'Collected charge in the TCT',
		subtitle = f'Measurement: {bureaucrat.measurement_name}',
		xlabel = 'Collected charge (a.u)',
		ylabel = 'Density of events',
	)
	for npw in n_laser_pulse_widths:
		µ, σ = norm.fit(data[(data['n_pulse_width']==npw)]['Collected charge (a.u.)'])
		µs.append(µ)
		σs.append(σ)
		color = np.random.rand(3)
		fig.hist(
			data[(data['n_pulse_width']==npw)]['Collected charge (a.u.)'],
			density = True,
			label = f"Measured at LPW={list(data[(data['n_pulse_width']==npw)]['Laser pulse width (%)'])[0]} %",
			color = tuple(color),
		)
		q_axis = np.linspace(
			data[(data['n_pulse_width']==npw)]['Collected charge (a.u.)'].min(), 
			data[(data['n_pulse_width']==npw)]['Collected charge (a.u.)'].max(), 
			99,
		)
		fig.plot(
			q_axis,
			norm.pdf(q_axis, µ, σ),	
			label = f"Fit for LPW={list(data[(data['n_pulse_width']==npw)]['Laser pulse width (%)'])[0]} %",
			linestyle = '--',
			color = tuple(color),
		)
	µs = np.array(µs)
	σs = np.array(σs)
	
	fig = mpl.manager.new(
		title = f'Fitted parameters',
		subtitle = f'Measurement: {bureaucrat.measurement_name}',
		xlabel = f'Laser pulse width (%)',
		ylabel = f'Collected charge (a.u.)',
	)
	fig.plot(
		[list(data[(data['n_pulse_width']==n)]['Laser pulse width (%)'])[0] for n in n_laser_pulse_widths],
		µs,
		label = f'Fitted µ',
		marker = '.',
	)
	fig.plot(
		[list(data[(data['n_pulse_width']==n)]['Laser pulse width (%)'])[0] for n in n_laser_pulse_widths],
		σs,
		label = f'Fitted σ',
		marker = '.',
	)
	fig = mpl.manager.new(
		title = f'Fitted parameters ratio',
		subtitle = f'Measurement: {bureaucrat.measurement_name}',
		xlabel = f'Laser pulse width (%)',
		ylabel = f'1/Collected charge (a.u.)',
	)
	fig.plot(
		[list(data[(data['n_pulse_width']==n)]['Laser pulse width (%)'])[0] for n in n_laser_pulse_widths],
		µs/σs**2,
		label = f'µ/σ²',
		marker = '.',
	)
	
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
	# ~ mpl.manager.show()

########################################################################

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description='Parse a bunch of signals from a single detector.')
	parser.add_argument('--dir',
		metavar = 'path', 
		help = 'Path to the base measurement directory. The raw data must be in a sub directory called "raw".',
		required = True,
		dest = 'path',
	)

	args = parser.parse_args()
	script_core(
		directory = args.path,
	)
