from data_processing_bureaucrat.Bureaucrat import Bureaucrat
import numpy as np
import myplotlib as mpl
from lgadtools.LGADSignal import LGADSignal
from pathlib import Path
import pandas

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	data = 	pandas.read_csv(
		bureaucrat.processed_by_script_dir_path('parse_single_detector_raw_data.py')/Path('parsed_data.csv'),
		sep = '\t',
	)
	for column in data:
		if column == 'n_signal': continue
		fig = mpl.manager.new(
			title = column[:column.find('(')],
			subtitle = f'Data set: {bureaucrat.measurement_name}',
			xlabel = column,
		)
		if column == 'Collected charge (a.u.)':
			fig.set(
				ylabel = 'Density of events',
			)
			fig.hist(
				data[column],
				label = 'Measured distribution',
				density = True,
			)
		else:
			fig.hist(
				data[column],
			)
			fig.set(
				ylabel = 'Number of events',
			)
	fig = mpl.manager.new(
		title = 'Signal to noise ratio',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'SNR',
		ylabel = 'Number of events',
	)
	fig.hist(
		data['Amplitude (V)']/data['Noise (V)'],
	)
	fig = mpl.manager.new(
		title = 'Amplitude and noise',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = 'Voltage (V)',
		ylabel = 'Number of events',
		xscale = 'log',
	)
	fig.hist(
		data['Amplitude (V)'],
		label = 'Amplitude',
	)
	fig.hist(
		data['Noise (V)'],
		label = 'Noise',
	)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)

########################################################################

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description='Parse a bunch of signals from a single detector.')
	parser.add_argument('--dir',
		metavar = 'path', 
		help = 'Path to the base measurement directory. The raw data must be in a sub directory called "raw".',
		required = True,
		dest = 'path',
	)

	args = parser.parse_args()
	script_core(
		directory = args.path,
	)
