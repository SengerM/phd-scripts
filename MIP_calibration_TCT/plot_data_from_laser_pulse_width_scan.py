from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramProgressBar
import numpy as np
import myplotlib as mpl
from lgadtools.LGADSignal import LGADSignal
from pathlib import Path
import pandas

COLUMNS_TO_PLOT = [
	'Amplitude (V)',
	'Noise (V)',
	'Rise time (s)',
	'Collected charge (a.u.)',
	'Time over 20 % threshold (s)',
]

def script_core(directory):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	data = 	pandas.read_csv(
		bureaucrat.processed_by_script_dir_path('laser_pulse_width_scan.py')/Path('measured_data.csv'),
		sep = '\t',
	)
	
	for column in COLUMNS_TO_PLOT:
		fig = mpl.manager.new(
			title = column[:column.find('(')],
			subtitle = f'Data set: {bureaucrat.measurement_name}',
			xlabel = column,
			ylabel = 'Number of events',
		)
		for n_pulse_width in sorted(list(set(data['n_pulse_width']))):
			fig.hist(
				data[data['n_pulse_width']==n_pulse_width][column],
				label = f"Pulse width = {list(data[data['n_pulse_width']==n_pulse_width]['Laser pulse width (%)'])[0]}",
			)
		mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)
	
	fig = mpl.manager.new(
		title = f'Signal to noise ratio',
		subtitle = f'Data set: {bureaucrat.measurement_name}',
		xlabel = f'SNR',
		ylabel = 'Number of events',
	)
	for n_pulse_width in sorted(list(set(data['n_pulse_width']))):
		snr = data[data['n_pulse_width']==n_pulse_width]['Amplitude (V)']/data[data['n_pulse_width']==n_pulse_width]['Noise (V)']
		snr = snr[snr<99e9]
		fig.hist(
			snr,
			label = f"Pulse width = {list(data[data['n_pulse_width']==n_pulse_width]['Laser pulse width (%)'])[0]}",
		)
	mpl.manager.save_all(mkdir = bureaucrat.processed_data_dir_path)

########################################################################

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description='Parse a bunch of signals from a single detector.')
	parser.add_argument('--dir',
		metavar = 'path', 
		help = 'Path to the base measurement directory. The raw data must be in a sub directory called "raw".',
		required = True,
		dest = 'path',
	)

	args = parser.parse_args()
	script_core(
		directory = args.path,
	)
