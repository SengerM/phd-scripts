from data_processing_bureaucrat.Bureaucrat import Bureaucrat, TelegramReportingInformation
from progressreporting.TelegramProgressReporter import TelegramProgressReporter
import numpy as np
import pandas
import myplotlib as mpl
from lgadtools.LGADSignal import LGADSignal
from pathlib import Path

def script_core(directory, invert=False):
	bureaucrat = Bureaucrat(
		directory,
		variables = locals(),
	)
	
	ofilepath = bureaucrat.processed_data_dir_path/Path(f'parsed_data.csv')
	
	raw_files_directory = bureaucrat.measurement_base_path/Path('raw')
	
	n_signals_to_parse = len(list(raw_files_directory.iterdir()))
	with open(ofilepath, 'w') as ofile:
		print(f'n_signal\tAmplitude (V)\tNoise (V)\tRise time (s)\tCollected charge (a.u.)\tTime over noise (s)', file = ofile)
		with TelegramProgressReporter(n_signals_to_parse, TelegramReportingInformation().token, TelegramReportingInformation().chat_id, f'parse_single_detector_raw_data.py processing {bureaucrat.measurement_name} (timestamp of this analysis: {bureaucrat.this_run_timestamp})') as reporter:
			for ifpath in sorted(raw_files_directory.iterdir()):
				reporter.update(1)
				current_signal_number = int(ifpath.parts[-1][3:-4])
				# ~ if current_signal_number == 2255:
				raw_df = pandas.read_csv(
					ifpath,
					sep = '\t',
					header = 4,
				)
				if invert:
					raw_df['Ampl'] *= -1
				signal = LGADSignal(
					time = raw_df['Time'],
					samples = raw_df['Ampl'],
				)
				
				print(f'{current_signal_number}\t{signal.amplitude}\t{signal.noise}\t{signal.rise_time}\t{signal.collected_charge}\t{signal.time_over_noise}', file = ofile)
				if np.random.rand() < 20/n_signals_to_parse:
					fig = mpl.manager.new(
						title = f'Signal number {current_signal_number}',
						subtitle = f'Data set: {bureaucrat.measurement_name}',
						xlabel = 'Time (s)',
						ylabel = 'Amplitude (V)',
					)
					signal.plot_myplotlib(fig)
					mpl.manager.save_all(mkdir = f'{bureaucrat.processed_data_dir_path}/signals plots')
				
########################################################################

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser(description='Parse a bunch of signals from a single detector.')
	parser.add_argument('--dir',
		metavar = 'path', 
		help = 'Path to the base measurement directory. The raw data must be in a sub directory called "raw".',
		required = True,
		dest = 'path',
	)
	parser.add_argument('--invert',
	help = 'If this argument is provided, the raw data is multiplied by -1 when reading. This is for negative pulses. Default is not to invert.',
	required = False,
	dest = 'invert',
	action = 'store_true',
)

	args = parser.parse_args()
	script_core(
		directory = args.path,
		invert = args.invert,
	)
